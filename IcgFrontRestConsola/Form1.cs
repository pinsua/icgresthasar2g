﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using RestHasar2G;
using System.Xml.Linq;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.IO;

namespace IcgFrontRestConsola
{
    public partial class Form1 : Form
    {
        public SqlConnection _conn = new SqlConnection();
        public string _N = "B";
        public string _numfiscal;

        public string _ip = "";
        public string _caja = "";
        public string _password = "";
        public string _serverConfig = "";
        public string _userConfig = "";
        public string _catalogConfig = "";
        public bool _monotributo = false;
        public bool _hasarLog = false;
        public string _terminal = "";
        public string _tktregalo1 = "";
        public string _tktregalo2 = "";
        public string _tktregalo3 = "";
        public string _pathIrsa = "";
        public string _keyIcg = "";

        public string strConnection;
        public bool _KeyIsOk;
        InfoIrsa _irsa = new InfoIrsa(); 

        public Form1()
        {
            InitializeComponent();
            this.Text = this.Text + " - V." + Application.ProductVersion;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (RestHasar2G.FuncionesVarias.LeerXmlConfiguracion(out _ip, out _caja, out _password, out _serverConfig,
                        out _userConfig, out _catalogConfig, out _monotributo, out _tktregalo1, out _tktregalo2,
                        out _tktregalo3, out _hasarLog, out _keyIcg, out _irsa))
            {

                //Validamos el KeySoftware.
                _KeyIsOk = ValidoKeySoftware(_keyIcg);
                //Armamos el stringConnection.
                strConnection = "Data Source=" + _serverConfig + ";Initial Catalog=" + _catalogConfig+ ";User Id=" + _userConfig+ ";Password=masterkey;";
                //Recupero nombre terminal
                _terminal = Environment.MachineName;

                //Conectamos.
                _conn.ConnectionString = strConnection;

                try
                {
                    _conn.Open();

                    GetDataset(_conn);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                DeshabilitarBotones();
                MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuración." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private bool ValidoKeySoftware(string keyIcg)
        {
            //string _key = IcgVarios.NumerosSerie.GetCPUId();

            //string _cod = IcgVarios.Licencia.GenerarLicenciaICG(_key);

            string _key = IcgVarios.LicenciaIcg.Value();

            string _cod = IcgVarios.NuevaLicencia.Value(_key);

            if (keyIcg == _cod)
                return true;
            else
                return false;

        }


        private void DeshabilitarBotones()
        {
            //Desabilito los botones.
            btImprimir.Enabled = false;
            btReimprimir.Enabled = false;
            btTicketRegalo.Enabled = false;
        }

        private void GetDataset(SqlConnection _con)
        {
            string _sql;

            if (rbSinImprimir.Checked)
            {

                _sql = @"SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.NUMFACTURA, 
                        TIQUETSCAB.TOTALBRUTO,  TIQUETSCAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, 
                        TIQUETSCAB.NUMEROFISCAL, 
						CASE WHEN TIQUETSCAB.TOTALBRUTO > 0 THEN 
						CASE WHEN CLIENTES.REGIMFACT = 4 THEN '006 Factura' ELSE '001 Factura' END
						ELSE 
						CASE WHEN CLIENTES.REGIMFACT = 4 THEN '008 Nota Credito' ELSE '003 Nota Credito' END
						END AS DESCRIPCION, 
                        TIQUETSCAB.FECHA, TIQUETSCAB.Z, TIQUETSCAB.CODVENDEDOR, isnull(TIQUETSVENTACAMPOSLIBRES.ANULACION_IRSA,0) as ANULACION_IRSA, TIQUETSCAB.HORAFIN
                        FROM TIQUETSCAB INNER JOIN CLIENTES ON TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE
                        inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO and TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE and TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N 
                        WHERE(TIQUETSCAB.NUMEROFISCAL is null or TIQUETSCAB.NUMEROFISCAL = '') 
                        AND TIQUETSCAB.FECHA <= @date AND TIQUETSCAB.N = @N AND TIQUETSCAB.CAJA = @caja 
                        AND year(TIQUETSCAB.FECHAANULACION) = 1899
                        ORDER BY TIQUETSCAB.FECHA";                
            }
            else
            {
                _sql = "SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.NUMFACTURA, " +
                    "TIQUETSCAB.TOTALBRUTO,  TIQUETSCAB.TOTALNETO, CLIENTES.NOMBRECLIENTE, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, " +
                    "TIQUETSCAB.NUMEROFISCAL, " +
                    //"CASE WHEN TIQUETSCAB.TOTALBRUTO > 0 THEN CASE WHEN CLIENTES.REGIMFACT = 4 THEN '006 Factura' ELSE '001 Factura' END "+
                    //"ELSE CASE WHEN CLIENTES.REGIMFACT = 4 THEN '008 Nota Credito' ELSE '003 Nota Credito' END END AS DESCRIPCION,"+
                    "CASE WHEN TIQUETSCAB.TOTALBRUTO > 0 THEN "+
                    "CASE WHEN CLIENTES.REGIMFACT = 4 THEN '006 Factura' ELSE '001 Factura' END "+
                    "ELSE CASE when TIQUETSCAB.TOTALBRUTO = 0 then 'Cancelacion' ELSE "+
	                "CASE WHEN CLIENTES.REGIMFACT = 4 THEN '008 Nota Credito' ELSE '003 Nota Credito' END "+
                    "END END AS DESCRIPCION," +
                    "TIQUETSCAB.FECHA, TIQUETSCAB.Z, TIQUETSCAB.CODVENDEDOR, isnull(TIQUETSVENTACAMPOSLIBRES.ANULACION_IRSA,0) as ANULACION_IRSA, TIQUETSCAB.HORAFIN" +
                    " FROM TIQUETSCAB INNER JOIN CLIENTES ON TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "inner join TIQUETSVENTACAMPOSLIBRES on TIQUETSCAB.FO = TIQUETSVENTACAMPOSLIBRES.FO and TIQUETSCAB.SERIE = TIQUETSVENTACAMPOSLIBRES.SERIE and TIQUETSCAB.NUMERO = TIQUETSVENTACAMPOSLIBRES.NUMERO and TIQUETSCAB.N = TIQUETSVENTACAMPOSLIBRES.N " +
                    "WHERE ISNULL(TIQUETSCAB.NUMEROFISCAL,0) > 0 AND TIQUETSCAB.FECHA <= @date " +
                    "AND TIQUETSCAB.N = @N AND TIQUETSCAB.CAJA = @caja  ORDER BY TIQUETSCAB.FECHA";
            }

            //Limpio el dataset
            dtsImpresiones.Tables["Impresiones"].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _con))
            {
                _cmd.Parameters.AddWithValue("@date", dtpSeleccion.Value.Date);
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(dtsImpresiones, "Impresiones");
                }
            }
        }

        private void dtpSeleccion_ValueChanged(object sender, EventArgs e)
        {
            GetDataset(_conn);
        }

        private void grImpresiones_SelectionChanged(object sender, EventArgs e)
        {
            if (grImpresiones.RowCount > 0)
            {
                if (grImpresiones.SelectedRows.Count > 0)
                {
                    //object _numfiscal = grImpresiones.SelectedRows[0].Cells["NUMEROFISCAL"].Value;
                    _numfiscal = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                    string _serieFiscal2 = grImpresiones.SelectedRows[0].Cells[5].Value.ToString();
                    if (String.IsNullOrEmpty(_numfiscal) || _numfiscal == "0")
                    {
                        btReimprimir.Enabled = false;
                        btTicketRegalo.Enabled = false;
                        btImprimir.Enabled = true;
                    }
                    else
                    {
                        if (grImpresiones.SelectedRows[0].Cells[2].Value.ToString().ToUpper().Contains("FACTURA"))
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = true;
                        }
                        else
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = false;
                        }
                    }
                }
            }
        }

        private void btReimprimir_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        try
                        {
                            string _descrip = grImpresiones.SelectedRows[0].Cells[2].Value.ToString();
                            string _numFactura = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                            decimal _totalNeto = Convert.ToDecimal(grImpresiones.SelectedRows[0].Cells[7].Value);
                            if (_totalNeto == 0)
                            {
                                MessageBox.Show(new Form { TopMost = true }, "El comprobante seleccionado fue una CANCELACION del controlador Fiscal." + Environment.NewLine + "No se puede reemprimir.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            else
                            {
                                //Recupero la cabecera.
                                bool _faltaPapel;
                                string _resp = Impresiones.Reimprimir(Convert.ToInt32(_numFactura), _descrip, _ip, out _faltaPapel);

                                if (!String.IsNullOrEmpty(_resp))
                                    MessageBox.Show(new Form { TopMost = true }, _resp, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                if (_faltaPapel)
                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel." + Environment.NewLine + "Por favor revise el papel del Controlador Fiscal.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            //Vemos si tenemos licencia OK.
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        int _fo = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[14].Value);
                        string _serie = grImpresiones.SelectedRows[0].Cells[16].Value.ToString();
                        int _numero = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[3].Value);
                        string _N = grImpresiones.SelectedRows[0].Cells[13].Value.ToString();
                        string _numFactura = grImpresiones.SelectedRows[0].Cells[15].Value.ToString();
                        string _ptoVta = grImpresiones.SelectedRows[0].Cells[4].Value.ToString();
                        string _nroFiscal = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                        string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                        string _z = grImpresiones.SelectedRows[0].Cells[12].Value.ToString();
                        int _codVendedor = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[10].Value);
                        int _anulacionIrsa = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[11].Value);

                        int _nrocomprobante = 0;
                        int _nropos = 0;
                        string _letracomprobante;
                        try
                        {
                            if (_anulacionIrsa == 0)
                            {
                                //Recuperamos los datos de la cabecera.
                                Cabecera _cab = Cabecera.GetCabecera(_fo, _serie, _numero, _N, _codVendedor, _conn);
                                List<Items> _items = Items.GetItems(_serie, _N, _numero, _conn);

                                if (_items.Count > 0)
                                {
                                    List<Promociones> _promociones = Promociones.GetPromociones(_cab.serie, _cab.numero, _cab.n, _conn);
                                    List<Pagos> _pagos = Pagos.GetPagos(_serie, _numero, _N, _conn);

                                    if (_pagos.Count > 0)
                                    {
                                        Cliente _cliente = Cliente.GetCliente(_cab.codcliente, _conn);
                                        if (_cab.totalbruto < 0)
                                        {
                                            bool _cuitOK = true;
                                            //Vemos si debemos validar el cuit.
                                            if (_cliente.regimenFacturacion == "1")
                                            {
                                                int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                if (_digitorecibido == _digitoValidador)
                                                    _cuitOK = true;
                                                else
                                                    _cuitOK = false;
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(_cliente.documento))
                                                    _cliente.documento = "11111111";
                                            }
                                            //Validamos el campo direccion del cliente.
                                            if (String.IsNullOrEmpty(_cliente.direccion))
                                                _cliente.direccion = ".";
                                            if (_cuitOK)
                                            {
                                                List<Descuentos> _desc = Descuentos.GetDescuentos(_fo, _serie, _numero, _N, _conn);
                                                bool _faltaPapel;
                                                string _respImpre;
                                                if (_monotributo)
                                                    _respImpre = Impresiones.ImprimirNotaCreditoCSinValidarFecha(_cab, _cliente, _items, _desc, _pagos, _ip, _conn, _hasarLog, out _faltaPapel);
                                                else
                                                    _respImpre = Impresiones.ImprimirNotaCreditoABSinValidarFecha(_cab, _cliente, _items, _desc, _pagos, _ip, _conn, _hasarLog, out _faltaPapel);
                                                if (!String.IsNullOrEmpty(_respImpre))
                                                    MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                //else
                                                //{
                                                //    //Recupero el nombre de la terminal.
                                                //    string _terminal = Environment.MachineName;
                                                //    //Grabamos en Rem_Transacciones para que la mande de nuevo a manager.
                                                //    Rem_transacciones.InsertRemTransacciones(_terminal, _serie, _numero, _N, _conn);
                                                //}

                                                if (_faltaPapel)
                                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel." + Environment.NewLine + " Por favor revise el papel del Controlador Fiscal.",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                //Vemos si tenemos que lanzar IRSA.
                                                if (!String.IsNullOrEmpty(_pathIrsa))
                                                {
                                                    EscribirXml(_fo, _serie, _numero, _N, _codVendedor);
                                                    LanzarTrancomp(_pathIrsa);
                                                }
                                            }
                                            else
                                                MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto." + Environment.NewLine + " Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                        }
                                        else
                                        {
                                            bool _cuitOK = true;
                                            //Vemos si debemos validar el cuit.
                                            if (_cliente.regimenFacturacion == "1")
                                            {

                                                int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                if (_digitorecibido == _digitoValidador)
                                                    _cuitOK = true;
                                                else
                                                    _cuitOK = false;
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(_cliente.documento))
                                                    _cliente.documento = "11111111";
                                            }
                                            //Validamos el campo direccion del cliente.
                                            if (String.IsNullOrEmpty(_cliente.direccion))
                                                _cliente.direccion = ".";
                                            if (_cuitOK)
                                            {
                                                List<Descuentos> _desc = Descuentos.GetDescuentos(_fo, _serie, _numero, _N, _conn);
                                                bool _faltaPapel;
                                                try
                                                {
                                                    string _respImpre;
                                                    if (_monotributo)
                                                        _respImpre = Impresiones.ImprimirFacturasCSinValidarFecha(_cab, _cliente, _items, _desc, _pagos, _promociones, _ip, _conn, _hasarLog, out _faltaPapel);
                                                    else
                                                        _respImpre = Impresiones.ImprimirFacturasABSinValidarFecha(_cab, _cliente, _items, _desc, _pagos, _promociones, _ip, _conn, _hasarLog, 
                                                            out _faltaPapel, out _nrocomprobante, out _nropos, out _letracomprobante);
                                                    if (!String.IsNullOrEmpty(_respImpre))
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    //else
                                                    //{
                                                    //    //Recupero el nombre de la terminal.
                                                    //    string _terminal = Environment.MachineName;
                                                    //    //Grabamos en,  Rem_Transacciones para que la mande de nuevo a manager.
                                                    //    Rem_transacciones.InsertRemTransacciones(_terminal, _serie, _numero, _N, _conn);
                                                    //}

                                                    if (_faltaPapel)
                                                        MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel." + Environment.NewLine + "Por favor revise el papel del Controlador Fiscal.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                    //Vemos si tenemos que lanzar IRSA.
                                                    if (!String.IsNullOrEmpty(_pathIrsa))
                                                    {
                                                        if (EscribirXml(_fo, _serie, _numero, _N, _codVendedor))
                                                            LanzarTrancomp(_pathIrsa);
                                                        else
                                                            MessageBox.Show(new Form { TopMost = true }, "No se pudo generar el archivo IMPRIME-XML." + Environment.NewLine + "La venta no fue informada.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                            }
                                            else
                                                MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto." + Environment.NewLine + "Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }

                                        //Refrescamos al grid.
                                        GetDataset(_conn);
                                    }
                                    else
                                    {
                                        MessageBox.Show(new Form { TopMost = true }, "No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(new Form { TopMost = true }, "No existen Items registrados. " + Environment.NewLine + "Por favor revise el comprobante.",
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                            }
                            else
                            {
                                MessageBox.Show(new Form { TopMost = true }, "El comprobante fue cancelado por el Controlador Fiscal." + Environment.NewLine + " Este comprobante debe ser ANULADO. No se puede Imprimir.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        catch (Exception ex)
                        {
                            //Validacion impuesta por Francisco.
                            if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: El controlador informa que ha cambiado la jornada Fiscal, y no es puede imprimir. Debe FORZAR un Cierre Z." ,
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            else
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                    }

                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina" ,
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private bool EscribirXml(int pfo, string pserie, int pnumero, string pn, int pcodvendedor)
        {
            bool _rta = false;
            try
            {
                string strXml = @"<doc>
                               <bd>
                                  <server>#server#</server>
                                  <database>#base#</database>
                                  <user>#user#</user>
                               </bd>
                               <tipodoc>FACVENTA</tipodoc>
                               <fo>#fo#</fo>
                               <serie>#serie#</serie>
                               <numero>#numero#</numero>
                               <n>#n#</n>
                               <codvendedor>#vendedor#</codvendedor>
                            </doc>";
                //Reemplazo los datos.
                strXml = strXml.Replace("#server#", _serverConfig).Replace("#base#", _catalogConfig).Replace("#user#", _userConfig).Replace("#fo#", pfo.ToString())
                    .Replace("#serie#", pserie).Replace("#numero#", pnumero.ToString()).Replace("#n#", pn).Replace("#vendedor#", pcodvendedor.ToString());

                XElement xml = XElement.Parse(strXml);

                XDocument pruebaXml = new XDocument(xml);
                string _path = Environment.CurrentDirectory;
                _path = _path + @"\imprime.xml";
                pruebaXml.Save(_path);
                _rta = true;
            }
            catch (Exception ex)
            {
                _rta = false;
                throw new NotImplementedException();
            }

            return _rta;
        }

        private void btTicketRegalo_Click(object sender, EventArgs e)
        {
            //Vemos si tenemos licencia OK.
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        if (grImpresiones.SelectedRows[0].Cells[2].Value.ToString().ToUpper().Contains("FACTURA"))
                        {
                            try
                            {
                                int _fo = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[14].Value);
                                string _serie = grImpresiones.SelectedRows[0].Cells[16].Value.ToString();
                                int _numero = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[3].Value);
                                string _N = grImpresiones.SelectedRows[0].Cells[13].Value.ToString();
                                string _numFactura = grImpresiones.SelectedRows[0].Cells[15].Value.ToString();
                                string _ptoVta = grImpresiones.SelectedRows[0].Cells[4].Value.ToString();
                                string _nroFiscal = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                                string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                                string _z = grImpresiones.SelectedRows[0].Cells[12].Value.ToString();
                                int _codVendedor = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[10].Value);

                                Cabecera _cabecera = Cabecera.GetCabecera(_fo, _serie, _numero, _N, _codVendedor, _conn);
                                List<Items> _lstItems = Items.GetItems(_serie, _N, _numero, _conn);

                                bool _faltaPapel;
                                Impresiones.ImprimirTicketRegalo(_cabecera, _lstItems, _ip, _tktregalo1, _tktregalo2, _tktregalo3, _compro, _hasarLog, out _faltaPapel);

                                if (_faltaPapel)
                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Los tickets de regalo/cambio solo se pueden imprimir sobre una factura A o B.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void rbTipoB_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoB.Checked)
                rbTipoN.Checked = false;
            //Cambio variable.
            _N = "B";
            //Cargo Dataset.
            GetDataset(_conn);
        }

        private void rbTipoN_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoN.Checked)
                rbTipoB.Checked = false;
            //Cambio Variable.
            _N = "N";
            //Cargo Dataset.
            GetDataset(_conn);
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_conn.State == ConnectionState.Open)
                _conn.Close();
        }

        private void rbImpresas_CheckedChanged(object sender, EventArgs e)
        {
            if (rbImpresas.Checked)
                rbSinImprimir.Checked = false;

            GetDataset(_conn);
        }

        private void rbSinImprimir_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSinImprimir.Checked)
                rbImpresas.Checked = false;

            GetDataset(_conn);
        }

        private static void LanzarTrancomp(string pPath)
        {
            try
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = pPath;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardInput = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();
         
                p.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, ex.Message);
            }
        }

        private void grImpresiones_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (Convert.ToInt32(grImpresiones.Rows[e.RowIndex].Cells[11].Value) > 0)
            {
                grImpresiones.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                grImpresiones.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.White;
            }
        }

        private void btReimpresionZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmReimpresionZ _frm = new frmReimpresionZ(_ip, strConnection, _caja, _hasarLog);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmCierreZ _frm = new frmCierreZ(_ip, strConnection, _caja, _password, _hasarLog);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btForzarZ_Click(object sender, EventArgs e)
        {
            string _msjCbte;
            bool _IsOk = true;

            if (_KeyIsOk)
            {
                //Vemos si tenemos algo en la Password
                if (!String.IsNullOrEmpty(_password))
                {
                    frmPassword frm = new frmPassword();
                    frm.ShowDialog(this);
                    string _pp = frm._Text;
                    frm.Dispose();
                    if (_password != _pp)
                        _IsOk = false;
                }

                if (_IsOk)
                {
                    //Vemos si tenemos comprobantes sin fiscalizar y armamos el MSJ.
                    if (RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _caja))
                        _msjCbte = "POSEE COMPROBANTES SIN FISCALIZAR. " + Environment.NewLine + "Desea continuar con la impresión del Z fiscal?.";
                    else
                        _msjCbte = "Desea continuar con la impresión del Z fiscal?.";

                    if (MessageBox.Show(new Form { TopMost = true }, _msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        //instanciamos el controlador Fiscal.
                        hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                        hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                        hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                        string _msj;

                        try
                        {
                            //Conectamos.
                            RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                            //ejecutamos el cierre Z.
                            _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                            _zeta = _cierre.Z;

                            _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                              "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                              "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                              "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                              "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                              "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                              "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                              "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                              "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                              "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                              "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                              "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                              "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                              "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                              "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                              "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                              "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                              "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                              "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                              "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                              "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                            //Modificamos la tabla Arqueos.
                            string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _zeta.getDF_Total();

                            int _rta = Arqueo.UpdateArqueosOK("Z", _dato, _conn);
                        }
                        catch (Exception ex)
                        {
                            RestHasar2G.Impresiones.Cancelar(hasar);
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
                else
                    MessageBox.Show(new Form { TopMost = true }, "No posee permisos para realizar esta acción." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

        }

        private void btCierreX_Click(object sender, EventArgs e)
        {

            if (_KeyIsOk)
            {
                DialogResult result = MessageBox.Show(new Form { TopMost = true }, "Seguro que desea realizar un Cierre X?.", "ICG - Argentina",
                                            MessageBoxButtons.YesNo);
                switch (result)
                {
                    case DialogResult.Yes:
                        {
                            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();

                            string _msj;

                            try
                            {
                                //Conectamos.
                                RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                                //ejecutamos el cierre X.
                                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                                _equis = _cierre.X;

                                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                  "Cierre 'X' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                                  "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                                  "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DF Total                 =[" + _equis.getDF_Total() + "]" + Environment.NewLine +
                                  "DF Total IVA             =[" + _equis.getDF_TotalIVA() + "]" + Environment.NewLine +
                                  "DF Total Otros Tributos  =[" + _equis.getDF_TotalTributos() + "]" + Environment.NewLine +
                                  "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "NC Total                 =[" + _equis.getNC_Total() + "]" + Environment.NewLine +
                                  "NC Total IVA             =[" + _equis.getNC_TotalIVA() + "]" + Environment.NewLine +
                                  "NC Total Otros Tributos  =[" + _equis.getNC_TotalTributos() + "]" + Environment.NewLine +
                                  "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]";

                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                            }
                            catch (Exception ex)
                            {
                                RestHasar2G.Impresiones.Cancelar(hasar);
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }

                            break;
                        }
                    case DialogResult.No:
                        {
                            this.Text = "[Cancel]";
                            break;
                        }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btInvocar_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                bool _rtaComprobantesOk = false;

                using (SqlConnection _con = new SqlConnection(strConnection))
                {
                    _con.Open();

                    _rtaComprobantesOk = RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_con, _caja);

                    if (_con.State == System.Data.ConnectionState.Open)
                        _con.Close();
                }

                if (!_rtaComprobantesOk)
                {
                    Process p = Process.GetProcessesByName("FrontRest").FirstOrDefault();
                    if (p != null)
                    {
                        string _msjCbte = "Desea continuar con la impresión del Z fiscal?.";
                        //Enviamos el CierreZ
                        if (MessageBox.Show(new Form { TopMost = true }, _msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {

                            //Invocamos al cierre de ICG, por medio del ShortCut.
                            IntPtr h = p.MainWindowHandle;
                            Program.SetForegroundWindow(h);
                            SendKeys.SendWait("%(z)");
                            SendKeys.Flush();

                            //instanciamos el controlador Fiscal.
                            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                            string _msj;

                            try
                            {
                                //Conectamos.
                                RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                                //ejecutamos el cierre Z.
                                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                                _zeta = _cierre.Z;

                                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                  "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                                  "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                                  "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                                  "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                                  "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                                  "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                  "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                                  "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                                  "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                                  "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                                  "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                                  "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                  "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                                  "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                                  "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                                // Modificamos la tabla Arqueos.
                                string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _zeta.getDF_Total();

                                int _rta = Arqueo.UpdateArqueosOK("Z", _dato, _conn);
                            }
                            catch (Exception ex)
                            {
                                RestHasar2G.Impresiones.Cancelar(hasar);
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btConfig_Click(object sender, EventArgs e)
        {
            bool _pasoOk = true;

            if (!String.IsNullOrEmpty(_password))
            {
                frmPassword frm = new frmPassword();
                frm.ShowDialog();
                string _super = frm._Text;
                if (_password != _super)
                    _pasoOk = false;

            }
            if (_pasoOk)
            {
                frmConfiguracionH2G frm = new frmConfiguracionH2G();
                frm.ShowDialog(this);
                frm.Dispose();
            }
        }
    }
}
