﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace RestHasar2G
{
    public class Series
    {
        public string serie { get; set; }
        public string descripcion { get; set; }
        public int numpedcb { get; set; }
        public int numalbcb { get; set; }
        public int numalbcn { get; set; }
        public int numtiqb { get; set; }
        public int numtiqn { get; set; }
        public int numfacvb { get; set; }
        public int numfacvn { get; set; }

        public static Series GetSerieBySerie(string _serie, SqlConnection _con)
        {
            Series _cls = new Series();

            string _sql = @"SELECT SERIE, DESCRIPCION, NUMPEDCB, NUMALBCB, NUMALBCN, NUMTIQB, NUMTIQN, NUMFACVB, NUMFACVN FROM SERIES where SERIE = @Serie";

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@Serie", _serie);
                
                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cls.descripcion = _reader["DESCRIPCION"].ToString();
                            _cls.serie = _reader["SERIE"].ToString();
                            _cls.numalbcb = Convert.ToInt32(_reader["NUMALBCB"]);
                            _cls.numalbcn = Convert.ToInt32(_reader["NUMALBCN"]);
                            _cls.numfacvn = Convert.ToInt32(_reader["NUMFACVN"]);
                            _cls.numfacvb = Convert.ToInt32(_reader["NUMFACVB"]);
                            _cls.numpedcb = Convert.ToInt32(_reader["NUMPEDCB"]);
                            _cls.numtiqb = Convert.ToInt32(_reader["NUMTIQB"]);
                            _cls.numtiqn = Convert.ToInt32(_reader["NUMTIQN"]);
                            
                        }
                    }
                }
            }

            return _cls;
        }

        public static bool UpdateSerieBySerie(Series _serie, SqlConnection _con)
        {
            Series _cls = new Series();

            string _sql = @"UPDATE SERIES
                SET NUMPEDCB = @numpedcb, NUMALBCB = @numalbcb, NUMALBCN = @numalbcn, NUMTIQB = @numtiqb, 
                NUMTIQN = @numtiqn, NUMFACVB = @numfacvb, NUMFACVN = @numfacvn 
                FROM SERIES where SERIE = @Serie";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@numpedcb", _serie.numpedcb);
                    _cmd.Parameters.AddWithValue("@numalbcb", _serie.numalbcb);
                    _cmd.Parameters.AddWithValue("@numalbcn", _serie.numalbcn);
                    _cmd.Parameters.AddWithValue("@numtiqb", _serie.numtiqb);
                    _cmd.Parameters.AddWithValue("@numtiqn", _serie.numtiqn);
                    _cmd.Parameters.AddWithValue("@numfacvb", _serie.numfacvb);
                    _cmd.Parameters.AddWithValue("@numfacvn", _serie.numfacvn);
                    _cmd.Parameters.AddWithValue("@Serie", _serie.serie);

                    int _Q = _cmd.ExecuteNonQuery();

                    if (_Q > 0)
                        return true;
                    else
                        return false;

                }
            }
            catch(Exception ex)
            {
                throw new Exception("UpdateSerieBySerie " + ex.Message);
            }
        }

        public static bool UpdateSerieBySerie(Series _serie, SqlConnection _con, SqlTransaction _tran)
        {
            Series _cls = new Series();

            string _sql = @"UPDATE SERIES
                SET NUMPEDCB = @numpedcb, NUMALBCB = @numalbcb, NUMALBCN = @numalbcn, NUMTIQB = @numtiqb, 
                NUMTIQN = @numtiqn, NUMFACVB = @numfacvb, NUMFACVN = @numfacvn 
                FROM SERIES where SERIE = @Serie";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@numpedcb", _serie.numpedcb);
                    _cmd.Parameters.AddWithValue("@numalbcb", _serie.numalbcb);
                    _cmd.Parameters.AddWithValue("@numalbcn", _serie.numalbcn);
                    _cmd.Parameters.AddWithValue("@numtiqb", _serie.numtiqb);
                    _cmd.Parameters.AddWithValue("@numtiqn", _serie.numtiqn);
                    _cmd.Parameters.AddWithValue("@numfacvb", _serie.numfacvb);
                    _cmd.Parameters.AddWithValue("@numfacvn", _serie.numfacvn);
                    _cmd.Parameters.AddWithValue("@Serie", _serie.serie);

                    int _Q = _cmd.ExecuteNonQuery();

                    if (_Q > 0)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {
                throw new Exception("UpdateSerieBySerie " + ex.Message);
            }
        }
    }
}
