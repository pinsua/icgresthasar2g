﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IcgFrontRestConsola2019
{
    public partial class frmReimpresionZ : Form
    {
        private string _ip;

        private string _Caja;

        private string _connection;
        private bool _hasarLog;

        public SqlConnection _conn = new SqlConnection();

        public static hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();

        public frmReimpresionZ(string sIp, string sConnection, string sCaja, bool _hasarlog)
        {
            InitializeComponent();
            //paso las variables
            _ip = sIp;
            _connection = sConnection;
            _Caja = sCaja;
            _hasarLog = _hasarlog;
            //
            this.Text = this.Text + " - V." + Application.ProductVersion;
        }

        private void frmReimpresionZ_Load(object sender, EventArgs e)
        {
            try
            {
                //Conectamos.
                _conn.ConnectionString = _connection;

                _conn.Open();

                GetCierresZ(dtsReimpresion, "Reimpresion", "Z", _Caja, _conn);

                if (grCierre.Rows.Count == 0)
                {
                    btCierreZ.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                DeshabilitarBotones();
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguente error: " + ex.Message + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        //private static void GetArqueo(DataSet _dts, string _table, string _tipo, string _caja, SqlConnection _con)
        //{
        //    string _sql = "SELECT ARQUEO, CAJA, NUMERO, FECHA, CLEANCASHCONTROLCODE1 FROM ARQUEOS " +
        //        "WHERE ARQUEO = @tipo AND CAJA = @caja ORDER BY NUMERO";

        //    //Limpio el dataset
        //    _dts.Tables[_table].Clear();

        //    using (SqlCommand _cmd = new SqlCommand(_sql, _con))
        //    {
        //        _cmd.Parameters.AddWithValue("@tipo", _tipo);
        //        _cmd.Parameters.AddWithValue("@caja", _caja);

        //        using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
        //        {
        //            _sda.Fill(_dts, _table);
        //        }
        //    }

        //    foreach (DataRow dr in _dts.Tables[_table].Rows)
        //    {
        //        try
        //        {
        //            string _dato = dr["CLEANCASHCONTROLCODE1"].ToString();

        //            if (String.IsNullOrEmpty(_dato))
        //                dr["NumeroHasar"] = 0;
        //            else
        //            {
        //                dr["NumeroHasar"] = _dato.Split(';')[0];
        //            }
        //        }
        //        catch
        //        {
        //            dr["NumeroHasar"] = 0;
        //        }
        //    }
        //}

        private static void GetCierresZ(DataSet _dts, string _table, string _tipo, string _caja, SqlConnection _con)
        {
            //string _sql = @"SELECT TIQUETSCAB.CAJA, TIQUETSVENTACAMPOSLIBRES.Z_NRO, TIQUETSVENTACAMPOSLIBRES.Z_TOTAL, CAST(TIQUETSVENTACAMPOSLIBRES.Z_FECHA_HORA AS DATE) AS FECHA, TIQUETSVENTACAMPOSLIBRES.Z_TIQUETS
            //    FROM TIQUETSVENTACAMPOSLIBRES JOIN TIQUETSCAB ON TIQUETSVENTACAMPOSLIBRES.FO = TIQUETSCAB.FO 
            //    AND TIQUETSVENTACAMPOSLIBRES.SERIE = TIQUETSCAB.SERIE 
            //    AND TIQUETSVENTACAMPOSLIBRES.NUMERO = TIQUETSCAB.NUMERO 
            //    AND TIQUETSVENTACAMPOSLIBRES.N = TIQUETSCAB.N
            //    WHERE TIQUETSCAB.CAJA = @caja AND TIQUETSVENTACAMPOSLIBRES.Z_NRO IS NOT NULL 
            //    GROUP BY TIQUETSCAB.CAJA, TIQUETSVENTACAMPOSLIBRES.Z_NRO, TIQUETSVENTACAMPOSLIBRES.Z_TOTAL, TIQUETSVENTACAMPOSLIBRES.Z_FECHA_HORA, TIQUETSVENTACAMPOSLIBRES.Z_TIQUETS
            //    ORDER BY TIQUETSVENTACAMPOSLIBRES.Z_NRO";
            string _sql = @"SELECT CAJA, NUMERO, FECHA, HORA, TOTAL, NUMVENTASIMPRESAS as Tiquets FROM ARQUEOS WHERE CODVENDEDOR = -99 and CAJA = @caja ORDER BY NUMERO DESC";

            //Limpio el dataset
            _dts.Tables[_table].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _con))
            {
                _cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(_dts, _table);
                }
            }

        }

        private void DeshabilitarBotones()
        {
            btCierreZ.Enabled = !btCierreZ.Enabled;
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            try
            {
                if (grCierre.SelectedRows.Count > 0)
                {
                    //Recupero los datos del grid.
                    int _caja = Convert.ToInt32(grCierre.SelectedRows[0].Cells[0].Value);
                    //DateTime _fecha = Convert.ToDateTime(grCierre.SelectedRows[0].Cells[1].Value);
                    int _numero = Convert.ToInt32(grCierre.SelectedRows[0].Cells[1].Value);
                    //decimal _total = Convert.ToDecimal(grCierre.SelectedRows[0].Cells[3].Value);
                    //int _tiquets = Convert.ToInt32(grCierre.SelectedRows[0].Cells[3].Value);
                    if (_numero > 0)
                    {
                        hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosCierreZeta _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosCierreZeta();
                        hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();

                        try
                        {
                            //Conectamos.   
                            RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                            //ejecutamos el cierre Z.
                            hasar.ReportarZetasPorNumeroZeta(_numero, _numero, hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteAuditoria.REPORTE_AUDITORIA_GLOBAL);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
                else
                    MessageBox.Show(new Form { TopMost = true }, "Debe seleccionar un Cierre de la tabla.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grCierre_SelectionChanged(object sender, EventArgs e)
        {
            if (grCierre.RowCount > 0)
            {
                if (grCierre.SelectedRows.Count > 0)
                {
                    string _numero = grCierre.SelectedRows[0].Cells[1].Value.ToString();
                    int _nroCierre;
                    if (String.IsNullOrEmpty(_numero))
                        btCierreZ.Enabled = false;
                    else
                    {
                        _nroCierre = Convert.ToInt32(_numero);
                        if (_nroCierre > 0)
                            btCierreZ.Enabled = true;
                        else
                            btCierreZ.Enabled = false;
                    }
                }
            }
        }
    }
}
