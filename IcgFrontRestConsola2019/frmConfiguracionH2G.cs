﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgFrontRestConsola2019
{
    public partial class frmConfiguracionH2G : Form
    {
        public frmConfiguracionH2G()
        {
            InitializeComponent();
            //Titulo.
            this.Text = this.Text + " - V." + Application.ProductVersion;
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            txtIp.Text = Form1._ip;
            txtCaja.Text = Form1._caja;
            txtTicketReg1.Text = Form1._tktregalo1;
            txtTicketReg2.Text = Form1._tktregalo2;
            txtTicketReg3.Text = Form1._tktregalo3;
            txtLicenciaIcg.Text = Form1._keyIcg;
            txtServidor.Text = Form1._serverConfig;
            txtUsuario.Text = Form1._userConfig;
            txtBaseDatos.Text = Form1._catalogConfig;
            #region Trancomp
            txtIrsaContrato.Text = Form1._irsa.contrato;
            txtIrsaLocal.Text = Form1._irsa.local;
            txtIrsaPath.Text = Form1._irsa.pathSalida;
            txtIrsaPos.Text = Form1._irsa.pos;
            txtIrsaRubro.Text = Form1._irsa.rubro;
            #endregion
            #region SiTef
            txtCuitSitef.Text = Form1._sitef.sitefCuit;
            txtCuitIsvSitef.Text = "30711277249";
            txtIdTerminalSitef.Text = Form1._sitef.sitefIdTerminal;
            txtIdTiendaSitef.Text = Form1._sitef.sitefIdTienda;
            txtPathSitef.Text = Form1._sitef.pathSendInvoice;
            chUsaClover.Checked = Convert.ToBoolean(Form1._sitef.usaClover);
            #endregion
            if (!File.Exists("Config2daGeneracion.xml"))
            {
                MessageBox.Show(new Form { TopMost = true }, "No existe el archivo de configuración." +
                        Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                XmlDocument xDocNew = new XmlDocument();
                xDocNew.Load("Config2daGeneracion.xml");
                //IP
                XmlNodeList _IP = xDocNew.GetElementsByTagName("ip");
                if (_IP.Count > 0)
                    _IP[0].InnerXml = txtIp.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("ip");
                    XmlText text = xDocNew.CreateTextNode(txtIp.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //CAJA
                XmlNodeList _CAJA = xDocNew.GetElementsByTagName("caja");
                if (_CAJA.Count > 0)
                    _CAJA[0].InnerXml = txtCaja.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("caja");
                    XmlText text = xDocNew.CreateTextNode(txtCaja.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Password
                XmlNodeList _PASSWORD = xDocNew.GetElementsByTagName("super");
                if (_PASSWORD.Count == 0)
                {
                    XmlElement elem = xDocNew.CreateElement("super");
                    XmlText text = xDocNew.CreateTextNode("hoguera");
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Monotributo
                XmlNodeList _MONOTRIBUTO = xDocNew.GetElementsByTagName("monotributo");
                if (_MONOTRIBUTO.Count == 0)
                {
                    XmlElement elem = xDocNew.CreateElement("monotributo");
                    XmlText text = xDocNew.CreateTextNode("false");
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //tktregalo1
                XmlNodeList _TKTREGALO1 = xDocNew.GetElementsByTagName("tktregalo1");
                if (_TKTREGALO1.Count > 0)
                    _TKTREGALO1[0].InnerXml = txtTicketReg1.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("tktregalo1");
                    XmlText text = xDocNew.CreateTextNode(txtTicketReg1.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //tktregalo2
                XmlNodeList _TKTREGALO2 = xDocNew.GetElementsByTagName("tktregalo2");
                if (_TKTREGALO2.Count > 0)
                    _TKTREGALO2[0].InnerXml = txtTicketReg2.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("tktregalo2");
                    XmlText text = xDocNew.CreateTextNode(txtTicketReg2.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //tktregalo3
                XmlNodeList _TKTREGALO3 = xDocNew.GetElementsByTagName("tktregalo3");
                if (_TKTREGALO3.Count > 0)
                    _TKTREGALO3[0].InnerXml = txtTicketReg3.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("tktregalo3");
                    XmlText text = xDocNew.CreateTextNode(txtTicketReg3.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //keyICG
                XmlNodeList _KEYICG = xDocNew.GetElementsByTagName("keyICG");
                if (_KEYICG.Count > 0)
                    _KEYICG[0].InnerXml = txtLicenciaIcg.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("keyICG");
                    XmlText text = xDocNew.CreateTextNode(txtLicenciaIcg.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //HasarLog
                XmlNodeList _HASARLOG = xDocNew.GetElementsByTagName("hasarlog");
                if (_HASARLOG.Count > 0)
                    _HASARLOG[0].InnerXml = "false";
                else
                {
                    XmlElement elem = xDocNew.CreateElement("hasarlog");
                    XmlText text = xDocNew.CreateTextNode("false");
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //CodigoFudex
                XmlNodeList _CODIGOFUDEX = xDocNew.GetElementsByTagName("codigoFudex");
                if (_CODIGOFUDEX.Count > 0)
                    _CODIGOFUDEX[0].InnerXml = "";
                else
                {
                    XmlElement elem = xDocNew.CreateElement("codigoFudex");
                    XmlText text = xDocNew.CreateTextNode("");
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }

                //SEver
                XmlNodeList _SEVER = xDocNew.GetElementsByTagName("server");
                if (_SEVER.Count > 0)
                    _SEVER[0].InnerXml = txtServidor.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("server");
                    XmlText text = xDocNew.CreateTextNode(txtServidor.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //USER
                XmlNodeList _USER = xDocNew.GetElementsByTagName("user");
                if (_USER.Count > 0)
                    _USER[0].InnerXml = txtUsuario.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("user");
                    XmlText text = xDocNew.CreateTextNode(txtUsuario.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //CATALOG
                XmlNodeList _CATALOG = xDocNew.GetElementsByTagName("database");
                if (_CATALOG.Count > 0)
                    _CATALOG[0].InnerXml = txtBaseDatos.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("database");
                    XmlText text = xDocNew.CreateTextNode(txtBaseDatos.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                #region Trancomp
                //IRSA CATALOG
                XmlNodeList _CONTRATO = xDocNew.GetElementsByTagName("Contrato");
                if (_CONTRATO.Count > 0)
                    _CONTRATO[0].InnerXml = txtIrsaContrato.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Contrato");
                    XmlText text = xDocNew.CreateTextNode(txtIrsaContrato.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA CATALOG
                XmlNodeList _LOCAL = xDocNew.GetElementsByTagName("Local");
                if (_LOCAL.Count > 0)
                    _LOCAL[0].InnerXml = txtIrsaLocal.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Local");
                    XmlText text = xDocNew.CreateTextNode(txtIrsaLocal.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA CATALOG
                XmlNodeList PATHSALIDA = xDocNew.GetElementsByTagName("PathSalida");
                if (PATHSALIDA.Count > 0)
                    PATHSALIDA[0].InnerXml = txtIrsaPath.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("PathSalida");
                    XmlText text = xDocNew.CreateTextNode(txtIrsaPath.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA POS
                XmlNodeList POS = xDocNew.GetElementsByTagName("Pos");
                if (POS.Count > 0)
                    POS[0].InnerXml = txtIrsaPos.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Pos");
                    XmlText text = xDocNew.CreateTextNode(txtIrsaPos.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //IRSA RUBRO
                XmlNodeList RUBRO = xDocNew.GetElementsByTagName("Rubro");
                if (RUBRO.Count > 0)
                    RUBRO[0].InnerXml = txtIrsaRubro.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Rubro");
                    XmlText text = xDocNew.CreateTextNode(txtIrsaRubro.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                #endregion
                #region IRSA SiTef
                //Terminal ID
                XmlNodeList SitefTerminal = xDocNew.GetElementsByTagName("IdTerminal");
                if (SitefTerminal.Count > 0)
                    SitefTerminal[0].InnerXml = txtIdTerminalSitef.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("IdTerminal");
                    XmlText text = xDocNew.CreateTextNode(txtIdTerminalSitef.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Tienda ID
                XmlNodeList SitefTienda = xDocNew.GetElementsByTagName("IdTienda");
                if (SitefTienda.Count > 0)
                    SitefTienda[0].InnerXml = txtIdTiendaSitef.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("IdTienda");
                    XmlText text = xDocNew.CreateTextNode(txtIdTiendaSitef.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Tienda CUIT
                XmlNodeList SitefCuit = xDocNew.GetElementsByTagName("Cuit");
                if (SitefCuit.Count > 0)
                    SitefCuit[0].InnerXml = txtCuitSitef.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("Cuit");
                    XmlText text = xDocNew.CreateTextNode(txtCuitSitef.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //CUIT ISV 
                XmlNodeList SitefCuitISV = xDocNew.GetElementsByTagName("CuitIsv");
                if (SitefCuitISV.Count > 0)
                    SitefCuitISV[0].InnerXml = txtCuitIsvSitef.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("CuitIsv");
                    XmlText text = xDocNew.CreateTextNode(txtCuitIsvSitef.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Path  
                XmlNodeList SiTefPath = xDocNew.GetElementsByTagName("PathSendInvioce");
                if (SiTefPath.Count > 0)
                    SiTefPath[0].InnerXml = txtPathSitef.Text.Trim();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("PathSendInvioce");
                    XmlText text = xDocNew.CreateTextNode(txtPathSitef.Text.Trim());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                //Clover  
                XmlNodeList SitefClover = xDocNew.GetElementsByTagName("UsaClover");
                if (SitefClover.Count > 0)
                    SitefClover[0].InnerXml = chUsaClover.Checked.ToString();
                else
                {
                    XmlElement elem = xDocNew.CreateElement("UsaClover");
                    XmlText text = xDocNew.CreateTextNode(chUsaClover.Checked.ToString());
                    xDocNew.DocumentElement.AppendChild(elem);
                    xDocNew.DocumentElement.LastChild.AppendChild(text);
                }
                #endregion
                //Fin Nuevo
                xDocNew.Save("Config2daGeneracion.xml");

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar guardar el archivo de configuración." +
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLicencia_Click(object sender, EventArgs e)
        {
            //frmKeyDisplay _frm = new frmKeyDisplay();
            //_frm.ShowDialog(this);
            //_frm.Dispose();
            if (MessageBox.Show("Para obtener una licencia, debe tener: " + Environment.NewLine + "El controlador Fiscal conectado y encendido." +
                Environment.NewLine + "La Ip asignada el mismo y configurada en esta pantalla." + Environment.NewLine + "Caso contrario no será posible gestionarla y deberá comunicarse con ICG Argentina.",
                "ICG Argentina", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //Recuperamos los Datos del controlador.
                RestHasar2G.DatosControlador _datosControlador = new RestHasar2G.DatosControlador();
                try
                {
                    _datosControlador = RestHasar2G.FuncionesVarias.GetDatosControlador(txtIp.Text.Trim());
                }
                catch
                {
                    MessageBox.Show("No ha sido posible conectar con el controlador fiscal " + Environment.NewLine +
                            "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Validamos licencia via WebService.
                try
                {
                    RestHasar2G.Licencia lic = new RestHasar2G.Licencia();
                    lic.ClientCuit = _datosControlador.Cuit;
                    lic.ClientName = _datosControlador.RazonSocial;
                    lic.ClientRazonSocial = _datosControlador.RazonSocial;

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = Form1._conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "select VALOR from PARAMETROS where TERMINAL = 'VersionBD' and CLAVE = 'VersionBD'";

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    lic.Version = reader["VALOR"].ToString();
                                }
                            }
                        }
                    }
                    lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                    lic.password = "Pinsua.2730";
                    lic.Plataforma = "REST";
                    lic.Release = Application.ProductVersion;
                    lic.user = "pinsua@yahoo.com";
                    lic.Tipo = "FC H2G";
                    lic.TerminalName = Environment.MachineName;
                    lic.PointOfSale = _datosControlador.NroPos;
                    var json = JsonConvert.SerializeObject(lic);
#if DEBUG
                    var client = new RestClient("http://localhost:18459/api/Licencias/GetKey");
#else
                        var client = new RestClient("http://licencias.icgargentina.com.ar:11100/api/Licencias/GetKey");
#endif
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", json, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        RestHasar2G.ResponcePostLicencia res = JsonConvert.DeserializeObject<RestHasar2G.ResponcePostLicencia>(response.Content);
                        txtLicenciaIcg.Text = res.Key;
                    }
                    else
                    {
                        MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                        response.Content, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                        ex.Message + Environment.NewLine + "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidarColumna(string _table, string _columna, SqlConnection _sqlConn)
        {
            string _sql = @"IF NOT EXISTS(SELECT column_name FROM information_schema.columns WHERE table_name=@table and column_name=@column )
                BEGIN SELECT 'NO EXISTE' END ELSE BEGIN SELECT 'EXISTE' END";
            bool _rta = false;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _sqlConn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                _cmd.Parameters.AddWithValue("@table", _table);
                _cmd.Parameters.AddWithValue("column", _columna);

                if (_cmd.ExecuteScalar().ToString() == "EXISTE")
                    _rta = true;
            }
            return _rta;
        }

        private string ValidarColumnasCamposLibres(string _connection)
        {
            string _msj = "";
            try
            {
                using (SqlConnection _sqlConn = new SqlConnection(_connection))
                {
                    _sqlConn.Open();

                    //if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_NRO", _sqlConn))
                    //{
                    //    _msj = _msj + "No existe la Columna Z_NRO en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    //}
                    //if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_TOTAL", _sqlConn))
                    //{
                    //    _msj = _msj + "No existe la Columna Z_TOTAL en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    //}
                    //if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_FECHA_HORA", _sqlConn))
                    //{
                    //    _msj = _msj + "No existe la Columna Z_FECHA_HORA en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    //}
                    //if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_IMPUESTOS", _sqlConn))
                    //{
                    //    _msj = _msj + "No existe la Columna Z_IMPUESTOS en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    //}
                    //if (!ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_TIQUETS", _sqlConn))
                    //{
                    //    _msj = _msj + "No existe la Columna Z_TIQUETS en la tabla TIQUETSVENTACAMPOSLIBRES" + Environment.NewLine;
                    //}
                    if (!ValidarColumna("CLIENTESCAMPOSLIBRES", "TIPO_DOC", _sqlConn))
                    {
                        _msj = _msj + "No existe la Columna TIPO_DOC en la tabla CLIENTESCAMPOSLIBRES" + Environment.NewLine;
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error validando CamposLibres. " + ex.Message);
            }
            return _msj;
        }

        private void btnValidarCampos_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtBaseDatos.Text))
                    throw new Exception("No se ha definido ninguna base de datos. Por favor definala y luego reintente.");
                if(String.IsNullOrEmpty(txtServidor.Text))
                    throw new Exception("No se ha definido ningun servidor. Por favor definalo y luego reintente.");
                if (String.IsNullOrEmpty(txtUsuario.Text))
                    throw new Exception("No se ha definido ningun usuario. Por favor definalo y luego reintente.");
                string _connection = "Data Source=" + txtServidor.Text.Trim() + ";Initial Catalog=" + txtBaseDatos.Text.Trim() + ";User Id=" + txtUsuario.Text.Trim() + ";Password=masterkey;";

                string _valcamposlibres = ValidarColumnasCamposLibres(_connection);
                if (!String.IsNullOrEmpty(_valcamposlibres))
                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar validar los campos." +
                        Environment.NewLine + "Error: " + _valcamposlibres + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            catch(Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar validar los campos." +
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnRegestry_Click(object sender, EventArgs e)
        {
            try
            {
                string _serverSql = "";
                string _database = "";
                IcgVarios.IcgRegestry.Rest.GetDataBaseToConnect(out _serverSql, out _database);
                txtBaseDatos.Text = _database;
                txtServidor.Text = _serverSql;
            }
            catch(Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar recuperar los datos de Conexión." +
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina, o ingreselos manualmente.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            }
        }
    }
}
