﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IcgVarios;

namespace IcgFrontRestConsola2019
{
    public partial class frmKeyDisplay : Form
    {
        public frmKeyDisplay()
        {
            InitializeComponent();
            textBox1.Text = IcgVarios.LicenciaIcg.Value();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
