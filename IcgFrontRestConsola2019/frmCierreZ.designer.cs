﻿namespace IcgFrontRestConsola2019
{
    partial class frmCierreZ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbGrid = new System.Windows.Forms.GroupBox();
            this.grCierre = new System.Windows.Forms.DataGridView();
            this.arqueoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.horaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtsCierres = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.gbCierres = new System.Windows.Forms.GroupBox();
            this.btClose = new System.Windows.Forms.Button();
            this.btCierreZ = new System.Windows.Forms.Button();
            this.gbGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCierre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsCierres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.gbCierres.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbGrid
            // 
            this.gbGrid.Controls.Add(this.grCierre);
            this.gbGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbGrid.Location = new System.Drawing.Point(0, 0);
            this.gbGrid.Name = "gbGrid";
            this.gbGrid.Size = new System.Drawing.Size(468, 261);
            this.gbGrid.TabIndex = 3;
            this.gbGrid.TabStop = false;
            this.gbGrid.Text = "Cierres de Caja";
            // 
            // grCierre
            // 
            this.grCierre.AllowUserToAddRows = false;
            this.grCierre.AllowUserToDeleteRows = false;
            this.grCierre.AutoGenerateColumns = false;
            this.grCierre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grCierre.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.arqueoDataGridViewTextBoxColumn,
            this.numeroDataGridViewTextBoxColumn,
            this.fechaDataGridViewTextBoxColumn,
            this.horaDataGridViewTextBoxColumn});
            this.grCierre.DataMember = "Cierres";
            this.grCierre.DataSource = this.dtsCierres;
            this.grCierre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grCierre.Location = new System.Drawing.Point(3, 16);
            this.grCierre.Name = "grCierre";
            this.grCierre.ReadOnly = true;
            this.grCierre.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grCierre.Size = new System.Drawing.Size(462, 242);
            this.grCierre.TabIndex = 0;
            // 
            // arqueoDataGridViewTextBoxColumn
            // 
            this.arqueoDataGridViewTextBoxColumn.DataPropertyName = "Arqueo";
            this.arqueoDataGridViewTextBoxColumn.HeaderText = "Arqueo";
            this.arqueoDataGridViewTextBoxColumn.Name = "arqueoDataGridViewTextBoxColumn";
            this.arqueoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numeroDataGridViewTextBoxColumn
            // 
            this.numeroDataGridViewTextBoxColumn.DataPropertyName = "Numero";
            this.numeroDataGridViewTextBoxColumn.HeaderText = "Numero";
            this.numeroDataGridViewTextBoxColumn.Name = "numeroDataGridViewTextBoxColumn";
            this.numeroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaDataGridViewTextBoxColumn
            // 
            this.fechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha";
            this.fechaDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn.Name = "fechaDataGridViewTextBoxColumn";
            this.fechaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // horaDataGridViewTextBoxColumn
            // 
            this.horaDataGridViewTextBoxColumn.DataPropertyName = "Hora";
            this.horaDataGridViewTextBoxColumn.HeaderText = "Hora";
            this.horaDataGridViewTextBoxColumn.Name = "horaDataGridViewTextBoxColumn";
            this.horaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtsCierres
            // 
            this.dtsCierres.DataSetName = "NewDataSet";
            this.dtsCierres.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4});
            this.dataTable1.TableName = "Cierres";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Arqueo";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Numero";
            this.dataColumn2.DataType = typeof(int);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Fecha";
            this.dataColumn3.DataType = typeof(System.DateTime);
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Hora";
            // 
            // gbCierres
            // 
            this.gbCierres.Controls.Add(this.btClose);
            this.gbCierres.Controls.Add(this.btCierreZ);
            this.gbCierres.Dock = System.Windows.Forms.DockStyle.Right;
            this.gbCierres.Location = new System.Drawing.Point(468, 0);
            this.gbCierres.Name = "gbCierres";
            this.gbCierres.Size = new System.Drawing.Size(158, 261);
            this.gbCierres.TabIndex = 2;
            this.gbCierres.TabStop = false;
            this.gbCierres.Text = "Cierres";
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(42, 93);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(75, 23);
            this.btClose.TabIndex = 1;
            this.btClose.Text = "Salir";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btCierreZ
            // 
            this.btCierreZ.Location = new System.Drawing.Point(42, 35);
            this.btCierreZ.Name = "btCierreZ";
            this.btCierreZ.Size = new System.Drawing.Size(75, 23);
            this.btCierreZ.TabIndex = 0;
            this.btCierreZ.Text = "Cierre Z";
            this.btCierreZ.UseVisualStyleBackColor = true;
            this.btCierreZ.Click += new System.EventHandler(this.btCierreZ_Click);
            // 
            // frmCierreZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 261);
            this.Controls.Add(this.gbGrid);
            this.Controls.Add(this.gbCierres);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmCierreZ";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina - CierreZ";
            this.Load += new System.EventHandler(this.frmCierreZ_Load);
            this.gbGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCierre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsCierres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.gbCierres.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbGrid;
        private System.Windows.Forms.DataGridView grCierre;
        private System.Windows.Forms.GroupBox gbCierres;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btCierreZ;
        private System.Data.DataSet dtsCierres;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn arqueoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn horaDataGridViewTextBoxColumn;
    }
}