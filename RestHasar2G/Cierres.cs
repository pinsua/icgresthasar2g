﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace RestHasar2G
{
    public class Cierres
    {      
        public static string ImprimirCierreZ(string _ip, bool _hasarLog, int _caja, SqlConnection _sqlCon)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
            string _msj;

            try
            {
                //Veo si tenemos campos libres
                bool _campoLibre = FuncionesVarias.ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_NRO", _sqlCon);
                //Conectamos.
                Impresiones.Conectar(hasar, _hasarLog, _ip);
                TimeSpan _hora = hasar.ConsultarFechaHora().getHora();
                //ejecutamos el cierre Z.
                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                _zeta = _cierre.Z;
                //Recupero el Total.
                double _total = _zeta.getDF_Total() / 100;
                //Recupero total Impuestos
                double _total_Impuestos = (_zeta.getDF_TotalIVA() / 100) + (_zeta.getDF_TotalTributos() / 100);              
                //Recupero Fecha
                DateTime _fecha = _zeta.getFecha();
                DateTime _fechaHora = _fecha + _hora;
                //Recupero numero Z.
                int _nroZeta = _zeta.getNumero();
                //Recuperro Total NC.
                double _totalNC = _zeta.getNC_Total() / 100;
                //Recupero Total NC Iva.
                double _totalNC_Iva = _zeta.getNC_TotalIVA() / 100;
                int _cantidadTiquets = _zeta.getDF_CantidadEmitidos();
                double _totalNeto = _total - _totalNC;
                string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _totalNeto.ToString();
                
                //Grabamos la información en los campos libres.
                if (_campoLibre)
                {
                    //Actualizo los campos libres.
                    UpdateTiquetsVentasCamposLibres(_fechaHora, _totalNeto, _total_Impuestos, _cantidadTiquets, _nroZeta, _sqlCon);
                    ////Actualizo RemTransacciones.
                    //Rem_transacciones.InsertRemTransaccionesCierreZ(Environment.MachineName, _nroZeta, _caja, _sqlCon);
                }
                InsertArqueos(_caja, _nroZeta, _fechaHora, _totalNeto, _total_Impuestos, _cantidadTiquets, _dato, _sqlCon);
                //Recupero nombre terminal
                string _terminal = Environment.MachineName;
                //Mandamos a Rem_transacciones.
                Rem_transacciones.InsertRemTransaccionesCierreZ_X(_terminal, _nroZeta, _caja, _sqlCon);
                int _rta = UpdateArqueosOK("Z", _dato, _sqlCon);
                Rem_transacciones.InsertRemTransaccionesCierreZ(_terminal, _nroZeta, _caja, _sqlCon);

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                  "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                  "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                  "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                  "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                  "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                  "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                  "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                  "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                  "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                  "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                  "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                return _msj;

            }
            catch (Exception ex)
            {
                //Cancelamos
                Impresiones.Cancelar(hasar);
                //Mandamos la Excepcion
                throw new Exception(ex.Message);
            }
        }

        public static string ImprimirCierreX(string _ip, bool _hasarLog, int _caja, SqlConnection _sqlCon)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
            //hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();
            string _msj;

            try
            {
                //Veo si tenemos campos libres
                bool _campoLibre = FuncionesVarias.ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_NRO", _sqlCon);
                //Conectamos.
                Impresiones.Conectar(hasar, _hasarLog, _ip);
                TimeSpan _hora = hasar.ConsultarFechaHora().getHora();
                //ejecutamos el cierre Z.
                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                _equis = _cierre.X;
                //Recupero el Total.
                double _total = _equis.getDF_Total() / 100;
                //Recupero total Impuestos
                double _total_Impuestos = (_equis.getDF_TotalIVA() / 100) + (_equis.getDF_TotalTributos() / 100);
                //Recupero Fecha
                DateTime _fecha = _equis.getFechaCierre();
                DateTime _fechaHora = _fecha + _hora;
                //Recupero numero Z.
                int _nroEquis = _equis.getNumero();
                //Recuperro Total NC.
                double _totalNC = _equis.getNC_Total() / 100;
                //Recupero Total NC Iva.
                double _totalNC_Iva = _equis.getNC_TotalIVA() / 100;
                int _cantidadTiquets = _equis.getDF_CantidadEmitidos();
                double _totalNeto = _total - _totalNC;
                string _dato = _equis.getNumero() + ";" + _equis.getDF_CantidadEmitidos() + ";" + _totalNeto.ToString();

                //Grabamos la información en los campos libres.
                //if (_campoLibre)
                //{
                //    //Actualizo los campos libres.
                //    UpdateTiquetsVentasCamposLibres(_fechaHora, _totalNeto, _total_Impuestos, _cantidadTiquets, _nroZeta, _sqlCon);
                //    ////Actualizo RemTransacciones.
                //    //Rem_transacciones.InsertRemTransaccionesCierreZ(Environment.MachineName, _nroZeta, _caja, _sqlCon);
                //}
                //InsertArqueos(_caja, _nroZeta, _fechaHora, _totalNeto, _total_Impuestos, _cantidadTiquets, _dato, _sqlCon);
                //Recupero nombre terminal
                string _terminal = Environment.MachineName;
                //Mandamos a Rem_transacciones.
                //Rem_transacciones.InsertRemTransaccionesCierreZ_X(_terminal, _nroEquis, _caja, _sqlCon);
                //int _rta = UpdateArqueosOK("Z", _dato, _sqlCon);
                //Rem_transacciones.InsertRemTransaccionesCierreZ(_terminal, _nroEquis, _caja, _sqlCon);

                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                  "Cierre 'Z' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                  "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                  "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                  "DF Total                 =[" + string.Format("{0:0.00}", _equis.getDF_Total() / 100) + "]" + Environment.NewLine +
                  "DF Total IVA             =[" + string.Format("{0:0.00}", _equis.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _equis.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                  "NC Total                 =[" + string.Format("{0:0.00}", _equis.getNC_Total() / 100) + "]" + Environment.NewLine +
                  "NC Total IVA             =[" + string.Format("{0:0.00}", _equis.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _equis.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                  "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +"]";

                return _msj;

            }
            catch (Exception ex)
            {
                //Cancelamos
                Impresiones.Cancelar(hasar);
                //Mandamos la Excepcion
                throw new Exception(ex.Message);
            }
        }
        public static bool UpdateTiquetsVentasCamposLibres(DateTime _fechaZ, double _totalZ, double _impuestosZ, int _tiquetsZ, int _nroZ, SqlConnection _connection)
        {
            string _sql = @"UPDATE TIQUETSVENTACAMPOSLIBRES SET TIQUETSVENTACAMPOSLIBRES.Z_FECHA_HORA = @FECHA, TIQUETSVENTACAMPOSLIBRES.Z_TOTAL = @TOTAL, 
                TIQUETSVENTACAMPOSLIBRES.Z_IMPUESTOS = @IMPUESTOS, TIQUETSVENTACAMPOSLIBRES.Z_TIQUETS = @TIQUETS
                FROM TIQUETSCAB INNER JOIN TIQUETSVENTACAMPOSLIBRES on tiquetscab.serie = TIQUETSVENTACAMPOSLIBRES.serie 
                and tiquetscab.numero = TIQUETSVENTACAMPOSLIBRES.numero and tiquetscab.n = TIQUETSVENTACAMPOSLIBRES.n
                WHERE TIQUETSVENTACAMPOSLIBRES.z_nro = @nroZeta";
            bool _rta = false;
            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _connection;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@FECHA", _fechaZ);
                _cmd.Parameters.AddWithValue("@TOTAL", _totalZ);
                _cmd.Parameters.AddWithValue("@IMPUESTOS", _impuestosZ);
                _cmd.Parameters.AddWithValue("@TIQUETS", _tiquetsZ);
                _cmd.Parameters.AddWithValue("@nroZeta", _nroZ);
                try
                {
                    _cmd.ExecuteNonQuery();
                    _rta = true;
                }
                catch
                {
                    _rta = false;
                }
                return _rta;
            }
        }

        public static int UpdateArqueosOK(string _tipo, string _dato, SqlConnection _conn)
        {
            string _sql = "UPDATE ARQUEOS SET CLEANCASHCONTROLCODE1 = @dato " +
                "WHERE ARQUEOS.ARQUEO = @Tipo AND (CLEANCASHCONTROLCODE1 IS NULL OR CLEANCASHCONTROLCODE1 = '')";

            int _rta = 0;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@dato", _dato);
                _cmd.Parameters.AddWithValue("@Tipo", _tipo);

                _rta = _cmd.ExecuteNonQuery();

            }

            return _rta;
        }

        public static int InsertArqueos(int _caja, int _nroZ, DateTime _fecha, double _totalZ, double _impuestosZ, int _tiquetsZ, string _dato, SqlConnection _conn)
        {
            //string _sql = "UPDATE ARQUEOS SET CLEANCASHCONTROLCODE1 = @dato " +
            //"WHERE ARQUEOS.ARQUEO = @Tipo AND (CLEANCASHCONTROLCODE1 IS NULL OR CLEANCASHCONTROLCODE1 = '')";

            string _sql = @"IF(SELECT COUNT(1) FROM ARQUEOS WHERE ARQUEO = 'X' AND CAJA = @caja AND NUMERO = @nroZ) = 0
            BEGIN
            INSERT INTO [ARQUEOS] ([ARQUEO],[CAJA],[NUMERO],[CODVENDEDOR],[FECHA],[HORA],[TOTAL],[DESCUADRE],[PUNTEO],[SESION],[ACUMULADO]
           ,[ACUMULADON],[NUMMESASABIERTAS],[IMPORTEMESASABIERTAS],[NUMVENTASIMPRESAS],[IMPORTEVENTASIMPRESAS],[OBSERVACIONES],[CORREUNICO],[CLEANCASHCONTROLCODE1]
           ,[CERRADO]) VALUES('X',@caja,@nroZ,-99,@fecha,@hora,@total,0,0,@totalImpuestos,0,0,0,0,@cantTiquets,@total,'Cierre Z',0,@dato,0)
            END
            ELSE
            BEGIN
            UPDATE ARQUEOS SET CODVENDEDOR = -99, FECHA = @fecha, HORA = @hora, TOTAL = @total, ACUMULADO = @totalImpuestos,
            NUMVENTASIMPRESAS = @cantTiquets, IMPORTEVENTASIMPRESAS = total, OBSERVACIONES = 'Cierre Z', CLEANCASHCONTROLCODE1 = @dato
            WHERE ARQUEO = 'X' and CAJA = @caja and NUMERO = @nroZ
            END";

            int _rta = 0;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _conn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@caja", _caja);
                _cmd.Parameters.AddWithValue("@nroZ", _nroZ);
                _cmd.Parameters.AddWithValue("@fecha", _fecha.Date);
                _cmd.Parameters.AddWithValue("@hora", _fecha.ToLongTimeString());
                _cmd.Parameters.AddWithValue("@total", _totalZ);
                _cmd.Parameters.AddWithValue("@totalImpuestos", _impuestosZ);
                _cmd.Parameters.AddWithValue("@cantTiquets", _tiquetsZ);                
                _cmd.Parameters.AddWithValue("@dato", _dato);

                _rta = _cmd.ExecuteNonQuery();

            }

            return _rta;
        }
    }
}
