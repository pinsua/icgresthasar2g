﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestHasar2G
{
    public class Impresiones
    {
        #region NOTA CREDITO AB
        public static string ImprimirNotaCreditoAB(Cabecera _cab, Cliente _cli,
            List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos, 
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            string _serieFiscal2 = "";
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            _letraComprobante = "";
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            //Recupero los datos del Punto de venta.
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    switch (_cli.regimenFacturacion)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                             _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _serie_numero, "");
                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                    }

                    List<Items> _itemsOrdenados = _items.OrderBy(o => o._cantidad).ToList();
                    foreach (Items _it in _itemsOrdenados)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            decimal _newCantidad = _it._cantidad * -1;
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_newCantidad),
                                Convert.ToDouble(_it._precio),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _procentaje = System.Math.Abs(_it._descuento);
                                double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                                //Valido que tengo texto descuento.
                                if (String.IsNullOrEmpty(_it._textoPromo))
                                {
                                    if (_it._descuento == 0)
                                        _it._textoPromo = "Descuento ";
                                    else
                                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                }
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                Math.Abs(_dsc._totDtoComer),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    Math.Abs(_dsc._totDtoPp),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;                                    
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                    }

                    //_subTotal = hasar.ConsultarSubtotal();

                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();                                      

                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();

                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Vemos si el error fue porque necesita el Cierre Z.
                    if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                    {
                        //_necesitaCierreZ = true;
                        _rta = "Cierre Z";
                    }
                    else
                    {
                        if (_nroComprobante > 0)
                        {
                            try
                            {
                                if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                    hasar.conectar(_ip);
                                if (hasar.ConsultarEstado().getNumeroUltimoComprobante() == _nroComprobante)
                                {
                                    if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                    {
                                        //Disparo la cancelacion.
                                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                        _rta = "CANCELADA ";
                                        Impresiones.Cancelar(hasar);
                                    }
                                    else
                                    {
                                        Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                                    }
                                }                                
                            }
                            catch (Exception exc)
                            {
                                _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                                throw new Exception(_rta + exc.Message);
                            }
                        }
                        else
                        {
                            _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + ex.Message);
                        }
                    }                    
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        public static string ImprimirNotaCreditoAB(Cabecera _cab, Cliente _cli,
            List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            _letraComprobante = "";
            string _serieFiscal2 = "";
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            _necesitaCierreZ = false;
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    switch (_cli.regimenFacturacion)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                             _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _serie_numero,"");
                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                    }

                    List<Items> _itemsOrdenados = _items.OrderBy(o => o._cantidad).ToList();
                    foreach (Items _it in _itemsOrdenados)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            decimal _newCantidad = _it._cantidad * -1;
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_newCantidad),
                                Convert.ToDouble(_it._precio),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _procentaje = System.Math.Abs(_it._descuento);
                                double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                                //Valido que tengo texto descuento.
                                if (String.IsNullOrEmpty(_it._textoPromo))
                                {
                                    if (_it._descuento == 0)
                                        _it._textoPromo = "Descuento ";
                                    else
                                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                }
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                Math.Abs(_dsc._totDtoComer),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    Math.Abs(_dsc._totDtoPp),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;                                    
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                    }

                    //_subTotal = hasar.ConsultarSubtotal();

                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();
                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();

                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Vemos si el error fue porque necesita el Cierre Z.
                    if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                    {
                        _necesitaCierreZ = true;
                        _rta = "Cierre Z";
                    }
                    else
                    {
                        //Grabamos el nro del comprobante con error.
                        if (_nroComprobante > 0)
                        {
                            try
                            {
                                if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                    hasar.conectar(_ip);
                                if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                {
                                    Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    _rta = "CANCELADA. Error: ";
                                    Impresiones.Cancelar(hasar);
                                }
                                else
                                    Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                            }
                            catch (Exception exc)
                            {
                                _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                                throw new Exception(_rta + exc.Message);
                            }
                        }
                        else
                        {
                            _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + ex.Message);
                        }                        
                    }
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

       
        public static string ImprimirNotaCreditoABSinValidarFecha(Cabecera _cab, Cliente _cli,
            List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, out bool _faltaPapel)
        {
            int _nroComprobante = 0;
            int _nroPos = 0;
            string _serieFiscal2 = "";
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

            try
            {
                string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                switch (_cli.regimenFacturacion)
                {
                    case "1":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                        _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "003_Credito_A";
                            break;
                        }
                    case "2":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                         hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                         hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                         _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "008_Credito_B";
                            break;
                        }
                    case "4":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                        _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "008_Credito_B";
                            break;
                        }
                    case "6":
                    case "5":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                        "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _serie_numero, "");
                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "003_Credito_A";
                            break;
                        }
                    default:
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                        _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "008_Credito_B";
                            break;
                        }
                }

                List<Items> _itemsOrdenados = _items.OrderBy(o => o._cantidad).ToList();
                foreach (Items _it in _itemsOrdenados)
                {
                    //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                    if (_it._detalleModif == 0)
                    {
                        decimal _newCantidad = _it._cantidad * -1;
                        hasar.ImprimirItem(_it._descripcion,
                            Convert.ToDouble(_newCantidad),
                            Convert.ToDouble(_it._precio),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            0.0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        //Veo si tengo dto en el item.
                        if (System.Math.Abs(_it._descuento) > 0)
                        {
                            decimal _procentaje = System.Math.Abs(_it._descuento);
                            double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                            //Valido que tengo texto descuento.
                            if (String.IsNullOrEmpty(_it._textoPromo))
                            {
                                if (_it._descuento == 0)
                                    _it._textoPromo = "Descuento ";
                                else
                                    _it._textoPromo = "Descuento " + _it._descuento.ToString();
                            }
                            hasar.ImprimirDescuentoItem(_it._textoPromo,
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                        }
                    }
                }

                //Vemos si tenemos dto en la cabecera.
                foreach (Descuentos _dsc in _descuentos)
                {
                    if (_dsc._dtoComercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                            Math.Abs(_dsc._totDtoComer),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_dsc._dtoPp != 0)
                    {
                        if (_dsc._dtoPp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                Math.Abs(_dsc._totDtoPp),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_dsc._totDtoPp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }
                }

                //Pagos.
                foreach (Pagos _pag in _pagos)
                {
                    switch (_pag.descripcion.ToUpper())
                    {
                        case "TARJETA CRÉDITO":
                        case "TARJETA CREDITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                            0, "", "");
                                break;
                            }
                        case "PENDIENTE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                            0, "", "");
                                break;
                            }
                        case "EFECTIVO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;                                
                            }
                        case "CHEQUE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                            0, "", "");
                                break;
                            }
                        case "TARJETA DEBITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                            0, "", "");
                                break;
                            }
                        default:
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                            0, "", "");
                                break;
                            }
                    }
                }

                //_subTotal = hasar.ConsultarSubtotal();

                _respCerrar = hasar.CerrarDocumento();

                int _numeroTicket = _respCerrar.getNumeroComprobante();
                //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                if (!Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                {
                    string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                    _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                }

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;

                if (_abrirCajon)
                    hasar.AbrirCajonDinero();

                //Valido si el comprobante se cancelo.
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                _estado = hasar.ConsultarEstado();
                if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                {
                    if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                    }
                }
            }
            catch (Exception ex)
            {
                //Grabamos el nro del comprobante con error.
                if (_nroComprobante > 0)
                {
                    try
                    {
                        if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                            hasar.conectar(_ip);
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "CANCELADA";
                        }
                        else
                            Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                    }
                    catch (Exception exc)
                    {
                        _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                            "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + exc.Message);
                    }
                }
                else
                {
                    _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                    throw new Exception(_rta + ex.Message);
                }                
            }

            return _rta;
        }


        #endregion

        #region NOTA CREDITO C
        public static string ImprimirNotaCreditoC(Cabecera _cab, Cliente _cli,
            List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out int _nroComprobante, out int _nroPos)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            //Datos Inicializacion.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);

            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                        _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_C);
                    _nroComprobante = _respAbrir.getNumeroComprobante();
                    _serieFiscal2 = "013_Credito_C";

                    foreach (Items _it in _items)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            hasar.ImprimirItem(_it._descripcion,
                            Math.Abs(Convert.ToDouble(_it._cantidad)),
                            Math.Abs(Convert.ToDouble(_it._precio)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            0.0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _procentaje = System.Math.Abs(_it._descuento);
                                double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                                //Valido que tengo texto descuento.
                                if (String.IsNullOrEmpty(_it._textoPromo))
                                {
                                    if (_it._descuento == 0)
                                        _it._textoPromo = "Descuento ";
                                    else
                                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                }
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                Math.Abs(_dsc._dtoComercial),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    Math.Abs(_dsc._totDtoPp),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }
                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;                                    
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                    }

                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();
                    

                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _nroPos.ToString().PadLeft(4,'0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();

                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Grabamos el nro del comprobante con error.
                    if (_nroComprobante > 0)
                    {
                        try
                        {
                            if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                hasar.conectar(_ip);
                            if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                            {
                                //Disparo la cancelacion.
                                Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "CANCELADA";
                            }
                            else
                                Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                        }
                        catch (Exception exc)
                        {
                            _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + exc.Message);
                        }
                    }
                    else
                    {
                        _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + ex.Message);
                    }                    
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        public static string ImprimirNotaCreditoC(Cabecera _cab, Cliente _cli,
            List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            _necesitaCierreZ = false;
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            //Datos Inicializacion.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);

            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                        _cli.direccion, _serie_numero, "", "");

                    _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_C);
                    _nroComprobante = _respAbrir.getNumeroComprobante();
                    _serieFiscal2 = "013_Credito_C";

                    foreach (Items _it in _items)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            hasar.ImprimirItem(_it._descripcion,
                            Math.Abs(Convert.ToDouble(_it._cantidad)),
                            Math.Abs(Convert.ToDouble(_it._precio)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            0.0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _procentaje = System.Math.Abs(_it._descuento);
                                double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                                //Valido que tengo texto descuento.
                                if (String.IsNullOrEmpty(_it._textoPromo))
                                {
                                    if (_it._descuento == 0)
                                        _it._textoPromo = "Descuento ";
                                    else
                                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                }
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                Math.Abs(_dsc._dtoComercial),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    Math.Abs(_dsc._totDtoPp),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }
                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;                                    
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                    }
                    _respCerrar = hasar.CerrarDocumento();
                    int _numeroTicket = _respCerrar.getNumeroComprobante();
                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }
                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Vemos si el error fue porque necesita el Cierre Z.
                    if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                    {
                        _necesitaCierreZ = true;
                        _rta = "Cierre Z";
                    }
                    else
                    {
                        //Grabamos el nro del comprobante con error.
                        if (_nroComprobante > 0)
                        {
                            try
                            {
                                if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                    hasar.conectar(_ip);
                                if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                {
                                    //Disparo la cancelacion.
                                    Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    Impresiones.Cancelar(hasar);
                                    _rta = "CANCELADA";
                                }
                                else
                                    Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                            }
                            catch (Exception exc)
                            {
                                if (MessageBox.Show(new Form { TopMost = true }, "El comprobante se imprimío correctamente?",
                            "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                                }
                                else
                                {
                                    _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente. Revise el error: ";
                                    throw new Exception(_rta + exc.Message);
                                }
                                //_rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                //    "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                                //throw new Exception(_rta + exc.Message);
                            }
                        }
                        else
                        {
                            _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + ex.Message);
                        }
                    }
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        public static string ImprimirNotaCreditoCSinValidarFecha(Cabecera _cab, Cliente _cli,
            List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, out bool _faltaPapel)
        {
            int _nroComprobante = 0;
            int _nroPos = 0;
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            //Datos Inicializacion.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);

            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);

            try
            {
                string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                    _cli.direccion, _serie_numero, "", "");

                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_C);
                _nroComprobante = _respAbrir.getNumeroComprobante();
                _serieFiscal2 = "013_Credito_C";

                foreach (Items _it in _items)
                {
                    //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                    if (_it._detalleModif == 0)
                    {
                        hasar.ImprimirItem(_it._descripcion,
                        Math.Abs(Convert.ToDouble(_it._cantidad)),
                        Math.Abs(Convert.ToDouble(_it._precio)),
                        hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                        Convert.ToDouble(_it._iva),
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                        0.0,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                        1, "", _it._codArticulo,
                        hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        //Veo si tengo dto en el item.
                        if (System.Math.Abs(_it._descuento) > 0)
                        {
                            decimal _procentaje = System.Math.Abs(_it._descuento);
                            double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                            //Valido que tengo texto descuento.
                            if (String.IsNullOrEmpty(_it._textoPromo))
                            {
                                if (_it._descuento == 0)
                                    _it._textoPromo = "Descuento ";
                                else
                                    _it._textoPromo = "Descuento " + _it._descuento.ToString();
                            }
                            hasar.ImprimirDescuentoItem(_it._textoPromo,
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                        }
                    }
                }

                //Vemos si tenemos dto en la cabecera.
                foreach (Descuentos _dsc in _descuentos)
                {
                    if (_dsc._dtoComercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                            Math.Abs(_dsc._dtoComercial),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_dsc._dtoPp != 0)
                    {
                        if (_dsc._dtoPp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                Math.Abs(_dsc._totDtoPp),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_dsc._totDtoPp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }
                }
                //Pagos.
                foreach (Pagos _pag in _pagos)
                {
                    switch (_pag.descripcion.ToUpper())
                    {
                        case "TARJETA CRÉDITO":
                        case "TARJETA CREDITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                            0, "", "");
                                break;
                            }
                        case "PENDIENTE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                            0, "", "");
                                break;
                            }
                        case "EFECTIVO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;                                
                            }
                        case "CHEQUE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                            0, "", "");
                                break;
                            }
                        case "TARJETA DEBITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                            0, "", "");
                                break;
                            }
                        default:
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                            0, "", "");
                                break;
                            }
                    }
                }
                _respCerrar = hasar.CerrarDocumento();
                int _numeroTicket = _respCerrar.getNumeroComprobante();
                //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                {
                    string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                    _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                }

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;

                if (_abrirCajon)
                    hasar.AbrirCajonDinero();

                //Valido si el comprobante se cancelo.
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                _estado = hasar.ConsultarEstado();
                if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                {
                    if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                    }
                }
            }
            catch (Exception ex)
            {
                //Grabamos el nro del comprobante con error.
                if (_nroComprobante > 0)
                {
                    try
                    {
                        if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                            hasar.conectar(_ip);
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "CANCELADA";
                        }
                        else
                            Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                    }
                    catch (Exception exc)
                    {
                        _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                            "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + exc.Message);
                    }
                }
                else
                {
                    _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                    throw new Exception(_rta + ex.Message);
                }            
            }

            return _rta;
        }
        #endregion

        #region FACTURAS AB
        public static string ImprimirFacturasAB(Cabecera _cab, Cliente _cli, List<Items> _items, 
            List<Descuentos> _descuentos, List<Pagos> _pagos, List<Promociones> _promos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            _letraComprobante = "";
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            _necesitaCierreZ = false;
            int _nroZeta = 0;
            DateTime _fechaCF = DateTime.Now;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero la fecha del CF.
                _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                    hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadLeft(5,' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);

                    switch(_cli.regimenFacturacion)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "001_Factura_A";
                                _letraComprobante = "A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                _serie_numero = _serie_numero + "    " + _supedido;
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _alias, _serie_numero);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "001_Factura_A";
                                _letraComprobante = "A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                    }
                    //Items
                    foreach (Items _it in _items)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            //Modif de hoy
                            var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                            foreach(Promociones _pr in _itPromo)
                            {
                                hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                            }

                            if (_it._cantidad < 0 && _it._descuento != 0)
                            {
                                decimal _porcentaje = System.Math.Abs(_it._descuento);
                                double _dto = Convert.ToDouble(((1 * _it._precio) * _porcentaje / 100));
                                double _prDto = Convert.ToDouble(_it._precio) - _dto;
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_prDto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);

                                //Veo si tengo dto en el item.
                                if (System.Math.Abs(_it._descuento) > 0)
                                {
                                    decimal _porcentaje = System.Math.Abs(_it._descuento);
                                    double _dto = Convert.ToDouble(((Math.Abs(_it._cantidad) * _it._precio) * _porcentaje / 100));
                                    //Valido que tengo texto descuento.
                                    //if (String.IsNullOrEmpty(_it._textoPromo))
                                    //{
                                    //    if (_it._descuento == 0)
                                    //        _it._textoPromo = "Descuento ";
                                    //    else
                                    //        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                    //}
                                    _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                                    hasar.ImprimirDescuentoItem(_it._textoPromo,
                                        _dto,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                }
                            }
                        }
                    }
                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                _dsc._totDtoComer,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    _dsc._totDtoPp,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }
                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_pag.entregado == 0)
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    else
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.entregado),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");

                                    break;
                                }
                        }
                    }
                    //Cerramos el documento.
                    _respCerrarDoc = hasar.CerrarDocumento();
                    //Recupero el numero del tiquet impreso.
                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();

                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;
                    }
                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        }
                    }
                    //Limpiamos
                    hasar.ConfigurarZona(1, _estilo, "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                }
                else
                {
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
                }                
            }
            catch (Exception ex)
            {
                //Vemos si el error fue porque necesita el Cierre Z.
                if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                {
                    _necesitaCierreZ = true;
                    _rta = "Cierre Z";
                }
                else
                {
                    //Grabamos el nro del comprobante con error.
                    if (_nroComprobante > 0)
                    {
                        try
                        {
                            if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                hasar.conectar(_ip);
                            if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                            {
                                //Disparo la cancelacion.
                                Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "CANCELADA";
                            }
                            else
                                Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                        }
                        catch (Exception exc)
                        {
                            _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + exc.Message);
                        }
                    }
                    else
                    {
                        _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + ex.Message);
                    }                    
                }
            }

            return _rta;
        }

        public static string ImprimirFacturasAB(Cabecera _cab, Cliente _cli, List<Items> _items,
            List<Descuentos> _descuentos, List<Pagos> _pagos, List<Promociones> _promos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            _letraComprobante = "";
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            int _nroZeta = 0;
            DateTime _fechaCF = DateTime.Now;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero la fecha del CF.
                _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                    hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    switch (_cli.regimenFacturacion)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "001_Factura_A";
                                _letraComprobante = "A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                _serie_numero = _serie_numero + "    " + _supedido;
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _alias, _serie_numero);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "001_Factura_A";
                                _letraComprobante = "A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                    }

                    foreach (Items _it in _items)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            //Modif de hoy
                            var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                            foreach (Promociones _pr in _itPromo)
                            {
                                hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                            }

                            if (_it._cantidad < 0 && _it._descuento != 0)
                            {
                                decimal _porcentaje = System.Math.Abs(_it._descuento);
                                double _dto = Convert.ToDouble(((1 * _it._precio) * _porcentaje / 100));
                                double _prDto = Convert.ToDouble(_it._precio) - _dto;
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_prDto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    0.0,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);

                                //Veo si tengo dto en el item.
                                if (System.Math.Abs(_it._descuento) > 0)
                                {
                                    decimal _porcentaje = System.Math.Abs(_it._descuento);
                                    double _dto = Convert.ToDouble(((Math.Abs(_it._cantidad) * _it._precio) * _porcentaje / 100));
                                    ////Valido que tengo texto descuento.
                                    //if (String.IsNullOrEmpty(_it._textoPromo))
                                    //{
                                    //    if (_it._descuento == 0)
                                    //        _it._textoPromo = "Descuento ";
                                    //    else
                                    //        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                    //}
                                    _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                                    hasar.ImprimirDescuentoItem(_it._textoPromo,
                                        _dto,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                }
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                _dsc._totDtoComer,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    _dsc._totDtoPp,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_pag.entregado == 0)
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    else
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.entregado),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");

                                    break;
                                }
                        }
                    }

                    _respCerrarDoc = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();
                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;
                    }

                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();

                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        }
                    }
                    //Limpiamos
                    hasar.ConfigurarZona(1, _estilo, "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                }
                else
                {
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
                }

            }
            catch (Exception ex)
            {
                //Grabamos el nro del comprobante con error.
                if (_nroComprobante > 0)
                {
                    try
                    {
                        if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                            hasar.conectar(_ip);
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "CANCELADA";
                        }
                        else
                            Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                    }
                    catch (Exception exc)
                    {
                        _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                            "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + exc.Message);
                    }
                }
                else
                {
                    _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                    throw new Exception(_rta + ex.Message);
                }                
            }

            return _rta;
        }

        public static string ImprimirFacturasABSinValidarFecha(Cabecera _cab, Cliente _cli, List<Items> _items,
            List<Descuentos> _descuentos, List<Pagos> _pagos, List<Promociones> _promos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            _letraComprobante = "";
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            int _nroZeta = 0;
            DateTime _fechaCF = DateTime.Now;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero la fecha del CF.
                _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;

                string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                switch (_cli.regimenFacturacion)
                {
                    case "1":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "001_Factura_A";
                            _letraComprobante = "A";
                            break;
                        }
                    case "2":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "006_Factura_B";
                            _letraComprobante = "B";
                            break;
                        }
                    case "4":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "006_Factura_B";
                            _letraComprobante = "B";
                            break;
                        }
                    case "6":
                    case "5":
                        {
                            _serie_numero = _serie_numero + "    " + _supedido;
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _alias, _serie_numero);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "001_Factura_A";
                            _letraComprobante = "A";
                            break;
                        }
                    default:
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "006_Factura_B";
                            _letraComprobante = "B";
                            break;
                        }
                }
                //recorro los items
                foreach (Items _it in _items)
                {
                    //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                    if (_it._detalleModif == 0)
                    {
                        //Modif de hoy
                        var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                        foreach (Promociones _pr in _itPromo)
                        {
                            hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                        }

                        if (_it._cantidad < 0 && _it._descuento != 0)
                        {
                            decimal _porcentaje = System.Math.Abs(_it._descuento);
                            double _dto = Convert.ToDouble(((1 * _it._precio) * _porcentaje / 100));
                            double _prDto = Convert.ToDouble(_it._precio) - _dto;
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_it._cantidad),
                                Convert.ToDouble(_prDto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        }
                        else
                        {
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_it._cantidad),
                                Convert.ToDouble(_it._precio),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                0.0,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);

                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _porcentaje = System.Math.Abs(_it._descuento);
                                double _dto = Convert.ToDouble(((Math.Abs(_it._cantidad) * _it._precio) * _porcentaje / 100));
                                //Valido que tengo texto descuento.
                                //if (String.IsNullOrEmpty(_it._textoPromo))
                                //{
                                //    if (_it._descuento == 0)
                                //        _it._textoPromo = "Descuento ";
                                //    else
                                //        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                //}
                                _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }
                }
                //Vemos si tenemos dto en la cabecera.
                foreach (Descuentos _dsc in _descuentos)
                {
                    if (_dsc._dtoComercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                            _dsc._totDtoComer,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_dsc._dtoPp != 0)
                    {
                        if (_dsc._dtoPp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                _dsc._totDtoPp,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_dsc._totDtoPp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }
                }
                //Pagos.
                foreach (Pagos _pag in _pagos)
                {
                    switch (_pag.descripcion.ToUpper())
                    {
                        case "TARJETA CRÉDITO":
                        case "TARJETA CREDITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                break;
                            }
                        case "PENDIENTE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                break;
                            }
                        case "EFECTIVO":
                            {
                                if (_pag.entregado == 0)
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                }
                                else
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.entregado),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                }
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "CHEQUE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                break;
                            }
                        case "TARJETA DEBITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                break;
                            }
                        default:
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");

                                break;
                            }
                    }
                }
                //Limpiamos
                hasar.ConfigurarZona(1, _estilo, " ",
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                //Cierro el tiquet
                _respCerrarDoc = hasar.CerrarDocumento();
                //Recupero el numero del tiquet.
                int _numeroTicket = _respCerrarDoc.getNumeroComprobante();
                //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                {
                    string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                    _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                }
                //Vemos si falta Papel
                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                {
                    _faltaPapel = true;
                }
                //Vemos si abrimos el cajon
                if (_abrirCajon)
                    hasar.AbrirCajonDinero();
                //Valido si el comprobante se cancelo.
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                _estado = hasar.ConsultarEstado();
                if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                {
                    if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                    }
                }
               
            }
            catch (Exception ex)
            {
                //Grabamos el nro del comprobante con error.
                if (_nroComprobante > 0)
                {
                    try
                    {
                        if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                            hasar.conectar(_ip);
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "CANCELADA";
                        }
                        else
                            Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                    }
                    catch(Exception exc)
                    {
                        _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                            "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + exc.Message);
                    }
                }
                else
                {
                    _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                    throw new Exception(_rta + ex.Message);
                }                
            }

            return _rta;
        }

        
        #endregion

        #region FACTURAS C
        public static string ImprimirFacturasC(Cabecera _cab, Cliente _cli, List<Items> _items,
            List<Descuentos> _descuentos, List<Pagos> _pagos, List<Promociones> _promos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog , 
            out bool _faltaPapel, out int _nroComprobante, out int _nroPOS)
        {
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            //int _nroTicket = 0;
            _nroComprobante = 0;
            _nroPOS = 0;
            int _nroZeta = 0;
            DateTime _fechaCF = DateTime.Now;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                if(_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    //Recupero el ultimo Z le sumo 1.
                    _seta = hasar.ConsultarCapacidadZetas();
                    _nroZeta = _seta.getUltimaZeta() + 1;
                    _nroPOS = hasar.ConsultarDatosInicializacion().getNumeroPos();
                    //Recupero la fecha del CF.
                    _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                    string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                    hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    //CArgamos los datos del cliente.
                    hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _alias, _serie_numero, _supedido);
                    //Abrimos el Documento.
                    _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C);
                    _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                    _serieFiscal2 = "011_Factura_C";

                    foreach (Items _it in _items)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            //Modif de hoy
                            var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                            foreach (Promociones _pr in _itPromo)
                            {
                                hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                            }

                            hasar.ImprimirItem(_it._descripcion,
                            Convert.ToDouble(_it._cantidad),
                            Convert.ToDouble(_it._precio),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            0.0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _porcentaje = System.Math.Abs(_it._descuento);
                                double _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _porcentaje / 100));
                                //Valido que tengo texto descuento.
                                //if (String.IsNullOrEmpty(_it._textoPromo))
                                //{
                                //    if (_it._descuento == 0)
                                //        _it._textoPromo = "Descuento ";
                                //    else
                                //        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                //}
                                _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                _dsc._totDtoComer,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    _dsc._totDtoPp,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_pag.entregado == 0)
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    else
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.entregado),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");

                                    break;
                                }
                        }
                    }
                    //Limpiamos
                    hasar.ConfigurarZona(1, _estilo, " ",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    _respCerrarDoc = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();                    
                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPOS.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _nroPOS.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;
                    }

                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                   
                }
                else
                {
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
                }

            }
            catch (Exception ex)
            {
                if (_nroComprobante > 0)
                {
                    if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                        hasar.conectar(_ip);
                    if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPOS.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        Impresiones.Cancelar(hasar);
                        _rta = "CANCELADA";
                    }
                    else
                        Cabecera.TransaccionOK(_cab, _nroZeta, _nroPOS.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                }
                else
                {
                    string _error = "La venta quedará registrada PENDIENTE de fiscalizar. Revise el error y REINTENTE IMPRIMIR el ticket.";
                    Impresiones.Cancelar(hasar);
                    throw new Exception(_error + ex.Message);
                }
            }

            return _rta;
        }

        public static string ImprimirFacturasC(Cabecera _cab, Cliente _cli, List<Items> _items,
            List<Descuentos> _descuentos, List<Pagos> _pagos, List<Promociones> _promos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            _necesitaCierreZ = false;
            int _nroZeta = 0;
            //int _nroTicket = 0;
            DateTime _fechaCF = DateTime.Now;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);

                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    //Recupero el ultimo Z le sumo 1.
                    _seta = hasar.ConsultarCapacidadZetas();
                    _nroZeta = _seta.getUltimaZeta() + 1;
                    _nroPos = hasar.ConsultarDatosInicializacion().getNumeroPos();
                    _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                    string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                    hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    //CArgamos los datos del cliente.
                    hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                        _cli.direccion, _alias, _serie_numero, _supedido);
                    //Abrimos el Documento.
                    _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C);
                    _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                    _serieFiscal2 = "011_Factura_C";

                    foreach (Items _it in _items)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            //Modif de hoy
                            var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                            foreach (Promociones _pr in _itPromo)
                            {
                                hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                            }

                            hasar.ImprimirItem(_it._descripcion,
                            Convert.ToDouble(_it._cantidad),
                            Convert.ToDouble(_it._precio),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            0.0,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _porcentaje = System.Math.Abs(_it._descuento);
                                double _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _porcentaje / 100));
                                //Valido que tengo texto descuento.
                                //if (String.IsNullOrEmpty(_it._textoPromo))
                                //{
                                //    if (_it._descuento == 0)
                                //        _it._textoPromo = "Descuento ";
                                //    else
                                //        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                //}
                                _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                _dsc._totDtoComer,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    _dsc._totDtoPp,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_pag.entregado == 0)
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    else
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.entregado),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");

                                    break;
                                }
                        }
                    }

                    _respCerrarDoc = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();
                    //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                    if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                    {
                        string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                    }

                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;
                    }

                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();
                    //Limpiamos
                    hasar.ConfigurarZona(1, _estilo, "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                }
                else
                {
                    _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";
                }

            }
            catch (Exception ex)
            {
                //Vemos si el error fue porque necesita el Cierre Z.
                if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                {
                    _necesitaCierreZ = true;
                    _rta = "Cierre Z";
                }
                else
                {
                    if (_nroComprobante > 0)
                    {
                        if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                            hasar.conectar(_ip);
                        if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "CANCELADA";
                        }
                        else
                            Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                    }
                    else
                    {
                        string _error = "La venta quedará registrada PENDIENTE de fiscalizar. Revise el error y REINTENTE IMPRIMIR el ticket.";
                        Impresiones.Cancelar(hasar);
                        throw new Exception(_error + ex.Message);
                    }
                }
            }

            return _rta;
        }

        public static string ImprimirFacturasCSinValidarFecha(Cabecera _cab, Cliente _cli, List<Items> _items,
            List<Descuentos> _descuentos, List<Pagos> _pagos, List<Promociones> _promos,
            string _ip, SqlConnection _sqlConn, bool _hasarLog, out bool _faltaPapel)
        {
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            string _serieFiscal2 = "";
            int _nroZeta = 0;
            int _nroComprobante = 0;
            DateTime _fechaCF = DateTime.Now;
            int _nroPos = 0;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);

                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;
                //Recupero la fecha del CF.
                _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                _nroPos = hasar.ConsultarDatosInicializacion().getNumeroPos();
                string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                //CArgamos los datos del cliente.
                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                    _cli.direccion, _alias, _serie_numero, _supedido);
                //Abrimos el Documento.
                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C);
                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                _serieFiscal2 = "011_Factura_C";

                foreach (Items _it in _items)
                {
                    //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                    if (_it._detalleModif == 0)
                    {
                        //Modif de hoy
                        var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                        foreach (Promociones _pr in _itPromo)
                        {
                            hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                        }

                        hasar.ImprimirItem(_it._descripcion,
                        Convert.ToDouble(_it._cantidad),
                        Convert.ToDouble(_it._precio),
                        hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                        Convert.ToDouble(_it._iva),
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                        0.0,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                        1, "", _it._codArticulo,
                        hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        //Veo si tengo dto en el item.
                        if (System.Math.Abs(_it._descuento) > 0)
                        {
                            decimal _porcentaje = System.Math.Abs(_it._descuento);
                            double _dto = Convert.ToDouble(((_it._cantidad * _it._precio) * _porcentaje / 100));
                            //Valido que tengo texto descuento.
                            //if (String.IsNullOrEmpty(_it._textoPromo))
                            //{
                            //    if (_it._descuento == 0)
                            //        _it._textoPromo = "Descuento ";
                            //    else
                            //        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                            //}
                            _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                            hasar.ImprimirDescuentoItem(_it._textoPromo,
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                        }
                    }
                }

                //Vemos si tenemos dto en la cabecera.
                foreach (Descuentos _dsc in _descuentos)
                {
                    if (_dsc._dtoComercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                            _dsc._totDtoComer,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_dsc._dtoPp != 0)
                    {
                        if (_dsc._dtoPp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                _dsc._totDtoPp,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_dsc._totDtoPp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }
                }

                //Pagos.
                foreach (Pagos _pag in _pagos)
                {
                    switch (_pag.descripcion.ToUpper())
                    {
                        case "TARJETA CRÉDITO":
                        case "TARJETA CREDITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                break;
                            }
                        case "PENDIENTE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                break;
                            }
                        case "EFECTIVO":
                            {
                                if (_pag.entregado == 0)
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                }
                                else
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.entregado),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                }
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "CHEQUE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                break;
                            }
                        case "TARJETA DEBITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                break;
                            }
                        default:
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");

                                break;
                            }
                    }
                }
                //Limpiamos
                hasar.ConfigurarZona(1, _estilo, "",
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                _respCerrarDoc = hasar.CerrarDocumento();

                int _numeroTicket = _respCerrarDoc.getNumeroComprobante();
                //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4,'0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                {
                    string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                    _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                }

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                {
                    _faltaPapel = true;
                }

                //Vemos si abrimos el cajon
                if (_abrirCajon)
                    hasar.AbrirCajonDinero();
                
            }
            catch (Exception ex)
            {
                if (_nroComprobante > 0)
                {
                    if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                        hasar.conectar(_ip);
                    if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        Impresiones.Cancelar(hasar);
                        _rta = "CANCELADA";
                    }
                    else
                        Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                }
                else
                {
                    string _error = "La venta quedará registrada PENDIENTE de fiscalizar. Revise el error y REINTENTE IMPRIMIR el ticket.";
                    Impresiones.Cancelar(hasar);
                    throw new Exception(_error + ex.Message);
                }
            }

            return _rta;
        }
        #endregion

        #region 2019 Nueva version.
        /// <summary>
        /// Imprime una Factura A/B validando que la fecha del comprobante no sea superior a 3 dias para atras.
        /// Se usa en la consola.
        /// </summary>
        /// <param name="_cab">Clase de la cabecera</param>
        /// <param name="_cli">Clase de cliente</param>
        /// <param name="_items">Lista de Items</param>
        /// <param name="_descuentos">Lista de descuentos</param>
        /// <param name="_pagos">Lista de Pagos</param>
        /// <param name="_promos">Lista de Promociones</param>
        /// <param name="_ip">Direccion IP del Controlador Hasar</param>
        /// <param name="_sqlConn">Conexion SQL</param>
        /// <param name="_hasarLog">Valor de SALIDA boleano que indica si escribe o no el log de hasara</param>
        /// <param name="_faltaPapel">Valor de SALIDA boleano que indica si el controlador se esta quedando sin papel</param>
        /// <param name="_nroComprobante">Valor de SALIDA con el numero del comprobante</param>
        /// <param name="_nroPos">Valor de SALIDA con el numero de Punto de Venta</param>
        /// <param name="_letraComprobante">Valor de SALIDA con la letra del comprobante</param>
        /// <returns>Mensaje de error.</returns>
        public static string ImprimirFacturasABSinValidarFecha2019(Cabecera _cab, Cliente _cli, List<Items> _items, List<Descuentos> _descuentos, 
            List<Pagos> _pagos, List<Promociones> _promos, string _ip, SqlConnection _sqlConn, bool _hasarLog, 
            out bool _faltaPapel, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            _letraComprobante = "";
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            int _nroZeta = 0;
            DateTime _fechaCF = DateTime.Now;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero la fecha del CF.
                _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;
                string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadRight(5, ' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                switch (_cli.regimenFacturacion)
                {
                    case "1":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "001_Factura_A";
                            _letraComprobante = "A";
                            break;
                        }
                    case "2":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "006_Factura_B";
                            _letraComprobante = "B";
                            break;
                        }
                    case "4":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "006_Factura_B";
                            _letraComprobante = "B";
                            break;
                        }
                    case "5":
                    case "6":
                        {
                            _serie_numero = _serie_numero + "    " + _supedido;
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _alias, _serie_numero);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "001_Factura_A";
                            _letraComprobante = "A";
                            break;
                        }
                    default:
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                _cli.direccion, _alias, _serie_numero, _supedido);
                            _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                            _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                            _serieFiscal2 = "006_Factura_B";
                            _letraComprobante = "B";
                            break;
                        }
                }
                //Recorro los items
                foreach (Items _it in _items)
                {
                    //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                    if (_it._detalleModif == 0)
                    {
                        //Modif de hoy
                        var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                        foreach (Promociones _pr in _itPromo)
                        {
                            hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                        }

                        if (_it._cantidad < 0 && _it._descuento != 0)
                        {
                            decimal _porcentaje = System.Math.Abs(_it._descuento);
                            double _dto = Convert.ToDouble(((1 * _it._precio) * _porcentaje / 100));
                            double _prDto = Convert.ToDouble(_it._precio) - _dto;
                            //hasar.ImprimirItem(_it._descripcion,
                            //    Convert.ToDouble(_it._cantidad),
                            //    Convert.ToDouble(_prDto),
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            //    Convert.ToDouble(_it._iva),
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            //    0.0,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            //    1, "", _it._codArticulo,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_it._cantidad),
                                Convert.ToDouble(_prDto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                Convert.ToDouble(_it._impuestoInterno),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        }
                        else
                        {
                            if (System.Math.Abs(_it._descuento) == 100)
                            {
                                decimal _porcentaje = System.Math.Abs(_it._descuento);
                                double _dto = Convert.ToDouble(((1 * _it._precio) * _porcentaje / 100));
                                double _prDto = Convert.ToDouble(_it._precio) - _dto;
                                //hasar.ImprimirItem(_it._descripcion,
                                //    Convert.ToDouble(_it._cantidad),
                                //    Convert.ToDouble(_prDto),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                //    Convert.ToDouble(_it._iva),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                //    0.0,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                //    1, "", _it._codArticulo,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                hasar.ImprimirItem(_it._descripcion,
                                   Convert.ToDouble(_it._cantidad),
                                   Convert.ToDouble(_prDto),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                   Convert.ToDouble(_it._iva),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                   Convert.ToDouble(_it._impuestoInterno),
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                   1, "", _it._codArticulo,
                                   hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                //hasar.ImprimirItem(_it._descripcion,
                                //    Convert.ToDouble(_it._cantidad),
                                //    Convert.ToDouble(_it._precio),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                //    Convert.ToDouble(_it._iva),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                //    0.0,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                //    1, "", _it._codArticulo,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_it._precio),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                    Convert.ToDouble(_it._impuestoInterno),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);

                                //Veo si tengo dto en el item.
                                if (System.Math.Abs(_it._descuento) > 0)
                                {
                                    decimal _porcentaje = System.Math.Abs(_it._descuento);
                                    double _dto = Convert.ToDouble(((Math.Abs(_it._cantidad) * _it._precio) * _porcentaje / 100));

                                    _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                                    hasar.ImprimirDescuentoItem(_it._textoPromo,
                                        _dto,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                }
                            }
                        }
                    }
                }
                //Vemos si tenemos dto en la cabecera.
                foreach (Descuentos _dsc in _descuentos)
                {
                    if (_dsc._dtoComercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                            _dsc._totDtoComer,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_dsc._dtoPp != 0)
                    {
                        if (_dsc._dtoPp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                _dsc._totDtoPp,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_dsc._totDtoPp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }
                }
                //Pagos.
                foreach (Pagos _pag in _pagos)
                {
                    switch (_pag.descripcion.ToUpper())
                    {
                        case "TARJETA CRÉDITO":
                        case "TARJETA CREDITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "PENDIENTE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "EFECTIVO":
                            {
                                if (_pag.entregado == 0)
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                }
                                else
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.entregado),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                }
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "CHEQUE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "TARJETA DEBITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        default:
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Convert.ToDouble(_pag.monto),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                break;
                            }
                    }
                }
                //Limpiamos
                hasar.ConfigurarZona(1, _estilo, " ",
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                //Cierro el tiquet
                _respCerrarDoc = hasar.CerrarDocumento();
                //Recupero el numero
                int _numeroTicket = _respCerrarDoc.getNumeroComprobante();
                //Valido si el comprobante se cancelo.
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                _estado = hasar.ConsultarEstado();
                if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                {
                    if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                    }
                    else
                    {
                        //Grabamos el nro del CF, la fecha y nro zeta.
                        if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                        {
                            string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                            _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                        }
                    }
                }                
                //Vemos si falta papel.
                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                {
                    _faltaPapel = true;
                }
                //Vemos si abrimos el cajon
                if (_abrirCajon)
                    hasar.AbrirCajonDinero();
            }
            catch (Exception ex)
            {
                //Grabamos el nro del comprobante con error.
                if (_nroComprobante > 0)
                {
                    try
                    {
                        if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                            hasar.conectar(_ip);
                        if (hasar.ObtenerUltimoEstadoFiscal().getDocumentoAbierto())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            Impresiones.Cancelar(hasar);
                            _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                            "Luego intente fiscalizarla nuevamente.";
                        }
                        else
                        {
                            if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                            {
                                //Disparo la cancelacion.
                                Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "CANCELADA";
                                throw new Exception(_rta + ex.Message);
                            }
                            else
                                Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                        }
                    }
                    catch (Exception exc)
                    {
                        if (MessageBox.Show(new Form { TopMost = true }, "El comprobante se imprimío correctamente?",
                            "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                        }
                        else
                        {
                            _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                            "Luego intente fiscalizarla nuevamente. Revise el error: ";
                            throw new Exception(_rta + exc.Message);
                        }
                    }
                }
                else
                {
                    _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                    throw new Exception(_rta + ex.Message);
                }
            }

            return _rta;
        }
        /// <summary>
        /// Imprime una Factura A/B 
        /// </summary>
        /// <param name="_cab">Clase de la cabecera</param>
        /// <param name="_cli">Clase del cliente</param>
        /// <param name="_items">Lista de los items</param>
        /// <param name="_descuentos">Lista de los descuentos</param>
        /// <param name="_pagos">Lista de Pagos </param>
        /// <param name="_promos">Lista de promociones</param>
        /// <param name="_ip">Dirección IP del controlador fiscal</param>
        /// <param name="_sqlConn">Conexion al SQL</param>
        /// <param name="_hasarLog">Valor boleano que indica si escribe el log</param>
        /// <param name="_faltaPapel">SALIDA. Valor boleano que indica la falta de papel</param>
        /// <param name="_necesitaCierreZ">SALIDA. Valor boleano que indica si necesita un cierre Z.</param>
        /// <param name="_nroComprobante">SALIDA. Numero del comprobante impreso</param>
        /// <param name="_nroPos">SALIDA. Numeo de punto de venta</param>
        /// <param name="_letraComprobante">SALIDA. Letra del comprobante.</param>
        /// <returns>Mensaje de Error.</returns>
        public static string ImprimirFacturasAB2019(Cabecera _cab, Cliente _cli, List<Items> _items,
           List<Descuentos> _descuentos, List<Pagos> _pagos, List<Promociones> _promos,
           string _ip, SqlConnection _sqlConn, bool _hasarLog,
           out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            _letraComprobante = "";
            bool _abrirCajon = false;
            string _rta = "";
            string _serieFiscal2 = "";
            _faltaPapel = false;
            _necesitaCierreZ = false;
            int _nroZeta = 0;
            DateTime _fechaCF = DateTime.Now;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);

            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero la fecha del CF.
                _fechaCF = FuncionesVarias.GetFechaCF(hasar);
                //Recupero el ultimo Z le sumo 1.
                _seta = hasar.ConsultarCapacidadZetas();
                _nroZeta = _seta.getUltimaZeta() + 1;
                //Valido la fecha.
                if (FuncionesVarias.FechaOk(hasar, _cab))
                {
                    string _alias = String.IsNullOrEmpty(_cab.aliasespera) ? "" : "ALIAS: " + _cab.aliasespera;
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    string _supedido = String.IsNullOrEmpty(_cab.supedido) ? "" : "Nro. PEDIDO: " + _cab.supedido;
                    hasar.ConfigurarZona(1, _estilo, "MESA: " + _cab.mesa.ToString() + string.Empty.PadLeft(5, ' ') + "VENDEDOR: " + _cab.nomVendedor,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    //Veo regimen de facturacion para saber cual 
                    switch (_cli.regimenFacturacion)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "001_Factura_A";
                                _letraComprobante = "A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "5":
                        case "6":
                            {
                                _serie_numero = _serie_numero + "    " + _supedido;
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ",""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                    "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _alias, _serie_numero);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "001_Factura_A";
                                _letraComprobante = "A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                                    _cli.direccion, _alias, _serie_numero, _supedido);
                                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B);
                                _nroComprobante = _respAbrirDoc.getNumeroComprobante();
                                _serieFiscal2 = "006_Factura_B";
                                _letraComprobante = "B";
                                break;
                            }
                    }

                    //Recorro los items
                    foreach (Items _it in _items)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            //Modif de hoy
                            var _itPromo = _promos.Where(x => x.numlinea == _it._numlinea);
                            foreach (Promociones _pr in _itPromo)
                            {
                                hasar.ImprimirTextoFiscal(_estilo, "Promo: " + _pr.textoImprimir);
                            }

                            if (_it._cantidad < 0 && _it._descuento != 0)
                            {
                                decimal _porcentaje = System.Math.Abs(_it._descuento);
                                double _dto = Convert.ToDouble(((1 * _it._precio) * _porcentaje / 100));
                                double _prDto = Convert.ToDouble(_it._precio) - _dto;
                                //hasar.ImprimirItem(_it._descripcion,
                                //    Convert.ToDouble(_it._cantidad),
                                //    Convert.ToDouble(_prDto),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                //    Convert.ToDouble(_it._iva),
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                //    0.0,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                //    1, "", _it._codArticulo,
                                //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                hasar.ImprimirItem(_it._descripcion,
                                    Convert.ToDouble(_it._cantidad),
                                    Convert.ToDouble(_prDto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    Convert.ToDouble(_it._iva),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                    Convert.ToDouble(_it._impuestoInterno),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    1, "", _it._codArticulo,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            }
                            else
                            {
                                if (System.Math.Abs(_it._descuento) == 100)
                                {
                                    decimal _porcentaje = System.Math.Abs(_it._descuento);
                                    double _dto = Convert.ToDouble(((1 * _it._precio) * _porcentaje / 100));
                                    double _prDto = Convert.ToDouble(_it._precio) - _dto;
                                    //hasar.ImprimirItem(_it._descripcion,
                                    //    Convert.ToDouble(_it._cantidad),
                                    //    Convert.ToDouble(_prDto),
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    //    Convert.ToDouble(_it._iva),
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    //    0.0,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    //    1, "", _it._codArticulo,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                    hasar.ImprimirItem(_it._descripcion,
                                        Convert.ToDouble(_it._cantidad),
                                        Convert.ToDouble(_prDto),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                        Convert.ToDouble(_it._iva),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                        Convert.ToDouble(_it._impuestoInterno),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                        1, "", _it._codArticulo,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                }
                                else
                                {
                                    //hasar.ImprimirItem(_it._descripcion,
                                    //    Convert.ToDouble(_it._cantidad),
                                    //    Convert.ToDouble(_it._precio),
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                    //    Convert.ToDouble(_it._iva),
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                                    //    0.0,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    //    1, "", _it._codArticulo,
                                    //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                                    hasar.ImprimirItem(_it._descripcion,
                                        Convert.ToDouble(_it._cantidad),
                                        Convert.ToDouble(_it._precio),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                        Convert.ToDouble(_it._iva),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                        Convert.ToDouble(_it._impuestoInterno),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                        1, "", _it._codArticulo,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);

                                    //Veo si tengo dto en el item.
                                    if (System.Math.Abs(_it._descuento) > 0)
                                    {
                                        decimal _porcentaje = System.Math.Abs(_it._descuento);
                                        double _dto = Convert.ToDouble(((Math.Abs(_it._cantidad) * _it._precio) * _porcentaje / 100));
                                        _it._textoPromo = "Total Promos " + _it._descuento.ToString();
                                        hasar.ImprimirDescuentoItem(_it._textoPromo,
                                            _dto,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                                    }
                                }
                            }
                        }
                    }
                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                _dsc._totDtoComer,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    _dsc._totDtoPp,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }
                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                    0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                    0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    if (_pag.entregado == 0)
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.monto),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    else
                                    {
                                        _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Convert.ToDouble(_pag.entregado),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    }
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                    0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                    0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Convert.ToDouble(_pag.monto),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    //_pag.descripcion,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                    0, "", "");
                                    break;
                                }
                        }
                    }
                    //Limpiamos
                    hasar.ConfigurarZona(1, _estilo, " ",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO);
                    //Cierro el documento.
                    _respCerrarDoc = hasar.CerrarDocumento();
                    //Recupero el numero del Ticket del documento cerrado.
                    int _numeroTicket = _respCerrarDoc.getNumeroComprobante();
                    //Vemos si se cancelo
                    if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y el comprobane en curso se cancelo. " + _numeroError + Environment.NewLine +
                            "Para imprimir correctamente este comprobante imprimalo desde la Consola.";
                    }
                    else
                    {
                        //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                        if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                        {
                            string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                            _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                        }
                    }
                    //Vemos si queda papel.
                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    {
                        _faltaPapel = true;
                    }
                    //Vemos si abrimos el cajon
                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();

                }
                else
                {
                    _rta = "Las fechas del Comprobate que quiere imprimir y de la controladora fiscal no coinciden." + Environment.NewLine +
                        "Intente imprimirla desde la consola o comuniquese con ICG Argentina.";
                }
            }
            catch (Exception ex)
            {
                //Vemos si el error fue porque necesita el Cierre Z.
                if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                {
                    _necesitaCierreZ = true;
                    _rta = "Cierre Z";
                }
                else
                {
                    //Grabamos el nro del comprobante con error.
                    if (_nroComprobante > 0)
                    {
                        try
                        {
                            //hasar.ConsultarEstado()..ConsultarUltimoError().
                            if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                hasar.conectar(_ip);
                            if (hasar.ObtenerUltimoEstadoFiscal().getDocumentoAbierto())
                            {
                                //Disparo la cancelacion.
                                Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente.";
                            }
                            else
                            {
                                if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                {
                                    //Disparo la cancelacion.
                                    Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    Impresiones.Cancelar(hasar);
                                    _rta = "CANCELADA";
                                }
                                else
                                    Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                            }
                        }
                        catch (Exception exc)
                        {
                            if (MessageBox.Show(new Form { TopMost = true }, "El comprobante se imprimío correctamente?",
                            "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                            }
                            else
                            {
                                _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente. Revise el error: ";
                                throw new Exception(_rta + exc.Message);
                            }
                        }
                    }
                    else
                    {
                        Impresiones.Cancelar(hasar);
                        _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + ex.Message);
                    }
                }
            }

            return _rta;
        }
        /// <summary>
        /// Imprime una NC de Credito A/B
        /// </summary>
        /// <param name="_cab">Clase de cabecera.</param>
        /// <param name="_cli">Clase de clientes</param>
        /// <param name="_items">Lista de Items.</param>
        /// <param name="_descuentos">Lista de descuentos.</param>
        /// <param name="_pagos">Lista de Pagos</param>
        /// <param name="_ip">Dirección IP del controlador fiscal.</param>
        /// <param name="_sqlConn">Conexion SQL</param>
        /// <param name="_hasarLog">Indica si se crea el log.</param>
        /// <param name="_faltaPapel">SALIDA. Indica si falta papel.</param>
        /// <returns>Mensaje de error.</returns>
        public static string ImprimirNotaCreditoABSinValidarFecha2019(Cabecera _cab, Cliente _cli,
    List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
    string _ip, SqlConnection _sqlConn, bool _hasarLog, out bool _faltaPapel)
        {
            int _nroComprobante = 0;
            int _nroPos = 0;
            string _serieFiscal2 = "";
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);

            try
            {
                string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                switch (_cli.regimenFacturacion)
                {
                    case "1":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                        _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "003_Credito_A";
                            break;
                        }
                    case "2":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                         hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                         hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                         _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "008_Credito_B";
                            break;
                        }
                    case "4":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                        _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "008_Credito_B";
                            break;
                        }
                    case "6":
                    case "5":
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                        "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _serie_numero, "");
                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "003_Credito_A";
                            break;
                        }
                    default:
                        {
                            hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                        _cli.direccion, _serie_numero, "", "");

                            _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                            _nroComprobante = _respAbrir.getNumeroComprobante();
                            _serieFiscal2 = "008_Credito_B";
                            break;
                        }
                }

                List<Items> _itemsOrdenados = _items.OrderBy(o => o._cantidad).ToList();
                foreach (Items _it in _itemsOrdenados)
                {
                    //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                    if (_it._detalleModif == 0)
                    {
                        decimal _newCantidad = _it._cantidad * -1;
                        //hasar.ImprimirItem(_it._descripcion,
                        //    Convert.ToDouble(_newCantidad),
                        //    Convert.ToDouble(_it._precio),
                        //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                        //    Convert.ToDouble(_it._iva),
                        //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                        //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                        //    0.0,
                        //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                        //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                        //    1, "", _it._codArticulo,
                        //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        hasar.ImprimirItem(_it._descripcion,
                            Convert.ToDouble(_newCantidad),
                            Convert.ToDouble(_it._precio),
                            hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            Convert.ToDouble(_it._iva),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                            Convert.ToDouble(_it._impuestoInterno),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            1, "", _it._codArticulo,
                            hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                        //Veo si tengo dto en el item.
                        if (System.Math.Abs(_it._descuento) > 0)
                        {
                            decimal _procentaje = System.Math.Abs(_it._descuento);
                            double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                            //Valido que tengo texto descuento.
                            if (String.IsNullOrEmpty(_it._textoPromo))
                            {
                                if (_it._descuento == 0)
                                    _it._textoPromo = "Descuento ";
                                else
                                    _it._textoPromo = "Descuento " + _it._descuento.ToString();
                            }
                            hasar.ImprimirDescuentoItem(_it._textoPromo,
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                        }
                    }
                }

                //Vemos si tenemos dto en la cabecera.
                foreach (Descuentos _dsc in _descuentos)
                {
                    if (_dsc._dtoComercial > 0)
                    {
                        hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                            Math.Abs(_dsc._totDtoComer),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                    }
                    if (_dsc._dtoPp != 0)
                    {
                        if (_dsc._dtoPp > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                Math.Abs(_dsc._totDtoPp),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        else
                        {
                            double _dto = Math.Abs(_dsc._totDtoPp);
                            hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                _dto,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                        }
                    }
                }

                //Pagos.
                foreach (Pagos _pag in _pagos)
                {
                    switch (_pag.descripcion.ToUpper())
                    {
                        case "TARJETA CRÉDITO":
                        case "TARJETA CREDITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                            0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "PENDIENTE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                            0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "EFECTIVO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                                    Math.Abs(Convert.ToDouble(_pag.monto)),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                    0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "CHEQUE":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                            0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        case "TARJETA DEBITO":
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                            0, "", "");
                                if (!_abrirCajon)
                                    _abrirCajon = _pag.AbrirCajon;
                                break;
                            }
                        default:
                            {
                                _respPago = hasar.ImprimirPago(_pag.descripcion,
                            Math.Abs(Convert.ToDouble(_pag.monto)),
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                            hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            "",
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                            0, "", "");
                                break;
                            }
                    }
                }
                _respCerrar = hasar.CerrarDocumento();
                int _numeroTicket = _respCerrar.getNumeroComprobante();
                //Valido si el comprobante se cancelo.
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                _estado = hasar.ConsultarEstado();
                if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                {
                    if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                    {
                        //Disparo la cancelacion.
                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                        string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                        _rta = "Hubo un Error y no se pudo Fiscalizar el comprobante, " + _numeroError + Environment.NewLine +
                            "Imprimalo desde la Consola.";
                    }
                    else
                    {
                        //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                        if (!Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                        {
                            string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                            _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                        }
                    }
                }

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;

                if (_abrirCajon)
                    hasar.AbrirCajonDinero();                
            }
            catch (Exception ex)
            {
                //Vemos si el error fue porque necesita el Cierre Z.
                if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                {
                    _rta = "Debe realizar un Cierre Z";
                }
                else
                {
                    //Grabamos el nro del comprobante con error.
                    if (_nroComprobante > 0)
                    {
                        try
                        {
                            if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                hasar.conectar(_ip);
                            if (hasar.ObtenerUltimoEstadoFiscal().getDocumentoAbierto())
                            {
                                //Disparo la cancelacion.
                                Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "La venta esta CANCELADA y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente.";
                            }
                            else
                            {
                                if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                {
                                    Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    Impresiones.Cancelar(hasar);
                                    _rta = "CANCELADA";
                                }
                                else
                                    Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                            }
                        }
                        catch (Exception exc)
                        {
                            Impresiones.Cancelar(hasar);
                            _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + exc.Message);
                        }
                    }
                    else
                    {
                        Impresiones.Cancelar(hasar);
                        _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + ex.Message);
                    }
                }
            }

            return _rta;
        }
        /// <summary>
        /// Imprime una Nota de Crédito A/B
        /// </summary>
        /// <param name="_cab">Clase de Cabecera</param>
        /// <param name="_cli">Clase de Clientes</param>
        /// <param name="_items">Lista de Items</param>
        /// <param name="_descuentos">Lista de descuentos.</param>
        /// <param name="_pagos">Lista de Pagos</param>
        /// <param name="_ip">Dirección IP del controlador fiscal</param>
        /// <param name="_sqlConn">Conexion SQL</param>
        /// <param name="_hasarLog">Indica si se genera el log.</param>
        /// <param name="_faltaPapel">SALIDA. Indica si falta papel</param>
        /// <param name="_necesitaCierreZ">SALIDA. Indica si necesita un cierre Z.</param>
        /// <param name="_nroComprobante">SALIDA. Numero del comprobante Impreso</param>
        /// <param name="_nroPos">SALIDA. Numero del punto de venta.</param>
        /// <param name="_letraComprobante">SALIDA. Letra del comprobante.</param>
        /// <returns></returns>
        public static string ImprimirNotaCreditoAB2019(Cabecera _cab, Cliente _cli,
           List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
           string _ip, SqlConnection _sqlConn, bool _hasarLog,
           out bool _faltaPapel, out bool _necesitaCierreZ, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            _letraComprobante = "";
            string _serieFiscal2 = "";
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            _necesitaCierreZ = false;
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    switch (_cli.regimenFacturacion)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                             _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                        "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _serie_numero, "");
                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                    }

                    List<Items> _itemsOrdenados = _items.OrderBy(o => o._cantidad).ToList();
                    foreach (Items _it in _itemsOrdenados)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            decimal _newCantidad = _it._cantidad * -1;
                            //hasar.ImprimirItem(_it._descripcion,
                            //    Convert.ToDouble(_newCantidad),
                            //    Convert.ToDouble(_it._precio),
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            //    Convert.ToDouble(_it._iva),
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            //    0.0,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            //    1, "", _it._codArticulo,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_newCantidad),
                                Convert.ToDouble(_it._precio),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                Convert.ToDouble(_it._impuestoInterno),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _procentaje = System.Math.Abs(_it._descuento);
                                double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                                //Valido que tengo texto descuento.
                                if (String.IsNullOrEmpty(_it._textoPromo))
                                {
                                    if (_it._descuento == 0)
                                        _it._textoPromo = "Descuento ";
                                    else
                                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                }
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                Math.Abs(_dsc._totDtoComer),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    Math.Abs(_dsc._totDtoPp),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        //_pag.descripcion,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                    }

                    //_subTotal = hasar.ConsultarSubtotal();

                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();
                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            string _numeroError = _nroPos.ToString().PadLeft(4, '0') + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                            _rta = "Hubo un Error y no se pudo Fiscalizar el comprobante, " + _numeroError + Environment.NewLine +
                                "Imprimalo desde la Consola.";
                        }
                        else
                        {
                            //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                            if (!Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                            {
                                string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                                _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                            }
                        }
                    }
                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();

                    
                }
                catch (Exception ex)
                {
                    //Vemos si el error fue porque necesita el Cierre Z.
                    if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                    {
                        _necesitaCierreZ = true;
                        _rta = "Cierre Z";
                    }
                    else
                    {
                        //Grabamos el nro del comprobante con error.
                        if (_nroComprobante > 0)
                        {
                            try
                            {
                                if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                    hasar.conectar(_ip);
                                if (hasar.ObtenerUltimoEstadoFiscal().getDocumentoAbierto())
                                {
                                    //Disparo la cancelacion.
                                    Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    Impresiones.Cancelar(hasar);
                                    _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente.";
                                }
                                else
                                {
                                    if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                    {
                                        Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                        _rta = "CANCELADA. Error: " + ex.Message;
                                        Impresiones.Cancelar(hasar);
                                    }
                                    else
                                        Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                                }
                            }
                            catch (Exception exc)
                            {
                                if (MessageBox.Show(new Form { TopMost = true }, "El comprobante se imprimío correctamente?",
                            "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    Cabecera.TransaccionOK(_cab, _nroZeta, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                                }
                                else
                                {
                                    _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente. Revise el error: ";
                                    throw new Exception(_rta + exc.Message);
                                }
                                //_rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                //    "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                                //throw new Exception(_rta + exc.Message);
                            }
                        }
                        else
                        {
                            _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + ex.Message);
                        }
                    }
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        public static string ImprimirNotaCreditoAB2019(Cabecera _cab, Cliente _cli,
    List<Items> _items, List<Descuentos> _descuentos, List<Pagos> _pagos,
    string _ip, SqlConnection _sqlConn, bool _hasarLog,
    out bool _faltaPapel, out int _nroComprobante, out int _nroPos, out string _letraComprobante)
        {
            _nroComprobante = 0;
            _nroPos = 0;
            string _serieFiscal2 = "";
            bool _abrirCajon = false;
            string _rta = "";
            _faltaPapel = false;
            _letraComprobante = "";
            int _nroZeta = 0;

            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas _seta = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas();

            //Abrimos el log.
            if (_hasarLog)
                hasar.archivoRegistro("HasarLog.log");
            //Conectamos con la hasar.
            hasar.conectar(_ip);
            //Recupero los datos de la Inicializacion.
            _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
            _nroPos = _respConsultarDatosInicializacion.getNumeroPos();
            string _numeroPos = hasar.ConsultarDatosInicializacion().getNumeroPos().ToString().PadLeft(4, '0');
            //Recupero la fecha del CF.
            DateTime _fechaCF = FuncionesVarias.GetFechaCF(hasar);
            //Recupero el ultimo Z le sumo 1.
            _seta = hasar.ConsultarCapacidadZetas();
            _nroZeta = _seta.getUltimaZeta() + 1;

            if (FuncionesVarias.FechaOk(hasar, _cab))
            {
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrir = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrar = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _subTotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();

                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);
                _estilo.setCentrado(false);
                _estilo.setBorradoTexto(false);

                try
                {
                    string _serie_numero = "Numero Interno: " + _cab.serie + "/" + _cab.numero.ToString();
                    switch (_cli.regimenFacturacion)
                    {
                        case "1":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        case "2":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_EXENTO,
                             hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                             _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "4":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                        case "6":
                        case "5":
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", "").Replace(" ", ""),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT,
                                        "Receptor del comprobante - Responsable Monotributo", _cli.direccion, _serie_numero, "");
                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "003_Credito_A";
                                _letraComprobante = "A";
                                break;
                            }
                        default:
                            {
                                hasar.CargarDatosCliente(_cli.nombre, _cli.documento.Replace("-", ""),
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL,
                            hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI,
                            _cli.direccion, _serie_numero, "", "");

                                _respAbrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B);
                                _nroComprobante = _respAbrir.getNumeroComprobante();
                                _serieFiscal2 = "008_Credito_B";
                                _letraComprobante = "B";
                                break;
                            }
                    }

                    List<Items> _itemsOrdenados = _items.OrderBy(o => o._cantidad).ToList();
                    foreach (Items _it in _itemsOrdenados)
                    {
                        //Si tengo detalleModif = 1 no lo imprimo, ya que es parte de un menu
                        if (_it._detalleModif == 0)
                        {
                            decimal _newCantidad = _it._cantidad * -1;
                            //hasar.ImprimirItem(_it._descripcion,
                            //    Convert.ToDouble(_newCantidad),
                            //    Convert.ToDouble(_it._precio),
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                            //    Convert.ToDouble(_it._iva),
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_KIVA,
                            //    0.0,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                            //    1, "", _it._codArticulo,
                            //    hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            hasar.ImprimirItem(_it._descripcion,
                                Convert.ToDouble(_newCantidad),
                                Convert.ToDouble(_it._precio),
                                hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO,
                                Convert.ToDouble(_it._iva),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL,
                                Convert.ToDouble(_it._impuestoInterno),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                1, "", _it._codArticulo,
                                hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD);
                            //Veo si tengo dto en el item.
                            if (System.Math.Abs(_it._descuento) > 0)
                            {
                                decimal _procentaje = System.Math.Abs(_it._descuento);
                                double _dto = Math.Abs(Convert.ToDouble(((_it._cantidad * _it._precio) * _procentaje / 100)));
                                //Valido que tengo texto descuento.
                                if (String.IsNullOrEmpty(_it._textoPromo))
                                {
                                    if (_it._descuento == 0)
                                        _it._textoPromo = "Descuento ";
                                    else
                                        _it._textoPromo = "Descuento " + _it._descuento.ToString();
                                }
                                hasar.ImprimirDescuentoItem(_it._textoPromo,
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE);
                            }
                        }
                    }

                    //Vemos si tenemos dto en la cabecera.
                    foreach (Descuentos _dsc in _descuentos)
                    {
                        if (_dsc._dtoComercial > 0)
                        {
                            hasar.ImprimirAjuste("Bonificación Gral. (comercial) " + _dsc._dtoComercial.ToString(),
                                Math.Abs(_dsc._totDtoComer),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                        }
                        if (_dsc._dtoPp != 0)
                        {
                            if (_dsc._dtoPp > 0)
                            {
                                hasar.ImprimirAjuste("Bonificación DTOPP " + _dsc._dtoPp.ToString(),
                                    Math.Abs(_dsc._totDtoPp),
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL);
                            }
                            else
                            {
                                double _dto = Math.Abs(_dsc._totDtoPp);
                                hasar.ImprimirAjuste("Recargo DTOPP " + _dsc._dtoPp.ToString(),
                                    _dto,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                    hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_BASE,
                                    "",
                                    hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_POS);
                            }
                        }
                    }

                    //Pagos.
                    foreach (Pagos _pag in _pagos)
                    {
                        switch (_pag.descripcion.ToUpper())
                        {
                            case "TARJETA CRÉDITO":
                            case "TARJETA CREDITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                //_pag.descripcion,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_CREDITO,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "PENDIENTE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CUENTA_CORRIENTE,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "EFECTIVO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                        Math.Abs(Convert.ToDouble(_pag.monto)),
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                        hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                        "",
                                        hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO,
                                        0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "CHEQUE":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            case "TARJETA DEBITO":
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.TARJETA_DE_DEBITO,
                                0, "", "");
                                    if (!_abrirCajon)
                                        _abrirCajon = _pag.AbrirCajon;
                                    break;
                                }
                            default:
                                {
                                    _respPago = hasar.ImprimirPago(_pag.descripcion,
                                Math.Abs(Convert.ToDouble(_pag.monto)),
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR,
                                hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO,
                                "",
                                hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.OTROS_MEDIOS_PAGO,
                                0, "", "");
                                    break;
                                }
                        }
                    }

                    _respCerrar = hasar.CerrarDocumento();

                    int _numeroTicket = _respCerrar.getNumeroComprobante();
                    //Valido si el comprobante se cancelo.
                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _estado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();
                    _estado = hasar.ConsultarEstado();
                    if (_estado.getNumeroUltimoComprobante() == _nroComprobante)
                    {
                        if (_estado.EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                        {
                            //Disparo la cancelacion.
                            Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                            _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                    "Luego intente fiscalizarla nuevamente.";
                        }
                    }
                    else
                    {
                        //Grabamos el nro del CF, la fecha, nro zeta y grabo en rem_transacciones.
                        if (!Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _numeroTicket, _fechaCF, _sqlConn))
                        {
                            string _numeroError = _numeroPos + "-" + _numeroTicket.ToString().PadLeft(8, '0');
                            _rta = "Hubo un Error y no se pudo grabar el numero de comprobante, " + _numeroError + ", por favor ingreselo manualmente.";
                        }
                    }
                    if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                        _faltaPapel = true;

                    if (_abrirCajon)
                        hasar.AbrirCajonDinero();

                   
                }
                catch (Exception ex)
                {
                    //Grabamos el nro del comprobante con error.
                    if (_nroComprobante > 0)
                    {
                        try
                        {
                            if (hasar.ObtenerUltimoEstadoImpresora().getImpresoraOffLine())
                                hasar.conectar(_ip);
                            if (hasar.ObtenerUltimoEstadoFiscal().getDocumentoAbierto())
                            {
                                //Disparo la cancelacion.
                                Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                Impresiones.Cancelar(hasar);
                                _rta = "La venta se cancelo y NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente.";
                            }
                            else
                            {
                                if (hasar.ConsultarEstado().EstadoAuxiliar.getUltimoComprobanteFueCancelado())
                                {
                                    Cancelaciones.TransaccionCancelacion(_cab, _nroPos.ToString().PadLeft(4, '0'), _serieFiscal2, _nroComprobante, _nroZeta, _sqlConn);
                                    _rta = "CANCELADA";
                                    Impresiones.Cancelar(hasar);
                                }
                                else
                                    Cabecera.TransaccionOK(_cab, _nroZeta, _numeroPos, _serieFiscal2, _nroComprobante, _fechaCF, _sqlConn);
                            }
                        }
                        catch (Exception exc)
                        {
                            _rta = "La venta NO SE FISCALIZO por problemas de conexion con el controlador fiscal." + Environment.NewLine +
                                "Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                            throw new Exception(_rta + exc.Message);
                        }
                    }
                    else
                    {
                        _rta = "La venta NO SE FISCALIZO. Luego intente fiscalizarla nuevamente. Revise el error. Error: ";
                        throw new Exception(_rta + ex.Message);
                    }
                }
            }
            else
                _rta = "Las fechas del sistema y de la controladora fiscal no coinciden, debe realizar un Cierre Z. Por favor comuniquese con ICG Argentina.";

            return _rta;
        }

        #endregion

        public static void ImprimirTicketRegalo(Cabecera _cab, List<Items> _items, string _ip
            , string _txtRegalo1, string _txtRegalo2, string _txtRegalo3,
            string _comprobante, bool _hasarLog, out bool _faltaPapel)
        {
            //string _rta = "";
            _faltaPapel = false;
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            //Hasar.
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento _respAbrirDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento _respCerrarDoc = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago _respPago = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal _respConsultaSubtotal = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal();
            _estilo.setNegrita(false);
            _estilo.setDobleAncho(false);
            _estilo.setCentrado(false);
            _estilo.setBorradoTexto(false);

            try
            {
                //Abrimos el log.
                if(_hasarLog)
                    hasar.archivoRegistro("HasarLog.log"); //todo
                //Conectamos con la hasar.
                hasar.conectar(_ip);

                //Abro el documento.
                _respAbrirDoc = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO);

                _estilo.setNegrita(true);
                //Titulo.
                _estilo.setDobleAncho(true);
                _estilo.setNegrita(true);
                _estilo.setCentrado(true);
                hasar.ImprimirTextoGenerico(_estilo, "Ticket Regalo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                hasar.ImprimirTextoGenerico(_estilo, "Comprobante: " + _comprobante, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setCentrado(false);
                _estilo.setNegrita(false);
                _estilo.setDobleAncho(false);

                //Cabecera de Items.
                hasar.ImprimirTextoGenerico(_estilo, "Descripcion Articulo", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //Linea separadora.
                hasar.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Recorro los items.
                decimal _cantidad = 0;
                foreach (Items _it in _items)
                {
                    //Imprimo items
                    hasar.ImprimirTextoGenerico(_estilo, _it._descripcion, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                    _cantidad = _cantidad + _it._cantidad;
                }
                _estilo.setNegrita(true);
                //Linea separadora.
                hasar.ImprimirTextoGenerico(_estilo, "----------------------------------------------------------------", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                hasar.ImprimirTextoGenerico(_estilo, "Total unidades: " + Convert.ToInt32(_cantidad).ToString(), hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                _estilo.setNegrita(false);
                //Imprimo texto de regalo.
                //_estilo.setDobleAncho(true);
                if (!String.IsNullOrEmpty(_txtRegalo1))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo2))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo2, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                if (!String.IsNullOrEmpty(_txtRegalo3))
                    hasar.ImprimirTextoGenerico(_estilo, _txtRegalo3, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO);
                //_estilo.setDobleAncho(false);

                _respCerrarDoc = hasar.CerrarDocumento();

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;
                //_rta = "La impresora fiscal informa que se esta quedando sin papel. Por favor revise el papel.";
            }
            catch (Exception ex)
            {
                Impresiones.Cancelar(hasar);
                throw new Exception(ex.Message);
            }
        }

        public static string Reimprimir(int _nro, string _descrip, string _ip, out bool _faltaPapel)
        {
            string _rta = "";
            _faltaPapel = false;
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
           
            hasar.conectar(_ip);

            try
            {
                switch (_descrip.Substring(0, 3))
                {
                    //case "NOTA DE CREDITO A":
                    case "003":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A, _nro);
                            break;
                        }
                    //case "NOTA DE CREDITO B":
                    case "008":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B, _nro);
                            break;
                        }
                    //case "TIQUET FACTURA A":
                    case "001":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, _nro);
                            break;
                        }
                    //case "TIQUET FACTURA B":
                    case "006":
                        {
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B, _nro);
                            break;
                        }
                    case "002":
                        {
                            //Nota de debito A
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_A, _nro);
                            break;
                        }
                    case "007":
                        {
                            //Nota de debito A
                            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_B, _nro);
                            break;
                        }
                    default:
                        {
                            _rta = "Tipo de Documento no configurado para su fiscalización.";
                            break;
                        }
                }

                if (hasar.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
                    _faltaPapel = true;
                //_rta = "La impresora fiscal informa que se esta quedando sin papel. Por favor revise el papel.";
            }
            catch (Exception ex)
            {
                Cancelar(hasar);
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static void Cancelar(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal _estadoFiscal = new hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado _respConsultarEstado = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado();


            _respConsultarEstado = _cf.ConsultarEstado();
            _estadoFiscal = _cf.ObtenerUltimoEstadoFiscal();

            if (_estadoFiscal.getDocumentoAbierto())
            {

                try
                {
                    _cf.Cancelar();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
            }
        }

        public static void Conectar(hfl.argentina.HasarImpresoraFiscalRG3561 _cf, bool _hasarLog, string _ip)
        {
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            try
            {
                //Abrimos el log.
                if (_hasarLog)
                    _cf.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                _cf.conectar(_ip);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo conectar con la Controladora Fiscal. Error:" + ex.Message);
            }
        }
    }
}
