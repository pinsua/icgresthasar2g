﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using RestHasar2G;

namespace IcgFrontRest2019H2G
{
    static class Program
    {
        /// <summary>f
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //aca Comienzo
            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
            //Estilo de Texto.
            hfl.argentina.Hasar_Funcs.AtributosDeTexto _estilo = new hfl.argentina.Hasar_Funcs.AtributosDeTexto();

            string _server = "";
            string _database = "";
            string _user = "";
            string _codVendedor = "";
            string _tipodoc = "";
            string _serie = "";
            string _numero = "";
            string _n = "";
            string _fo = "";

            string _ip = "";
            string _caja = "";
            bool _monotributo = false;
            bool _hasarLog = false;
            string _terminal = Environment.MachineName;
            string _codigoFudex = "";

            InfoIrsa _infoIrsa = new InfoIrsa();
            int _numeroComprobante = 0;
            int _numeroPOS = 0;
            string _letraComprobante = "";
            string _tipoComprobante = "";
            //SiTef
            SiTefService.InfoSitef _inforSiTef = new SiTefService.InfoSitef();

            if (File.Exists("IMPRIME.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("IMPRIME.xml");

                XmlNodeList _List1 = xDoc.SelectNodes("/doc/bd");
                foreach (XmlNode xn in _List1)
                {
                    _server = xn["server"].InnerText;
                    _database = xn["database"].InnerText;
                    _user = xn["user"].InnerText;
                }

                XmlNodeList _List2 = xDoc.SelectNodes("/doc");
                foreach (XmlNode xn in _List2)
                {
                    _codVendedor = xn["codvendedor"].InnerText;
                    _tipodoc = xn["tipodoc"].InnerText;
                    _serie = xn["serie"].InnerText;
                    _numero = xn["numero"].InnerText;
                    _n = xn["n"].InnerText;
                    _fo = xn["fo"].InnerText;
                }
                //Armamos el stringConnection.
                string strConnection = "Data Source=" + _server + ";Initial Catalog=" + _database + ";User Id=" + _user + ";Password=masterkey;";
                //Solo imprimimos las que N = B
                if (_n.ToUpper() == "B")
                {
                    //Obtenemos el primer digito de la serie.
                    string _digitoSerie = _serie.Substring(0, 1);                    
                    //Solo imprimimos las que poseen el primer digito de la serie = F
                    if (_digitoSerie != "G" && _digitoSerie != "S" && _digitoSerie != "I")
                    {
                        //Leemos el archivo de configuracion
                        if (RestHasar2G.FuncionesVarias.LeerXmlConfiguracionPlugin(out _ip, out _caja, out _monotributo, out _hasarLog, 
                            out _codigoFudex, out _infoIrsa, out _inforSiTef))
                        {
                            //Conectamos.
                            using (SqlConnection _connection = new SqlConnection(strConnection))
                            {
                                try
                                {
                                    //Abrimos la conexion con la base de datos.
                                    _connection.Open();
                                    //Busco el comprobante.
                                    RestHasar2G.Cabecera _cabecera = RestHasar2G.Cabecera.GetCabecera(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, Convert.ToInt32(_codVendedor), _connection);

                                    //Vemos si no existe y la imprimos, si existe la ReImprimimos.
                                    if (_cabecera.numeroFiscal == 0 && String.IsNullOrEmpty(_cabecera.serieFiscal2))
                                    {
                                        if (_cabecera.serie != null)
                                        {
                                            //Recuperamos los datos de la cabecera.
                                            Cliente _cliente = Cliente.GetCliente(_cabecera.codcliente, _connection);
                                            List<Items> _items = Items.GetItems(_cabecera.serie, _cabecera.n, _cabecera.numero, _connection);
                                            var _Fudex = _items.Where(x => x._codArticulo == _codigoFudex).Count();
                                            if (_Fudex == 0)
                                            {
                                                if (_items.Count > 0)
                                                {
                                                    List<Promociones> _promociones = Promociones.GetPromociones(_cabecera.serie, _cabecera.numero, _cabecera.n, _connection);
                                                    List<Pagos> _pagos = Pagos.GetPagos(_cabecera.serie, _cabecera.numero, _cabecera.n, _connection);
                                                    List<Descuentos> _descuentos = Descuentos.GetDescuentos(_cabecera.fo, _cabecera.serie, _cabecera.numero, _cabecera.n, _connection);

                                                    if (_pagos.Count > 0)
                                                    {
                                                        if (_pagos.Count < 6)
                                                        {
                                                            //si el cliente es Hardcode los valores.
                                                            if (_cabecera.codcliente == 0)
                                                            {
                                                                _cliente.direccion = ".";
                                                                _cliente.regimenFacturacion = "4";
                                                                _cliente.tipoDoc = "96_DNI";
                                                                _cliente.nombre = "Consumidor Final";
                                                                _cliente.documento = "11111111";
                                                            }

                                                            if (_cabecera.totalbruto < 0)
                                                            {
                                                                bool _cuitOK = true;

                                                                //Vemos si debemos validar el cuit.
                                                                if (_cliente.regimenFacturacion == "1")
                                                                {
                                                                    int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                                    int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                                    if (_digitorecibido == _digitoValidador)
                                                                        _cuitOK = true;
                                                                    else
                                                                        _cuitOK = false;
                                                                }
                                                                else
                                                                {
                                                                    if (String.IsNullOrEmpty(_cliente.documento))
                                                                        _cliente.documento = "0";
                                                                }
                                                                //Validamos el campo direccion del cliente.
                                                                if (String.IsNullOrEmpty(_cliente.direccion))
                                                                    _cliente.direccion = ".";
                                                                if (_cuitOK)
                                                                {
                                                                    bool _faltaPapel;
                                                                    string _respImpre;
                                                                    bool _necesitaCierreZ = false;

                                                                    if (!_monotributo)
                                                                    {
                                                                        _respImpre = Impresiones.ImprimirNotaCreditoAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                            out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                        _tipoComprobante = "C";
                                                                    }
                                                                    else
                                                                    {
                                                                        _respImpre = Impresiones.ImprimirNotaCreditoC(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                            out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS);
                                                                        _letraComprobante = "C";
                                                                        _tipoComprobante = "C";
                                                                    }

                                                                    if (_necesitaCierreZ)
                                                                    {
                                                                        string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                        //Preguntamos.
                                                                        if (MessageBox.Show(new Form { TopMost = true },_msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                        {
                                                                            //Sacamos el Cierre Z.
                                                                            Cierres.ImprimirCierreZ(_ip, _hasarLog, Convert.ToInt32(_caja), _connection);
                                                                            //Volvemos a Lanzar la impresion
                                                                            if (!_monotributo)
                                                                                _respImpre = Impresiones.ImprimirNotaCreditoAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                                    out _faltaPapel, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                            else
                                                                            {
                                                                                _respImpre = Impresiones.ImprimirNotaCreditoC(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                                    out _faltaPapel, out _numeroComprobante, out _numeroPOS);
                                                                                _letraComprobante = "C";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            //Cambiar texto de _respImprime.
                                                                            _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                        }
                                                                    }

                                                                    if (!String.IsNullOrEmpty(_respImpre))
                                                                        MessageBox.Show(new Form { TopMost = true },_respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                    if (_faltaPapel)
                                                                        MessageBox.Show(new Form { TopMost = true },"La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                    //Vemos si tenemos que lanzar IRSA.
                                                                    if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                                    {
                                                                        //LanzarTrancomp(_pathIrsa);
                                                                        string _compro = _numeroPOS.ToString().PadLeft(4, '0') + "-" + _numeroComprobante.ToString().PadLeft(8, '0');
                                                                        RestHasar2G.Irsa.LanzarTrancomp(_cabecera, _cliente, _pagos, _infoIrsa, _tipoComprobante, _compro, _connection);
                                                                    }
                                                                    //SiTef
                                                                    if (!String.IsNullOrEmpty(_inforSiTef.pathSendInvoice))
                                                                    {
                                                                        if (Directory.Exists(_inforSiTef.pathSendInvoice))
                                                                        {
                                                                            //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                            //    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                            SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                                    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                        }
                                                                        else
                                                                            MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                    }
                                                                }
                                                                else
                                                                    MessageBox.Show(new Form { TopMost = true },"El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            }
                                                            else
                                                            {
                                                                bool _cuitOK = true;
                                                                //Vemos si debemos validar el cuit.
                                                                if (_cliente.regimenFacturacion == "1")
                                                                {
                                                                    int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                                    int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                                    if (_digitorecibido == _digitoValidador)
                                                                        _cuitOK = true;
                                                                    else
                                                                        _cuitOK = false;
                                                                }
                                                                else
                                                                {
                                                                    if (String.IsNullOrEmpty(_cliente.documento))
                                                                        _cliente.documento = "11111111";
                                                                }

                                                                //Validamos el campo direccion del cliente.
                                                                if (String.IsNullOrEmpty(_cliente.direccion))
                                                                    _cliente.direccion = ".";
                                                                if (_cuitOK)
                                                                {
                                                                    bool _faltaPapel;
                                                                    string _respImpre;
                                                                    bool _necesitaCierreZ = false;

                                                                    if (!_monotributo)
                                                                    {
                                                                        _respImpre = Impresiones.ImprimirFacturasAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                            out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                        _tipoComprobante = "D";
                                                                    }
                                                                    else
                                                                    {
                                                                        _respImpre = Impresiones.ImprimirFacturasC(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                            out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS);
                                                                        _letraComprobante = "C";
                                                                        _tipoComprobante = "D";
                                                                    }

                                                                    if (_necesitaCierreZ)
                                                                    {
                                                                        string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                        //Preguntamos.
                                                                        if (MessageBox.Show(new Form { TopMost = true },_msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                        {
                                                                            //Sacamos el Cierre Z.
                                                                            Cierres.ImprimirCierreZ(_ip, _hasarLog, Convert.ToInt32(_caja), _connection);
                                                                            //Volvemos a Lanzar la impresion
                                                                            if (!_monotributo)
                                                                                _respImpre = Impresiones.ImprimirFacturasAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                                    out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                            else
                                                                            {
                                                                                _respImpre = Impresiones.ImprimirFacturasC(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                                    out _faltaPapel, out _numeroComprobante, out _numeroPOS);
                                                                                _letraComprobante = "C";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            //Cambiar texto de _respImprime.
                                                                            _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                        }
                                                                    }

                                                                    if (!String.IsNullOrEmpty(_respImpre))
                                                                        MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                    if (_faltaPapel)
                                                                        MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                    //Vemos si tenemos que lanzar IRSA
                                                                    if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                                    {
                                                                        //LanzarTrancomp(_pathIrsa);
                                                                        string _compro = _numeroPOS.ToString().PadLeft(4, '0') + "-" + _numeroComprobante.ToString().PadLeft(8, '0');
                                                                        RestHasar2G.Irsa.LanzarTrancomp(_cabecera, _cliente, _pagos, _infoIrsa, _tipoComprobante, _compro, _connection);
                                                                    }
                                                                    //SiTef
                                                                    if (!String.IsNullOrEmpty(_inforSiTef.pathSendInvoice))
                                                                    {
                                                                        if (Directory.Exists(_inforSiTef.pathSendInvoice))
                                                                        {
                                                                            //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                            //    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                            SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                                 _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                        }
                                                                        else
                                                                            MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                    }
                                                                }
                                                                else
                                                                    MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                            }

                                                            if (RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_connection, _caja))
                                                            {
                                                                MessageBox.Show(new Form { TopMost = true }, "Existen Comprobantes sin fiscalizar." +
                                                                    Environment.NewLine + "Por favor fiscalizelos desde la consola.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            MessageBox.Show(new Form { TopMost = true }, "No puede haber mas de 5 formas de pago." + Environment.NewLine +
                                                                "Antes de fiscalizar por consola debe modificar la venta desde Ventas opción Modificar." + Environment.NewLine +
                                                                "Luego de fizcalizarla puede volver a modificar con las forma de pago que quiera",
                                                                           "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, "No existen Items registrados. Por favor revise el comprobante.",
                                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_tipodoc.ToUpper() == "ABONO")
                                        {
                                            //Recuperamos los datos de la cabecera.
                                            Cliente _cliente = Cliente.GetCliente(_cabecera.codcliente, _connection);
                                            List<Items> _items = Items.GetItems(_cabecera.serie, _cabecera.n, _cabecera.numero, _connection);
                                            if (_items.Count > 0)
                                            {
                                                List<Promociones> _promociones = Promociones.GetPromociones(_cabecera.serie, _cabecera.numero, _cabecera.n, _connection);
                                                List<Pagos> _pagos = Pagos.GetPagos(_cabecera.serie, _cabecera.numero, _cabecera.n, _connection);
                                                List<Descuentos> _descuentos = Descuentos.GetDescuentos(_cabecera.fo, _cabecera.serie, _cabecera.numero, _cabecera.n, _connection);

                                                if (_pagos.Count > 0)
                                                {
                                                    if (_pagos.Count < 6)
                                                    {
                                                        //si el cliente es 0 Hardcode los valores.
                                                        if (_cabecera.codcliente == 0)
                                                        {
                                                            _cliente.direccion = ".";
                                                            _cliente.regimenFacturacion = "4";
                                                            _cliente.tipoDoc = "96_DNI";
                                                            _cliente.nombre = "Consumidor Final";
                                                            _cliente.documento = "0";
                                                        }
                                                        if (_cabecera.totalbruto < 0)
                                                        {
                                                            bool _cuitOK = true;
                                                            //Vemos si debemos validar el cuit.
                                                            if (_cliente.regimenFacturacion == "1")
                                                            {
                                                                int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                                int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                                if (_digitorecibido == _digitoValidador)
                                                                    _cuitOK = true;
                                                                else
                                                                    _cuitOK = false;
                                                            }
                                                            else
                                                            {
                                                                if (String.IsNullOrEmpty(_cliente.documento))
                                                                    _cliente.documento = "11111111";
                                                            }
                                                            //Validamos el campo direccion del cliente.
                                                            if (String.IsNullOrEmpty(_cliente.direccion))
                                                                _cliente.direccion = ".";
                                                            if (_cuitOK)
                                                            {
                                                                bool _faltaPapel;
                                                                string _respImpre;
                                                                bool _necesitaCierreZ = false;

                                                                if (!_monotributo)
                                                                {
                                                                    _respImpre = Impresiones.ImprimirNotaCreditoAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                        out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                    _tipoComprobante = "C";
                                                                }
                                                                else
                                                                {
                                                                    _respImpre = Impresiones.ImprimirNotaCreditoC(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                        out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS);
                                                                    _letraComprobante = "C";
                                                                    _tipoComprobante = "C";
                                                                }

                                                                if (_necesitaCierreZ)
                                                                {
                                                                    string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                    //Preguntamos.
                                                                    if (MessageBox.Show(new Form { TopMost = true }, _msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                    {
                                                                        //Sacamos el Cierre Z.
                                                                        Cierres.ImprimirCierreZ(_ip, _hasarLog, Convert.ToInt32(_caja), _connection);
                                                                        //Volvemos a Lanzar la impresion
                                                                        if (!_monotributo)
                                                                            _respImpre = Impresiones.ImprimirNotaCreditoAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                                out _faltaPapel, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                        else
                                                                        {
                                                                            _respImpre = Impresiones.ImprimirNotaCreditoC(_cabecera, _cliente, _items, _descuentos, _pagos, _ip, _connection, _hasarLog,
                                                                                out _faltaPapel, out _numeroComprobante, out _numeroPOS);
                                                                            _letraComprobante = "C";
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        //Cambiar texto de _respImprime.
                                                                        _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                    }
                                                                }

                                                                if (!String.IsNullOrEmpty(_respImpre))
                                                                    MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                if (_faltaPapel)
                                                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                //Vemos si tenemos que lanzar IRSA.
                                                                if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                                {
                                                                    //LanzarTrancomp(_pathIrsa);
                                                                    string _compro = _numeroPOS.ToString().PadLeft(4, '0') + "-" + _numeroComprobante.ToString().PadLeft(8, '0');
                                                                    RestHasar2G.Irsa.LanzarTrancomp(_cabecera, _cliente, _pagos, _infoIrsa, _tipoComprobante, _compro, _connection);
                                                                }
                                                                //SiTef
                                                                if (!String.IsNullOrEmpty(_inforSiTef.pathSendInvoice))
                                                                {
                                                                    if (Directory.Exists(_inforSiTef.pathSendInvoice))
                                                                    {
                                                                        //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                        //    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                        SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                                _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                    }
                                                                    else
                                                                        MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                }
                                                            }
                                                            else
                                                                MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                        else
                                                        {
                                                            bool _cuitOK = true;
                                                            //Vemos si debemos validar el cuit.
                                                            if (_cliente.regimenFacturacion == "1")
                                                            {
                                                                int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                                int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                                if (_digitorecibido == _digitoValidador)
                                                                    _cuitOK = true;
                                                                else
                                                                    _cuitOK = false;
                                                            }
                                                            else
                                                            {
                                                                if (String.IsNullOrEmpty(_cliente.documento))
                                                                    _cliente.documento = "11111111";
                                                            }

                                                            //Validamos el campo direccion del cliente.
                                                            if (String.IsNullOrEmpty(_cliente.direccion))
                                                                _cliente.direccion = ".";
                                                            if (_cuitOK)
                                                            {
                                                                bool _faltaPapel;
                                                                string _respImpre;
                                                                bool _necesitaCierreZ = false;

                                                                if (!_monotributo)
                                                                {
                                                                    _respImpre = Impresiones.ImprimirFacturasAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                        out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                    _tipoComprobante = "D";
                                                                }
                                                                else
                                                                {
                                                                    _respImpre = Impresiones.ImprimirFacturasC(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                        out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS);
                                                                    _letraComprobante = "C";
                                                                    _tipoComprobante = "D";
                                                                }

                                                                if (_necesitaCierreZ)
                                                                {
                                                                    string _msg = "Ha llegado al horario del FINAL DE LA JORNADA FISCAL y debe realizar un Cierre Z. Desea realizarlo ahora y luego imprimir el tiquet?.";
                                                                    //Preguntamos.
                                                                    if (MessageBox.Show(new Form { TopMost = true },_msg, "ICG Argentina", MessageBoxButtons.YesNo,
                                                                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                                                    {
                                                                        //Sacamos el Cierre Z.
                                                                        Cierres.ImprimirCierreZ(_ip, _hasarLog, Convert.ToInt32(_caja), _connection);
                                                                        //Volvemos a Lanzar la impresion
                                                                        if (!_monotributo)
                                                                            _respImpre = Impresiones.ImprimirFacturasAB2019(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                                out _faltaPapel, out _necesitaCierreZ, out _numeroComprobante, out _numeroPOS, out _letraComprobante);
                                                                        else
                                                                        {
                                                                            _respImpre = Impresiones.ImprimirFacturasC(_cabecera, _cliente, _items, _descuentos, _pagos, _promociones, _ip, _connection, _hasarLog,
                                                                                out _faltaPapel, out _numeroComprobante, out _numeroPOS);
                                                                            _letraComprobante = "C";
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        //Cambiar texto de _respImprime.
                                                                        _respImpre = "Recuerde que para seguir facturando debe SI o SI realizar un CIERRE Z.";
                                                                    }
                                                                }

                                                                if (!String.IsNullOrEmpty(_respImpre))
                                                                    MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                                if (_faltaPapel)
                                                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                //Vemos si tenemos que lanzar IRSA
                                                                if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                                {
                                                                    //LanzarTrancomp(_pathIrsa);
                                                                    string _compro = _numeroPOS.ToString().PadLeft(4, '0') + "-" + _numeroComprobante.ToString().PadLeft(8, '0');
                                                                    RestHasar2G.Irsa.LanzarTrancomp(_cabecera, _cliente, _pagos, _infoIrsa, _tipoComprobante, _compro, _connection);
                                                                }
                                                                //SiTef
                                                                if (!String.IsNullOrEmpty(_inforSiTef.pathSendInvoice))
                                                                {
                                                                    if (Directory.Exists(_inforSiTef.pathSendInvoice))
                                                                    {
                                                                        //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                        //    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                        SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                                _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                                    }
                                                                    else
                                                                        MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                                }
                                                            }
                                                            else
                                                                MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto. Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "No puede haber mas de 5 formas de pago." + Environment.NewLine +
                                                            "Antes de fiscalizar por consola debe modificar la venta desde Ventas opción Modificar." + Environment.NewLine +
                                                            "Luego de fizcalizarla puede volver a modificar con las forma de pago que quiera",
                                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, "No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                            }
                                            else
                                            {
                                                MessageBox.Show(new Form { TopMost = true }, "No existen Items registrados. Por favor revise el comprobante.",
                                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            }
                                        }
                                    }
                                    //Eliinamos el Log.
                                    if(!_hasarLog)
                                        DeleteLog();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                          "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                    //Vemos si tenemos que lanzar IRSA
                                    if (ex.Message.Contains("CANCELADA") || ex.Message.Contains("Se excedió el tiempo de espera"))
                                    {
                                        if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                        {
                                            //LanzarTrancomp(_pathIrsa);
                                            string _compro = _numeroPOS.ToString().PadLeft(4, '0') + "-" + _numeroComprobante.ToString().PadLeft(8, '0');
                                            if (_connection.State == System.Data.ConnectionState.Open)
                                            {
                                                RestHasar2G.Cabecera cabecera = Cabecera.GetCabecera(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, Convert.ToInt32(_codVendedor), _connection);
                                                Cliente cliente = Cliente.GetCliente(cabecera.codcliente, _connection);
                                                List<Pagos> pagos = Pagos.GetPagos(_serie, Convert.ToInt32(_numero), _n, _connection);
                                                RestHasar2G.Irsa.LanzarTrancomp(cabecera, cliente, pagos, _infoIrsa, _tipoComprobante, _compro, _connection);
                                            }
                                        }
                                        //SiTef
                                        if (!String.IsNullOrEmpty(_inforSiTef.pathSendInvoice))
                                        {
                                            if (Directory.Exists(_inforSiTef.pathSendInvoice))
                                            {
                                                //SiTefService.SendInvoices.SendCanceledInvoice(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                //    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _connection);
                                                SiTefService.SendInvoices.SendCanceledInvoiceSql12(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _connection);
                                            }
                                            else
                                                MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }
                                    }
                                    else
                                    {
                                        if (MessageBox.Show(new Form { TopMost = true }, "No se pudo establecer comunicacion con la Controladora Fiscal. Desea ingresar el Nro. del Talonario?.",
                                          "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                        {
                                            string _tipoComprobante2 = ""; // = FuncionesVarias.QueComprobanteEs(_fo, _serie, _numero, _n, _codVendedor, _connection);
                                            RestHasar2G.Cabecera cabecera = Cabecera.GetCabecera(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, Convert.ToInt32(_codVendedor), _connection);
                                            Cliente cliente = Cliente.GetCliente(cabecera.codcliente, _connection);
                                            if (cabecera.totalbruto > 0)
                                            {
                                                if (cabecera.codcliente == 0)
                                                    _tipoComprobante2 = "006_FC_Preimpresa_B";
                                                else
                                                {
                                                    if(cliente.regimenFacturacion == "1")
                                                        _tipoComprobante2 = "001_FC_Preimpresa_A";
                                                    else
                                                        _tipoComprobante2 = "006_FC_Preimpresa_B";
                                                }
                                            }
                                            else
                                            {
                                                if (cabecera.codcliente == 0)
                                                    _tipoComprobante2 = "008_NC_Preimpresa_B";
                                                else
                                                {
                                                    if (cliente.regimenFacturacion == "1")
                                                        _tipoComprobante2 = "003_NC_Preimpresa_A";
                                                    else
                                                        _tipoComprobante2 = "008_NC_Preimpresa_B";
                                                }
                                            }
                                            imgresoManual frm = new imgresoManual(_tipoComprobante2);
                                            frm.ShowDialog();
                                            int _ptoVta = frm._ptoVta;
                                            int _nroCbte = frm._nroCbte;
                                            frm.Dispose();
                                            _numeroComprobante = _nroCbte;
                                            _numeroPOS = _ptoVta;
                                            //_tipoComprobante = "D";
                                            if (_ptoVta > 0 && _nroCbte > 0)
                                            {
                                                
                                                string _serieFiscalMan = "";
                                                switch (_tipoComprobante2.Substring(0, 3))
                                                {
                                                    case "003":
                                                        {
                                                            _serieFiscalMan = "003_NC_Preimpreso_A";
                                                            break;
                                                        }
                                                    case "008":
                                                        {
                                                            _serieFiscalMan = "008_NC_Preimpreso_B";
                                                            break;
                                                        }
                                                    case "001":
                                                        {
                                                            _serieFiscalMan = "006_FC_Preimpreso_A";
                                                            break;
                                                        }
                                                    case "006":
                                                        {
                                                            _serieFiscalMan = "006_FC_Preimpreso_B";
                                                            break;
                                                        }
                                                    default:
                                                        {
                                                            _serieFiscalMan = "Sin_definirPreimpreso";
                                                            break;
                                                        }
                                                }
                                                Cabecera.TransaccionOK(cabecera, 0, _ptoVta.ToString().PadLeft(4, '0'), _serieFiscalMan, _nroCbte, DateTime.Now, _connection);
                                                if (!String.IsNullOrEmpty(_infoIrsa.pathSalida))
                                                {
                                                    if (Directory.Exists(_infoIrsa.pathSalida))
                                                    {
                                                        if (_connection.State == System.Data.ConnectionState.Open)
                                                        {
                                                            string _compro = _numeroPOS.ToString().PadLeft(4, '0') + "-" + _numeroComprobante.ToString().PadLeft(8, '0');
                                                            List<Pagos> pagos = Pagos.GetPagos(_serie, Convert.ToInt32(_numero), _n, _connection);
                                                            RestHasar2G.Irsa.LanzarTrancomp(cabecera, cliente, pagos, _infoIrsa, _tipoComprobante, _compro, _connection);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a IRSA.", 
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                                    }
                                                    //SiTef
                                                    if (!String.IsNullOrEmpty(_inforSiTef.pathSendInvoice))
                                                    {
                                                        if (Directory.Exists(_inforSiTef.pathSendInvoice))
                                                        {
                                                            //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                            //    _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                            SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero, _n, _inforSiTef.sitefIdTienda,
                                                                _inforSiTef.sitefIdTerminal, _inforSiTef.sitefCuit, _inforSiTef.sitefCuitIsv, _inforSiTef.pathSendInvoice, _inforSiTef.usaClover, _connection);
                                                        }
                                                        else
                                                            MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "No se encontro el archivo XML, de Configuración.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                    else
                    {
                        //Ponemos el numerofiscal en -1 y la seiefiscal en 00000
                        //Invocamos la transaccion Black
                        Cabecera.TransaccionBlack(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strConnection);
                    }
                }
                else
                {
                    //Ponemos el numerofiscal en -1 y la seiefiscal en 00000
                    //Invocamos la transaccion Black
                    Cabecera.TransaccionBlack(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strConnection);
                }
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "No se encontro el archivo XML, de ICG.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private static void DeleteLog()
        {
            if (File.Exists("HasarLog.log"))
            {
                File.Delete("HasarLog.log");
            }
            if (File.Exists("Fiscal.log"))
            {
                File.Delete("Fiscal.log");
            }
        }
    }
}
