﻿namespace IcgFrontRestConsola2019
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gbAcciones = new System.Windows.Forms.GroupBox();
            this.btTicketRegalo = new System.Windows.Forms.Button();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btReimprimir = new System.Windows.Forms.Button();
            this.btExit = new System.Windows.Forms.Button();
            this.gbImpresas = new System.Windows.Forms.GroupBox();
            this.rbSinImprimir = new System.Windows.Forms.RadioButton();
            this.rbImpresas = new System.Windows.Forms.RadioButton();
            this.gbTipo = new System.Windows.Forms.GroupBox();
            this.rbTipoN = new System.Windows.Forms.RadioButton();
            this.rbTipoB = new System.Windows.Forms.RadioButton();
            this.grImpresiones = new System.Windows.Forms.DataGridView();
            this.FECHA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HORAFIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRIPCIONDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIEFISCAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sERIEFISCAL2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMEROFISCALDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTALNETODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOTALBRUTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMBRECLIENTEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODVENDEDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nUMFACTURADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cambiarFechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dtsImpresiones = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpSeleccion = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btInvocar = new System.Windows.Forms.Button();
            this.btCierreX = new System.Windows.Forms.Button();
            this.btForzarZ = new System.Windows.Forms.Button();
            this.btReimpresionZ = new System.Windows.Forms.Button();
            this.btCierreZ = new System.Windows.Forms.Button();
            this.btConfig = new System.Windows.Forms.Button();
            this.gbAcciones.SuspendLayout();
            this.gbImpresas.SuspendLayout();
            this.gbTipo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grImpresiones)).BeginInit();
            this.contextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtsImpresiones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAcciones
            // 
            this.gbAcciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAcciones.BackColor = System.Drawing.SystemColors.Control;
            this.gbAcciones.Controls.Add(this.btTicketRegalo);
            this.gbAcciones.Controls.Add(this.btImprimir);
            this.gbAcciones.Controls.Add(this.btReimprimir);
            this.gbAcciones.Location = new System.Drawing.Point(18, 523);
            this.gbAcciones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbAcciones.Name = "gbAcciones";
            this.gbAcciones.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbAcciones.Size = new System.Drawing.Size(1059, 86);
            this.gbAcciones.TabIndex = 9;
            this.gbAcciones.TabStop = false;
            this.gbAcciones.Text = "TICKETS";
            // 
            // btTicketRegalo
            // 
            this.btTicketRegalo.Location = new System.Drawing.Point(447, 29);
            this.btTicketRegalo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btTicketRegalo.Name = "btTicketRegalo";
            this.btTicketRegalo.Size = new System.Drawing.Size(177, 35);
            this.btTicketRegalo.TabIndex = 2;
            this.btTicketRegalo.Text = "Ticket de Cambio";
            this.btTicketRegalo.UseVisualStyleBackColor = true;
            this.btTicketRegalo.Click += new System.EventHandler(this.btTicketRegalo_Click);
            // 
            // btImprimir
            // 
            this.btImprimir.Location = new System.Drawing.Point(224, 29);
            this.btImprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(177, 35);
            this.btImprimir.TabIndex = 1;
            this.btImprimir.Text = "Imprimir";
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btReimprimir
            // 
            this.btReimprimir.Location = new System.Drawing.Point(9, 29);
            this.btReimprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btReimprimir.Name = "btReimprimir";
            this.btReimprimir.Size = new System.Drawing.Size(177, 35);
            this.btReimprimir.TabIndex = 0;
            this.btReimprimir.Text = "Reimprimir";
            this.btReimprimir.UseVisualStyleBackColor = true;
            this.btReimprimir.Click += new System.EventHandler(this.btReimprimir_Click);
            // 
            // btExit
            // 
            this.btExit.Location = new System.Drawing.Point(927, 705);
            this.btExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(112, 35);
            this.btExit.TabIndex = 3;
            this.btExit.Text = "Salir";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.btExit_Click);
            // 
            // gbImpresas
            // 
            this.gbImpresas.Controls.Add(this.rbSinImprimir);
            this.gbImpresas.Controls.Add(this.rbImpresas);
            this.gbImpresas.Location = new System.Drawing.Point(663, 431);
            this.gbImpresas.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbImpresas.Name = "gbImpresas";
            this.gbImpresas.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbImpresas.Size = new System.Drawing.Size(376, 78);
            this.gbImpresas.TabIndex = 12;
            this.gbImpresas.TabStop = false;
            this.gbImpresas.Text = "Impresas";
            // 
            // rbSinImprimir
            // 
            this.rbSinImprimir.AutoSize = true;
            this.rbSinImprimir.Checked = true;
            this.rbSinImprimir.Location = new System.Drawing.Point(134, 29);
            this.rbSinImprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbSinImprimir.Name = "rbSinImprimir";
            this.rbSinImprimir.Size = new System.Drawing.Size(117, 24);
            this.rbSinImprimir.TabIndex = 1;
            this.rbSinImprimir.TabStop = true;
            this.rbSinImprimir.Text = "Sin Imprimir";
            this.rbSinImprimir.UseVisualStyleBackColor = true;
            this.rbSinImprimir.CheckedChanged += new System.EventHandler(this.rbSinImprimir_CheckedChanged);
            // 
            // rbImpresas
            // 
            this.rbImpresas.AutoSize = true;
            this.rbImpresas.Location = new System.Drawing.Point(9, 29);
            this.rbImpresas.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbImpresas.Name = "rbImpresas";
            this.rbImpresas.Size = new System.Drawing.Size(100, 24);
            this.rbImpresas.TabIndex = 0;
            this.rbImpresas.Text = "Impresas";
            this.rbImpresas.UseVisualStyleBackColor = true;
            this.rbImpresas.CheckedChanged += new System.EventHandler(this.rbImpresas_CheckedChanged);
            // 
            // gbTipo
            // 
            this.gbTipo.Controls.Add(this.rbTipoN);
            this.gbTipo.Controls.Add(this.rbTipoB);
            this.gbTipo.Location = new System.Drawing.Point(432, 431);
            this.gbTipo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbTipo.Name = "gbTipo";
            this.gbTipo.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbTipo.Size = new System.Drawing.Size(207, 78);
            this.gbTipo.TabIndex = 11;
            this.gbTipo.TabStop = false;
            this.gbTipo.Text = "Tipo";
            this.gbTipo.Visible = false;
            // 
            // rbTipoN
            // 
            this.rbTipoN.AutoSize = true;
            this.rbTipoN.Location = new System.Drawing.Point(123, 26);
            this.rbTipoN.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbTipoN.Name = "rbTipoN";
            this.rbTipoN.Size = new System.Drawing.Size(43, 24);
            this.rbTipoN.TabIndex = 1;
            this.rbTipoN.Text = "2";
            this.rbTipoN.UseVisualStyleBackColor = true;
            this.rbTipoN.CheckedChanged += new System.EventHandler(this.rbTipoN_CheckedChanged);
            // 
            // rbTipoB
            // 
            this.rbTipoB.AutoSize = true;
            this.rbTipoB.Checked = true;
            this.rbTipoB.Location = new System.Drawing.Point(27, 26);
            this.rbTipoB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbTipoB.Name = "rbTipoB";
            this.rbTipoB.Size = new System.Drawing.Size(43, 24);
            this.rbTipoB.TabIndex = 0;
            this.rbTipoB.TabStop = true;
            this.rbTipoB.Text = "1";
            this.rbTipoB.UseVisualStyleBackColor = true;
            this.rbTipoB.CheckedChanged += new System.EventHandler(this.rbTipoB_CheckedChanged);
            // 
            // grImpresiones
            // 
            this.grImpresiones.AllowUserToAddRows = false;
            this.grImpresiones.AllowUserToDeleteRows = false;
            this.grImpresiones.AutoGenerateColumns = false;
            this.grImpresiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grImpresiones.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FECHA,
            this.HORAFIN,
            this.dESCRIPCIONDataGridViewTextBoxColumn,
            this.NUMERO,
            this.SERIEFISCAL,
            this.sERIEFISCAL2DataGridViewTextBoxColumn,
            this.nUMEROFISCALDataGridViewTextBoxColumn,
            this.tOTALNETODataGridViewTextBoxColumn,
            this.tOTALBRUTODataGridViewTextBoxColumn,
            this.nOMBRECLIENTEDataGridViewTextBoxColumn,
            this.CODVENDEDOR,
            this.Z,
            this.nDataGridViewTextBoxColumn,
            this.FO,
            this.nUMFACTURADataGridViewTextBoxColumn,
            this.SERIE});
            this.grImpresiones.ContextMenuStrip = this.contextMenu;
            this.grImpresiones.DataMember = "Impresiones";
            this.grImpresiones.DataSource = this.dtsImpresiones;
            this.grImpresiones.Dock = System.Windows.Forms.DockStyle.Top;
            this.grImpresiones.Location = new System.Drawing.Point(0, 0);
            this.grImpresiones.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grImpresiones.MultiSelect = false;
            this.grImpresiones.Name = "grImpresiones";
            this.grImpresiones.ReadOnly = true;
            this.grImpresiones.RowHeadersWidth = 62;
            this.grImpresiones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grImpresiones.Size = new System.Drawing.Size(1094, 412);
            this.grImpresiones.TabIndex = 10;
            this.grImpresiones.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.grImpresiones_RowPrePaint);
            this.grImpresiones.SelectionChanged += new System.EventHandler(this.grImpresiones_SelectionChanged);
            // 
            // FECHA
            // 
            this.FECHA.DataPropertyName = "FECHA";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.FECHA.DefaultCellStyle = dataGridViewCellStyle1;
            this.FECHA.HeaderText = "Fecha";
            this.FECHA.MinimumWidth = 8;
            this.FECHA.Name = "FECHA";
            this.FECHA.ReadOnly = true;
            this.FECHA.Width = 80;
            // 
            // HORAFIN
            // 
            this.HORAFIN.DataPropertyName = "HORAFIN";
            dataGridViewCellStyle2.Format = "T";
            dataGridViewCellStyle2.NullValue = null;
            this.HORAFIN.DefaultCellStyle = dataGridViewCellStyle2;
            this.HORAFIN.HeaderText = "Hora";
            this.HORAFIN.MinimumWidth = 8;
            this.HORAFIN.Name = "HORAFIN";
            this.HORAFIN.ReadOnly = true;
            this.HORAFIN.Width = 80;
            // 
            // dESCRIPCIONDataGridViewTextBoxColumn
            // 
            this.dESCRIPCIONDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPCION";
            this.dESCRIPCIONDataGridViewTextBoxColumn.HeaderText = "Tipo Comprobante";
            this.dESCRIPCIONDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.dESCRIPCIONDataGridViewTextBoxColumn.Name = "dESCRIPCIONDataGridViewTextBoxColumn";
            this.dESCRIPCIONDataGridViewTextBoxColumn.ReadOnly = true;
            this.dESCRIPCIONDataGridViewTextBoxColumn.Width = 80;
            // 
            // NUMERO
            // 
            this.NUMERO.DataPropertyName = "NUMERO";
            this.NUMERO.HeaderText = "Nro. Interno";
            this.NUMERO.MinimumWidth = 8;
            this.NUMERO.Name = "NUMERO";
            this.NUMERO.ReadOnly = true;
            this.NUMERO.Width = 70;
            // 
            // SERIEFISCAL
            // 
            this.SERIEFISCAL.DataPropertyName = "SERIEFISCAL";
            this.SERIEFISCAL.HeaderText = "Serie Fiscal";
            this.SERIEFISCAL.MinimumWidth = 8;
            this.SERIEFISCAL.Name = "SERIEFISCAL";
            this.SERIEFISCAL.ReadOnly = true;
            this.SERIEFISCAL.Width = 150;
            // 
            // sERIEFISCAL2DataGridViewTextBoxColumn
            // 
            this.sERIEFISCAL2DataGridViewTextBoxColumn.DataPropertyName = "SERIEFISCAL2";
            this.sERIEFISCAL2DataGridViewTextBoxColumn.HeaderText = "Serie Fiscal 2";
            this.sERIEFISCAL2DataGridViewTextBoxColumn.MinimumWidth = 8;
            this.sERIEFISCAL2DataGridViewTextBoxColumn.Name = "sERIEFISCAL2DataGridViewTextBoxColumn";
            this.sERIEFISCAL2DataGridViewTextBoxColumn.ReadOnly = true;
            this.sERIEFISCAL2DataGridViewTextBoxColumn.Width = 150;
            // 
            // nUMEROFISCALDataGridViewTextBoxColumn
            // 
            this.nUMEROFISCALDataGridViewTextBoxColumn.DataPropertyName = "NUMEROFISCAL";
            this.nUMEROFISCALDataGridViewTextBoxColumn.HeaderText = "Nro. Fiscal";
            this.nUMEROFISCALDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.nUMEROFISCALDataGridViewTextBoxColumn.Name = "nUMEROFISCALDataGridViewTextBoxColumn";
            this.nUMEROFISCALDataGridViewTextBoxColumn.ReadOnly = true;
            this.nUMEROFISCALDataGridViewTextBoxColumn.Width = 70;
            // 
            // tOTALNETODataGridViewTextBoxColumn
            // 
            this.tOTALNETODataGridViewTextBoxColumn.DataPropertyName = "TOTALNETO";
            dataGridViewCellStyle3.Format = "c";
            this.tOTALNETODataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.tOTALNETODataGridViewTextBoxColumn.HeaderText = "Total";
            this.tOTALNETODataGridViewTextBoxColumn.MinimumWidth = 8;
            this.tOTALNETODataGridViewTextBoxColumn.Name = "tOTALNETODataGridViewTextBoxColumn";
            this.tOTALNETODataGridViewTextBoxColumn.ReadOnly = true;
            this.tOTALNETODataGridViewTextBoxColumn.Width = 70;
            // 
            // tOTALBRUTODataGridViewTextBoxColumn
            // 
            this.tOTALBRUTODataGridViewTextBoxColumn.DataPropertyName = "TOTALBRUTO";
            dataGridViewCellStyle4.Format = "c";
            this.tOTALBRUTODataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.tOTALBRUTODataGridViewTextBoxColumn.HeaderText = "Neto";
            this.tOTALBRUTODataGridViewTextBoxColumn.MinimumWidth = 8;
            this.tOTALBRUTODataGridViewTextBoxColumn.Name = "tOTALBRUTODataGridViewTextBoxColumn";
            this.tOTALBRUTODataGridViewTextBoxColumn.ReadOnly = true;
            this.tOTALBRUTODataGridViewTextBoxColumn.Width = 70;
            // 
            // nOMBRECLIENTEDataGridViewTextBoxColumn
            // 
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.DataPropertyName = "NOMBRECLIENTE";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.HeaderText = "Cliente";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.Name = "nOMBRECLIENTEDataGridViewTextBoxColumn";
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMBRECLIENTEDataGridViewTextBoxColumn.Width = 150;
            // 
            // CODVENDEDOR
            // 
            this.CODVENDEDOR.DataPropertyName = "CODVENDEDOR";
            this.CODVENDEDOR.HeaderText = "Cod. Vendedor";
            this.CODVENDEDOR.MinimumWidth = 8;
            this.CODVENDEDOR.Name = "CODVENDEDOR";
            this.CODVENDEDOR.ReadOnly = true;
            this.CODVENDEDOR.Width = 70;
            // 
            // Z
            // 
            this.Z.DataPropertyName = "Z";
            this.Z.HeaderText = "Cierre Z";
            this.Z.MinimumWidth = 8;
            this.Z.Name = "Z";
            this.Z.ReadOnly = true;
            this.Z.Visible = false;
            this.Z.Width = 150;
            // 
            // nDataGridViewTextBoxColumn
            // 
            this.nDataGridViewTextBoxColumn.DataPropertyName = "N";
            this.nDataGridViewTextBoxColumn.HeaderText = "N";
            this.nDataGridViewTextBoxColumn.MinimumWidth = 8;
            this.nDataGridViewTextBoxColumn.Name = "nDataGridViewTextBoxColumn";
            this.nDataGridViewTextBoxColumn.ReadOnly = true;
            this.nDataGridViewTextBoxColumn.Visible = false;
            this.nDataGridViewTextBoxColumn.Width = 40;
            // 
            // FO
            // 
            this.FO.DataPropertyName = "FO";
            this.FO.HeaderText = "FO";
            this.FO.MinimumWidth = 8;
            this.FO.Name = "FO";
            this.FO.ReadOnly = true;
            this.FO.Visible = false;
            this.FO.Width = 150;
            // 
            // nUMFACTURADataGridViewTextBoxColumn
            // 
            this.nUMFACTURADataGridViewTextBoxColumn.DataPropertyName = "NUMFACTURA";
            this.nUMFACTURADataGridViewTextBoxColumn.HeaderText = "Nro Factura";
            this.nUMFACTURADataGridViewTextBoxColumn.MinimumWidth = 8;
            this.nUMFACTURADataGridViewTextBoxColumn.Name = "nUMFACTURADataGridViewTextBoxColumn";
            this.nUMFACTURADataGridViewTextBoxColumn.ReadOnly = true;
            this.nUMFACTURADataGridViewTextBoxColumn.Visible = false;
            this.nUMFACTURADataGridViewTextBoxColumn.Width = 70;
            // 
            // SERIE
            // 
            this.SERIE.DataPropertyName = "SERIE";
            this.SERIE.HeaderText = "Serie";
            this.SERIE.MinimumWidth = 8;
            this.SERIE.Name = "SERIE";
            this.SERIE.ReadOnly = true;
            this.SERIE.Visible = false;
            this.SERIE.Width = 70;
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cambiarFechaToolStripMenuItem,
            this.borrarToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(201, 68);
            // 
            // cambiarFechaToolStripMenuItem
            // 
            this.cambiarFechaToolStripMenuItem.Name = "cambiarFechaToolStripMenuItem";
            this.cambiarFechaToolStripMenuItem.Size = new System.Drawing.Size(200, 32);
            this.cambiarFechaToolStripMenuItem.Text = "Cambiar Fecha";
            this.cambiarFechaToolStripMenuItem.Click += new System.EventHandler(this.cambiarFechaToolStripMenuItem_Click);
            // 
            // borrarToolStripMenuItem
            // 
            this.borrarToolStripMenuItem.Name = "borrarToolStripMenuItem";
            this.borrarToolStripMenuItem.Size = new System.Drawing.Size(200, 32);
            this.borrarToolStripMenuItem.Text = "Borrar";
            this.borrarToolStripMenuItem.Click += new System.EventHandler(this.borrarToolStripMenuItem_Click);
            // 
            // dtsImpresiones
            // 
            this.dtsImpresiones.DataSetName = "NewDataSet";
            this.dtsImpresiones.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn17});
            this.dataTable1.TableName = "Impresiones";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Serie";
            this.dataColumn1.ColumnName = "SERIE";
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Nuemro";
            this.dataColumn2.ColumnName = "NUMERO";
            this.dataColumn2.DataType = typeof(int);
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "N";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Numero ICG";
            this.dataColumn4.ColumnName = "NUMFACTURA";
            this.dataColumn4.DataType = typeof(int);
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Neto";
            this.dataColumn5.ColumnName = "TOTALBRUTO";
            this.dataColumn5.DataType = typeof(double);
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "Bruto";
            this.dataColumn6.ColumnName = "TOTALNETO";
            this.dataColumn6.DataType = typeof(double);
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Cliente";
            this.dataColumn7.ColumnName = "NOMBRECLIENTE";
            // 
            // dataColumn8
            // 
            this.dataColumn8.Caption = "Serie Fiscal";
            this.dataColumn8.ColumnName = "SERIEFISCAL";
            // 
            // dataColumn9
            // 
            this.dataColumn9.Caption = "Serie Fiscal 2";
            this.dataColumn9.ColumnName = "SERIEFISCAL2";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "Nro. Fiscal";
            this.dataColumn10.ColumnName = "NUMEROFISCAL";
            this.dataColumn10.DataType = typeof(int);
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "Descripción";
            this.dataColumn11.ColumnName = "DESCRIPCION";
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "Fecha";
            this.dataColumn12.ColumnName = "FECHA";
            this.dataColumn12.DataType = typeof(System.DateTime);
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "Nro. Z";
            this.dataColumn13.ColumnName = "Z";
            this.dataColumn13.DataType = typeof(int);
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "FO";
            this.dataColumn14.DataType = typeof(int);
            // 
            // dataColumn15
            // 
            this.dataColumn15.Caption = "Cod. Vendedor";
            this.dataColumn15.ColumnName = "CODVENDEDOR";
            this.dataColumn15.DataType = typeof(int);
            // 
            // dataColumn17
            // 
            this.dataColumn17.Caption = "Hora";
            this.dataColumn17.ColumnName = "HORAFIN";
            this.dataColumn17.DataType = typeof(System.DateTime);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 460);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Fecha Facturación";
            // 
            // dtpSeleccion
            // 
            this.dtpSeleccion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSeleccion.Location = new System.Drawing.Point(201, 455);
            this.dtpSeleccion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpSeleccion.Name = "dtpSeleccion";
            this.dtpSeleccion.Size = new System.Drawing.Size(188, 26);
            this.dtpSeleccion.TabIndex = 7;
            this.dtpSeleccion.ValueChanged += new System.EventHandler(this.dtpSeleccion_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btInvocar);
            this.groupBox1.Controls.Add(this.btCierreX);
            this.groupBox1.Controls.Add(this.btForzarZ);
            this.groupBox1.Controls.Add(this.btReimpresionZ);
            this.groupBox1.Controls.Add(this.btCierreZ);
            this.groupBox1.Location = new System.Drawing.Point(16, 618);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1059, 77);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CIERRES";
            // 
            // btInvocar
            // 
            this.btInvocar.Location = new System.Drawing.Point(14, 29);
            this.btInvocar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btInvocar.Name = "btInvocar";
            this.btInvocar.Size = new System.Drawing.Size(174, 35);
            this.btInvocar.TabIndex = 4;
            this.btInvocar.Text = "Cierre de Caja";
            this.btInvocar.UseVisualStyleBackColor = true;
            this.btInvocar.Click += new System.EventHandler(this.btInvocar_Click);
            // 
            // btCierreX
            // 
            this.btCierreX.Location = new System.Drawing.Point(656, 29);
            this.btCierreX.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCierreX.Name = "btCierreX";
            this.btCierreX.Size = new System.Drawing.Size(174, 35);
            this.btCierreX.TabIndex = 3;
            this.btCierreX.Text = "Cierre X";
            this.btCierreX.UseVisualStyleBackColor = true;
            this.btCierreX.Click += new System.EventHandler(this.btCierreX_Click);
            // 
            // btForzarZ
            // 
            this.btForzarZ.Location = new System.Drawing.Point(448, 29);
            this.btForzarZ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btForzarZ.Name = "btForzarZ";
            this.btForzarZ.Size = new System.Drawing.Size(174, 35);
            this.btForzarZ.TabIndex = 2;
            this.btForzarZ.Text = "Forzar Z";
            this.btForzarZ.UseVisualStyleBackColor = true;
            this.btForzarZ.Click += new System.EventHandler(this.btForzarZ_Click);
            // 
            // btReimpresionZ
            // 
            this.btReimpresionZ.Location = new System.Drawing.Point(225, 29);
            this.btReimpresionZ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btReimpresionZ.Name = "btReimpresionZ";
            this.btReimpresionZ.Size = new System.Drawing.Size(177, 35);
            this.btReimpresionZ.TabIndex = 1;
            this.btReimpresionZ.Text = "Reimprimir Z";
            this.btReimpresionZ.UseVisualStyleBackColor = true;
            this.btReimpresionZ.Click += new System.EventHandler(this.btReimpresionZ_Click);
            // 
            // btCierreZ
            // 
            this.btCierreZ.Enabled = false;
            this.btCierreZ.Location = new System.Drawing.Point(846, 29);
            this.btCierreZ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCierreZ.Name = "btCierreZ";
            this.btCierreZ.Size = new System.Drawing.Size(177, 35);
            this.btCierreZ.TabIndex = 0;
            this.btCierreZ.Text = "Cierres";
            this.btCierreZ.UseVisualStyleBackColor = true;
            this.btCierreZ.Visible = false;
            this.btCierreZ.Click += new System.EventHandler(this.btCierreZ_Click);
            // 
            // btConfig
            // 
            this.btConfig.Location = new System.Drawing.Point(432, 705);
            this.btConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btConfig.Name = "btConfig";
            this.btConfig.Size = new System.Drawing.Size(224, 35);
            this.btConfig.TabIndex = 14;
            this.btConfig.Text = "Configuración";
            this.btConfig.UseVisualStyleBackColor = true;
            this.btConfig.Click += new System.EventHandler(this.btConfig_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1094, 754);
            this.Controls.Add(this.btConfig);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbAcciones);
            this.Controls.Add(this.gbImpresas);
            this.Controls.Add(this.gbTipo);
            this.Controls.Add(this.grImpresiones);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpSeleccion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbAcciones.ResumeLayout(false);
            this.gbImpresas.ResumeLayout(false);
            this.gbImpresas.PerformLayout();
            this.gbTipo.ResumeLayout(false);
            this.gbTipo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grImpresiones)).EndInit();
            this.contextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtsImpresiones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAcciones;
        private System.Windows.Forms.Button btExit;
        private System.Windows.Forms.Button btTicketRegalo;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btReimprimir;
        private System.Windows.Forms.GroupBox gbImpresas;
        private System.Windows.Forms.RadioButton rbSinImprimir;
        private System.Windows.Forms.RadioButton rbImpresas;
        private System.Windows.Forms.GroupBox gbTipo;
        private System.Windows.Forms.RadioButton rbTipoN;
        private System.Windows.Forms.RadioButton rbTipoB;
        private System.Windows.Forms.DataGridView grImpresiones;
        private System.Data.DataSet dtsImpresiones;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpSeleccion;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btCierreX;
        private System.Windows.Forms.Button btForzarZ;
        private System.Windows.Forms.Button btReimpresionZ;
        private System.Windows.Forms.Button btCierreZ;
        private System.Windows.Forms.Button btInvocar;
        private System.Windows.Forms.Button btConfig;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn HORAFIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRIPCIONDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIEFISCAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn sERIEFISCAL2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMEROFISCALDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTALNETODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOTALBRUTODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMBRECLIENTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODVENDEDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Z;
        private System.Windows.Forms.DataGridViewTextBoxColumn nDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FO;
        private System.Windows.Forms.DataGridViewTextBoxColumn nUMFACTURADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SERIE;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem cambiarFechaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarToolStripMenuItem;
    }
}

