﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestHasar2G
{
    public class Cliente
    {
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string documento { get; set; }
        public string regimenFacturacion { get; set; }
        public string alias { get; set; }
        public string tipoDoc { get; set; }
        

        public static Cliente GetCliente(int _codCliente, SqlConnection _con)
        {
            string _sql = "SELECT CLIENTES.NOMBRECLIENTE, CLIENTES.DIRECCION1, CLIENTES.CIF, CLIENTES.CODPOSTAL, " +
                "CLIENTES.POBLACION, CLIENTES.PROVINCIA, CLIENTES.REGIMFACT, CLIENTES.ALIAS, CLIENTESCAMPOSLIBRES.TIPO_DOC " +
                "FROM CLIENTES inner join CLIENTESCAMPOSLIBRES on CLIENTES.CODCLIENTE = CLIENTESCAMPOSLIBRES.CODCLIENTE " +
                "WHERE CLIENTES.CODCLIENTE = @CodCliente";

            Cliente _cls = new Cliente();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@CodCliente", _codCliente);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cls.nombre = _reader["NOMBRECLIENTE"].ToString();
                            _cls.direccion = _reader["DIRECCION1"].ToString() + ", " + _reader["POBLACION"].ToString() + ", " + _reader["PROVINCIA"].ToString();
                            _cls.documento = _reader["CIF"].ToString();
                            _cls.regimenFacturacion = _reader["REGIMFACT"].ToString();
                            _cls.alias = _reader["ALIAS"] == null ? null : _reader["ALIAS"].ToString();
                            _cls.tipoDoc = _reader["TIPO_DOC"] == null ? null : _reader["TIPO_DOC"].ToString();
                        }
                    }
                }
            }

            return _cls;
        }
    }
}
