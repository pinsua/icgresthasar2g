﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using RestHasar2G;
using System.Xml.Linq;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.IO;
using Newtonsoft.Json;
using RestSharp;
using System.Xml;

namespace IcgFrontRestConsola2019
{
    public partial class Form1 : Form
    {
        public static SqlConnection _conn = new SqlConnection();
        public string _N = "B";
        public string _numfiscal;

        public static string _ip = "";
        public static string _caja = "";
        public static string _password = "";
        public static string _serverConfig = "";
        public static string _userConfig = "";
        public static string _catalogConfig = "";
        public bool _monotributo = false;
        public bool _hasarLog = false;
        public string _terminal = "";
        public static string _tktregalo1 = "";
        public static string _tktregalo2 = "";
        public static string _tktregalo3 = "";
        public static string _pathIrsa = "";
        public static string _keyIcg = "";
        public static string _codigoFudex = "";

        public string strConnection;
        public bool _KeyIsOk;
        public static InfoIrsa _irsa = new InfoIrsa();
        public static SiTefService.InfoSitef _sitef = new SiTefService.InfoSitef();

        public Form1()
        {
            InitializeComponent();
            this.Text = this.Text + " - V." + Application.ProductVersion;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (RestHasar2G.FuncionesVarias.LeerXmlConfiguracion(out _ip, out _caja, out _password, out _serverConfig,
                        out _userConfig, out _catalogConfig, out _monotributo, out _tktregalo1, out _tktregalo2,
                        out _tktregalo3, out _hasarLog, out _keyIcg, out _irsa, out _codigoFudex, out _sitef))
            {
                //Validamos el KeySoftware.
                //_KeyIsOk = ValidoKeySoftware(_keyIcg);
                _KeyIsOk = true;
                //Armamos el stringConnection.
                strConnection = "Data Source=" + _serverConfig + ";Initial Catalog=" + _catalogConfig + ";User Id=" + _userConfig + ";Password=masterkey;";
                //Recupero nombre terminal
                _terminal = Environment.MachineName;
                try
                {
                    //Recuperamos los Datos del controlador.
                    RestHasar2G.DatosControlador _datosControlador = FuncionesVarias.GetDatosControlador(_ip);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(new Form { TopMost = true }, "No hemos podido conectar con el controlador Fiscal." + Environment.NewLine +
                        "Por favor revise que el controlador Fiscal este encendido. Caso contrario vea la IP asignada en la congiguración.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }

                //Conectamos.
                _conn.ConnectionString = strConnection;
                try
                {
                    //Habrimos conexion con el SQL
                    _conn.Open();
                    //Validamos la KEY
                    if(_KeyIsOk)
                        ValidateKey();
                    //Obtebemos los datos.
                    GetDataset(_conn);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                DeshabilitarBotones();
                MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuración." + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private bool ValidoKeySoftware(string keyIcg)
        {
            string _key = IcgVarios.LicenciaIcg.Value();

            string _cod = IcgVarios.NuevaLicencia.Value(_key);

            if (keyIcg == _cod)
                return true;
            else
                return false;

        }
        private void DeshabilitarBotones()
        {
            //Desabilito los botones.
            btImprimir.Enabled = false;
            btReimprimir.Enabled = false;
            btTicketRegalo.Enabled = false;
        }

        private void GetDataset(SqlConnection _con)
        {
            string _sql;

            if (rbSinImprimir.Checked)
            {

                _sql = @"SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.NUMFACTURA, 
                        TIQUETSCAB.TOTALBRUTO,  TIQUETSCAB.TOTALNETO, ISNULL(CLIENTES.NOMBRECLIENTE,'Consumidor Final') as NOMBRECLIENTE, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, 
                        TIQUETSCAB.NUMEROFISCAL, 
						CASE WHEN TIQUETSCAB.TOTALBRUTO > 0 THEN 
						CASE WHEN CLIENTES.REGIMFACT = '4' THEN '006 Factura' ELSE '001 Factura' END
						ELSE 
						CASE WHEN CLIENTES.REGIMFACT = '4' THEN '008 Nota Credito' ELSE '003 Nota Credito' END
						END AS DESCRIPCION, 
                        TIQUETSCAB.FECHA, TIQUETSCAB.Z, TIQUETSCAB.CODVENDEDOR, TIQUETSCAB.HORAFIN
                        FROM TIQUETSCAB LEFT JOIN CLIENTES ON TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE
                        WHERE(TIQUETSCAB.NUMEROFISCAL is null or TIQUETSCAB.NUMEROFISCAL = '') 
                        AND TIQUETSCAB.FECHA <= @date AND TIQUETSCAB.CAJA = @caja AND TIQUETSCAB.N = 'B'
                        AND (SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'G' AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'S') AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'I'
                        AND (year(TIQUETSCAB.FECHAANULACION) <= 1900) AND TIQUETSCAB.SUBTOTAL = 'F' 
                        ORDER BY TIQUETSCAB.FECHA";
            }
            else
            {
                _sql = "SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.NUMFACTURA, " +
                    "TIQUETSCAB.TOTALBRUTO,  TIQUETSCAB.TOTALNETO, ISNULL(CLIENTES.NOMBRECLIENTE,'Consumidor Final') as NOMBRECLIENTE, TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.SERIEFISCAL2, " +
                    "TIQUETSCAB.NUMEROFISCAL, " +
                    "CASE WHEN TIQUETSCAB.TOTALBRUTO > 0 THEN " +
                    "CASE WHEN CLIENTES.REGIMFACT = '4' THEN '006 Factura' ELSE '001 Factura' END " +
                    "ELSE CASE when TIQUETSCAB.TOTALBRUTO = 0 then 'Cancelacion' ELSE " +
                    "CASE WHEN CLIENTES.REGIMFACT = '4' THEN '008 Nota Credito' ELSE '003 Nota Credito' END " +
                    "END END AS DESCRIPCION," +
                    "TIQUETSCAB.FECHA, TIQUETSCAB.Z, TIQUETSCAB.CODVENDEDOR, TIQUETSCAB.HORAFIN" +
                    " FROM TIQUETSCAB LEFT JOIN CLIENTES ON TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE " +
                    "WHERE ISNULL(TIQUETSCAB.NUMEROFISCAL,0) > 0 AND TIQUETSCAB.FECHA <= @date AND TIQUETSCAB.N = 'B' " +
                    "AND TIQUETSCAB.CAJA = @caja AND (SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'G' AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'S' AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'I') " +
                    "ORDER BY TIQUETSCAB.FECHA";
            }

            //Limpio el dataset
            dtsImpresiones.Tables["Impresiones"].Clear();

            using (SqlCommand _cmd = new SqlCommand(_sql, _con))
            {
                _cmd.Parameters.AddWithValue("@date", dtpSeleccion.Value.Date);
                _cmd.Parameters.AddWithValue("@N", _N);
                _cmd.Parameters.AddWithValue("@caja", _caja);

                using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
                {
                    _sda.Fill(dtsImpresiones, "Impresiones");
                }
            }
        }

        private void dtpSeleccion_ValueChanged(object sender, EventArgs e)
        {
            GetDataset(_conn);
        }

        private void grImpresiones_SelectionChanged(object sender, EventArgs e)
        {
            if (grImpresiones.RowCount > 0)
            {
                if (grImpresiones.SelectedRows.Count > 0)
                {
                    //object _numfiscal = grImpresiones.SelectedRows[0].Cells["NUMEROFISCAL"].Value;
                    _numfiscal = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                    string _serieFiscal2 = grImpresiones.SelectedRows[0].Cells[5].Value.ToString();
                    if (String.IsNullOrEmpty(_numfiscal) || _numfiscal == "0")
                    {
                        btReimprimir.Enabled = false;
                        btTicketRegalo.Enabled = false;
                        btImprimir.Enabled = true;
                    }
                    else
                    {
                        if (grImpresiones.SelectedRows[0].Cells[2].Value.ToString().ToUpper().Contains("FACTURA"))
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = true;
                        }
                        else
                        {
                            btImprimir.Enabled = false;
                            btReimprimir.Enabled = true;
                            btTicketRegalo.Enabled = false;
                        }
                    }
                }
            }
        }

        private void btReimprimir_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        try
                        {
                            string _descrip = grImpresiones.SelectedRows[0].Cells[2].Value.ToString();
                            string _numFactura = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                            decimal _totalNeto = Convert.ToDecimal(grImpresiones.SelectedRows[0].Cells[7].Value);
                            if (_totalNeto == 0)
                            {
                                MessageBox.Show(new Form { TopMost = true }, "El comprobante seleccionado fue una CANCELACION del controlador Fiscal." + Environment.NewLine + "No se puede reemprimir.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            else
                            {
                                if (_descrip.Contains("Preimpreso"))
                                {
                                    MessageBox.Show(new Form { TopMost = true }, "El comprobante seleccionado es un comprobante PREIMPRESO de carga manual." + Environment.NewLine +
                                        "Estos comprobante no se pueden REIMPIMIR, ya que no estan en la memoria del Controlador.",
                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                                else
                                {
                                    //Recupero la cabecera.
                                    bool _faltaPapel;
                                    string _resp = Impresiones.Reimprimir(Convert.ToInt32(_numFactura), _descrip, _ip, out _faltaPapel);

                                    if (!String.IsNullOrEmpty(_resp))
                                        MessageBox.Show(new Form { TopMost = true }, _resp, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                    if (_faltaPapel)
                                        MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel." + Environment.NewLine + "Por favor revise el papel del Controlador Fiscal.",
                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            //Vemos si tenemos licencia OK.
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        int _fo = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[13].Value);
                        string _serie = grImpresiones.SelectedRows[0].Cells[15].Value.ToString();
                        int _numero = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[3].Value);
                        string _N = grImpresiones.SelectedRows[0].Cells[12].Value.ToString();
                        string _numFactura = grImpresiones.SelectedRows[0].Cells[14].Value.ToString();
                        string _ptoVta = grImpresiones.SelectedRows[0].Cells[4].Value.ToString();
                        string _nroFiscal = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                        string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                        string _z = grImpresiones.SelectedRows[0].Cells[11].Value.ToString();
                        int _codVendedor = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[10].Value);
                        string _tipoComprobante = grImpresiones.SelectedRows[0].Cells[2].Value.ToString();

                        int _nrocomprobante = 0;
                        int _nropos = 0;
                        string _letracomprobante;
                        try
                        {
                            //Recuperamos los datos de la cabecera.
                            Cabecera _cab = Cabecera.GetCabecera(_fo, _serie, _numero, _N, _codVendedor, _conn);
                            List<Items> _items = Items.GetItems(_serie, _N, _numero, _conn);

                            if (_items.Count > 0)
                            {
                                List<Promociones> _promociones = Promociones.GetPromociones(_cab.serie, _cab.numero, _cab.n, _conn);
                                List<Pagos> _pagos = Pagos.GetPagos(_serie, _numero, _N, _conn);

                                if (_pagos.Count > 0)
                                {
                                    if (_pagos.Count < 6)
                                    {
                                        Cliente _cliente = Cliente.GetCliente(_cab.codcliente, _conn);
                                        //si el cliente es Hardcode los valores.
                                        if (_cab.codcliente == 0)
                                        {
                                            _cliente.direccion = ".";
                                            _cliente.regimenFacturacion = "4";
                                            _cliente.tipoDoc = "96_DNI";
                                            _cliente.nombre = "Consumidor Final";
                                            _cliente.documento = "0";
                                        }
                                        if (_cab.totalbruto < 0)
                                        {
                                            bool _cuitOK = true;
                                            //Vemos si debemos validar el cuit.
                                            if (_cliente.regimenFacturacion == "1")
                                            {
                                                int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                if (_digitorecibido == _digitoValidador)
                                                    _cuitOK = true;
                                                else
                                                    _cuitOK = false;
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(_cliente.documento))
                                                    _cliente.documento = "11111111";
                                            }
                                            //Validamos el campo direccion del cliente.
                                            if (String.IsNullOrEmpty(_cliente.direccion))
                                                _cliente.direccion = ".";
                                            if (_cuitOK)
                                            {
                                                List<Descuentos> _desc = Descuentos.GetDescuentos(_fo, _serie, _numero, _N, _conn);
                                                bool _faltaPapel;
                                                string _respImpre;
                                                if (_monotributo)
                                                    _respImpre = Impresiones.ImprimirNotaCreditoCSinValidarFecha(_cab, _cliente, _items, _desc, _pagos, _ip, _conn, _hasarLog, out _faltaPapel);
                                                else
                                                    _respImpre = Impresiones.ImprimirNotaCreditoABSinValidarFecha2019(_cab, _cliente, _items, _desc, _pagos, _ip, _conn, _hasarLog, out _faltaPapel);
                                                _tipoComprobante = "C";
                                                if (!String.IsNullOrEmpty(_respImpre))
                                                    MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                if (_faltaPapel)
                                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel." + Environment.NewLine + " Por favor revise el papel del Controlador Fiscal.",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                //Vemos si tenemos que lanzar IRSA.
                                                if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                {
                                                    if (Directory.Exists(_irsa.pathSalida))
                                                    {
                                                        if (EscribirXml(_fo, _serie, _numero, _N, _codVendedor))
                                                        {
                                                            //LanzarTrancomp(_irsa.pathSalida);
                                                            string _comprobante = _nropos.ToString().PadLeft(4, '0') + "-" + _nrocomprobante.ToString().PadLeft(8, '0');
                                                            RestHasar2G.Irsa.LanzarTrancomp(_cab, _cliente, _pagos, _irsa, _tipoComprobante, _comprobante, _conn);
                                                        }
                                                        else
                                                            MessageBox.Show(new Form { TopMost = true }, "No se pudo generar el archivo IMPRIME-XML." + Environment.NewLine + "La venta no fue informada.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a IRSA.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                                    }
                                                }
                                                //SiTef
                                                if (!String.IsNullOrEmpty(_sitef.pathSendInvoice))
                                                {
                                                    if (Directory.Exists(_sitef.pathSendInvoice))
                                                    {
                                                        //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero.ToString(), _N, _sitef.sitefIdTienda,
                                                        //_sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _conn);
                                                        SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero.ToString(), _N, _sitef.sitefIdTienda,
                                                        _sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _conn);
                                                    }
                                                    else
                                                        MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                            }
                                            else
                                                MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto." + Environment.NewLine + " Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                        }
                                        else
                                        {
                                            bool _cuitOK = true;
                                            //Vemos si debemos validar el cuit.
                                            if (_cliente.regimenFacturacion == "1")
                                            {

                                                int _digitoValidador = FuncionesVarias.CalcularDigitoCuit(_cliente.documento.Replace("-", ""));
                                                int _digitorecibido = Convert.ToInt16(_cliente.documento.Substring(_cliente.documento.Length - 1));
                                                if (_digitorecibido == _digitoValidador)
                                                    _cuitOK = true;
                                                else
                                                    _cuitOK = false;
                                            }
                                            else
                                            {
                                                if (String.IsNullOrEmpty(_cliente.documento))
                                                    _cliente.documento = "11111111";
                                            }
                                            //Validamos el campo direccion del cliente.
                                            if (String.IsNullOrEmpty(_cliente.direccion))
                                                _cliente.direccion = ".";
                                            if (_cuitOK)
                                            {
                                                List<Descuentos> _desc = Descuentos.GetDescuentos(_fo, _serie, _numero, _N, _conn);
                                                bool _faltaPapel;

                                                string _respImpre;
                                                if (_monotributo)
                                                    _respImpre = Impresiones.ImprimirFacturasCSinValidarFecha(_cab, _cliente, _items, _desc, _pagos, _promociones, _ip, _conn, _hasarLog, out _faltaPapel);
                                                else
                                                    _respImpre = Impresiones.ImprimirFacturasABSinValidarFecha2019(_cab, _cliente, _items, _desc, _pagos, _promociones, _ip, _conn, _hasarLog,
                                                        out _faltaPapel, out _nrocomprobante, out _nropos, out _letracomprobante);
                                                _tipoComprobante = "D";
                                                if (!String.IsNullOrEmpty(_respImpre))
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, _respImpre, "ICG Argentina", MessageBoxButtons.OK,
                                                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }

                                                if (_faltaPapel)
                                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel." + Environment.NewLine + "Por favor revise el papel del Controlador Fiscal.",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

                                                //Vemos si tenemos que lanzar IRSA.
                                                if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                                {
                                                    if (Directory.Exists(_irsa.pathSalida))
                                                    {
                                                        if (EscribirXml(_fo, _serie, _numero, _N, _codVendedor))
                                                        {
                                                            //LanzarTrancomp(_irsa.pathSalida);
                                                            string _comprobante = _nropos.ToString().PadLeft(4, '0') + "-" + _nrocomprobante.ToString().PadLeft(8, '0');
                                                            RestHasar2G.Irsa.LanzarTrancomp(_cab, _cliente, _pagos, _irsa, _tipoComprobante, _comprobante, _conn);
                                                        }
                                                        else
                                                            MessageBox.Show(new Form { TopMost = true }, "No se pudo generar el archivo IMPRIME-XML." + Environment.NewLine + "La venta no fue informada.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a IRSA.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                                    }
                                                }
                                                //SiTef
                                                if (!String.IsNullOrEmpty(_sitef.pathSendInvoice))
                                                {
                                                    if (Directory.Exists(_sitef.pathSendInvoice))
                                                    {
                                                        //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero.ToString(), _N, _sitef.sitefIdTienda,
                                                        //_sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _conn);
                                                        SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero.ToString(), _N, _sitef.sitefIdTienda,
                                                        _sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _conn);
                                                    }
                                                    else
                                                        MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                }
                                            }
                                            else
                                                MessageBox.Show(new Form { TopMost = true }, "El CUIT ingresado no es correcto." + Environment.NewLine + "Por favor ingreselo correctamente y luego reimprimar el comprobante.",
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show(new Form { TopMost = true }, "No puede haber mas de 5 formas de pago." + Environment.NewLine +
                                            "Antes de fiscalizar por consola debe modificar la venta desde Ventas opción Modificar." + Environment.NewLine +
                                            "Luego de fizcalizarla puede volver a modificar con las forma de pago que quiera",
                                                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                    }

                                    //Refrescamos al grid.
                                    GetDataset(_conn);
                                }
                                else
                                {
                                    MessageBox.Show(new Form { TopMost = true }, "No existen Pagos registrados, verifique que el importe sea mayor a cero. Por favor revise el comprobante.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                            }
                            else
                            {
                                MessageBox.Show(new Form { TopMost = true }, "No existen Items registrados. " + Environment.NewLine + "Por favor revise el comprobante.",
                                                   "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        catch (Exception ex)
                        {
                            //Validacion impuesta por Francisco.
                            if (ex.Message.StartsWith("POS_DOCUMENT_BEYOND_FISCAL_DAY"))
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: El controlador informa que ha cambiado la jornada Fiscal, y no es puede imprimir. Debe FORZAR un Cierre Z.",
                                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            else
                            {
                                if (ex.Message.Contains("Se excedió el tiempo de espera") ||
                                                        ex.Message.Contains("404"))
                                {
                                    if (MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message +
                                        Environment.NewLine + "Desea Ingresarlo manualmente?.",
                                        "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                        MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                    {
                                        frmIngresoManual frm = new frmIngresoManual(_tipoComprobante);
                                        frm.ShowDialog();
                                        int _ptoVtaManual = frm._ptoVta;
                                        int _nroCbteManual = frm._nroCbte;
                                        frm.Dispose();
                                        if (_ptoVtaManual > 0 && _nroCbteManual > 0)
                                        {

                                            string _serieFiscalMan = "";
                                            switch (_tipoComprobante.Substring(0, 3))
                                            {
                                                case "003":
                                                    {
                                                        _serieFiscalMan = "003_NC_Preimpreso_A";
                                                        break;
                                                    }
                                                case "008":
                                                    {
                                                        _serieFiscalMan = "008_NC_Preimpreso_B";
                                                        break;
                                                    }
                                                case "001":
                                                    {
                                                        _serieFiscalMan = "001_FC_Preimpreso_A";
                                                        break;
                                                    }
                                                case "006":
                                                    {
                                                        _serieFiscalMan = "006_FC_Preimpreso_B";
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        _serieFiscalMan = "Sin_definirPreimpreso";
                                                        break;
                                                    }
                                            }
                                            //Recuperamos los datos de la cabecera.
                                            Cabecera _cab = Cabecera.GetCabecera(_fo, _serie, _numero, _N, _codVendedor, _conn);
                                            Cabecera.TransaccionOK(_cab, 0, _ptoVtaManual.ToString().PadLeft(4, '0'), _serieFiscalMan, _nroCbteManual, DateTime.Now, _conn);
                                            if (!String.IsNullOrEmpty(_irsa.pathSalida))
                                            {
                                                if (Directory.Exists(_irsa.pathSalida))
                                                {
                                                    if (_conn.State == System.Data.ConnectionState.Open)
                                                    {
                                                        string _comprobanteIrsa = _ptoVtaManual.ToString().PadLeft(4, '0') + "-" + _nroCbteManual.ToString().PadLeft(8, '0');
                                                        Cliente _cliente = Cliente.GetCliente(_cab.codcliente, _conn);
                                                        List<Pagos> pagos = Pagos.GetPagos(_serie, Convert.ToInt32(_numero), _N, _conn);
                                                        RestHasar2G.Irsa.LanzarTrancomp(_cab, _cliente, pagos, _irsa, _tipoComprobante, _comprobanteIrsa, _conn);
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a IRSA.",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                                }
                                            }
                                            //SiTef
                                            if (!String.IsNullOrEmpty(_sitef.pathSendInvoice))
                                            {
                                                if (Directory.Exists(_sitef.pathSendInvoice))
                                                {
                                                    //SiTefService.SendInvoices.SendNormalInvoice(_serie, _numero.ToString(), _N, _sitef.sitefIdTienda,
                                                    //_sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _conn);
                                                    SiTefService.SendInvoices.SendNormalInvoiceSql12(_serie, _numero.ToString(), _N, _sitef.sitefIdTienda,
                                                    _sitef.sitefIdTerminal, _sitef.sitefCuit, _sitef.sitefCuitIsv, _sitef.pathSendInvoice, _sitef.usaClover, _conn);
                                                }
                                                else
                                                    MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                            }
                                        }
                                    }
                                }
                                else
                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        //Refrescamos el grid
                        GetDataset(_conn);
                    }                     
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + 
                    Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private bool EscribirXml(int pfo, string pserie, int pnumero, string pn, int pcodvendedor)
        {
            bool _rta = false;
            try
            {
                string strXml = @"<doc>
                               <bd>
                                  <server>#server#</server>
                                  <database>#base#</database>
                                  <user>#user#</user>
                               </bd>
                               <tipodoc>FACVENTA</tipodoc>
                               <fo>#fo#</fo>
                               <serie>#serie#</serie>
                               <numero>#numero#</numero>
                               <n>#n#</n>
                               <codvendedor>#vendedor#</codvendedor>
                            </doc>";
                //Reemplazo los datos.
                strXml = strXml.Replace("#server#", _serverConfig).Replace("#base#", _catalogConfig).Replace("#user#", _userConfig).Replace("#fo#", pfo.ToString())
                    .Replace("#serie#", pserie).Replace("#numero#", pnumero.ToString()).Replace("#n#", pn).Replace("#vendedor#", pcodvendedor.ToString());

                XElement xml = XElement.Parse(strXml);

                XDocument pruebaXml = new XDocument(xml);
                string _path = Environment.CurrentDirectory;
                _path = _path + @"\imprime.xml";
                pruebaXml.Save(_path);
                _rta = true;
            }
            catch (Exception ex)
            {
                _rta = false;
                throw new NotImplementedException();
            }

            return _rta;
        }

        private void btTicketRegalo_Click(object sender, EventArgs e)
        {
            //Vemos si tenemos licencia OK.
            if (_KeyIsOk)
            {
                if (grImpresiones.RowCount > 0)
                {
                    if (grImpresiones.SelectedRows.Count > 0)
                    {
                        if (grImpresiones.SelectedRows[0].Cells[2].Value.ToString().ToUpper().Contains("FACTURA"))
                        {
                            try
                            {
                                int _fo = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[14].Value);
                                string _serie = grImpresiones.SelectedRows[0].Cells[15].Value.ToString();
                                int _numero = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[3].Value);
                                string _N = grImpresiones.SelectedRows[0].Cells[12].Value.ToString();
                                string _numFactura = grImpresiones.SelectedRows[0].Cells[14].Value.ToString();
                                string _ptoVta = grImpresiones.SelectedRows[0].Cells[4].Value.ToString();
                                string _nroFiscal = grImpresiones.SelectedRows[0].Cells[6].Value.ToString();
                                string _compro = _ptoVta + "-" + _nroFiscal.PadLeft(8, '0');
                                string _z = grImpresiones.SelectedRows[0].Cells[11].Value.ToString();
                                int _codVendedor = Convert.ToInt32(grImpresiones.SelectedRows[0].Cells[10].Value);

                                Cabecera _cabecera = Cabecera.GetCabecera(_fo, _serie, _numero, _N, _codVendedor, _conn);
                                List<Items> _lstItems = Items.GetItems(_serie, _N, _numero, _conn);

                                bool _faltaPapel;
                                Impresiones.ImprimirTicketRegalo(_cabecera, _lstItems, _ip, _tktregalo1, _tktregalo2, _tktregalo3, _compro, _hasarLog, out _faltaPapel);

                                if (_faltaPapel)
                                    MessageBox.Show(new Form { TopMost = true }, "La impresora fiscal informa que se esta quedando sin papel.Por favor revise el papel.",
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Los tickets de regalo/cambio solo se pueden imprimir sobre una factura A o B.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void rbTipoB_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoB.Checked)
                rbTipoN.Checked = false;
            //Cambio variable.
            _N = "B";
            //Cargo Dataset.
            GetDataset(_conn);
        }

        private void rbTipoN_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoN.Checked)
                rbTipoB.Checked = false;
            //Cambio Variable.
            _N = "N";
            //Cargo Dataset.
            GetDataset(_conn);
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_conn.State == ConnectionState.Open)
                _conn.Close();
        }

        private void rbImpresas_CheckedChanged(object sender, EventArgs e)
        {
            if (rbImpresas.Checked)
                rbSinImprimir.Checked = false;

            GetDataset(_conn);
        }

        private void rbSinImprimir_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSinImprimir.Checked)
                rbImpresas.Checked = false;

            GetDataset(_conn);
        }

        private void grImpresiones_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            //if (Convert.ToInt32(grImpresiones.Rows[e.RowIndex].Cells[11].Value) > 0)
            //{
            //    grImpresiones.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
            //    grImpresiones.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.White;
            //}
        }

        private void btReimpresionZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmReimpresionZ _frm = new frmReimpresionZ(_ip, strConnection, _caja, _hasarLog);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                frmCierreZ _frm = new frmCierreZ(_ip, strConnection, _caja, _password, _hasarLog);
                _frm.ShowDialog(this);
                _frm.Dispose();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btForzarZ_Click(object sender, EventArgs e)
        {
            string _msjCbte;
            bool _IsOk = true;

            if (_KeyIsOk)
            {
                //Vemos si tenemos algo en la Password
                if (!String.IsNullOrEmpty(_password))
                {
                    frmPassword frm = new frmPassword();
                    frm.ShowDialog(this);
                    string _pp = frm._Text;
                    frm.Dispose();
                    if (_password != _pp)
                        _IsOk = false;
                }

                if (_IsOk)
                {
                    //Vemos si tenemos comprobantes sin fiscalizar y armamos el MSJ.
                    if (RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _caja))
                        _msjCbte = "POSEE COMPROBANTES SIN FISCALIZAR. " + Environment.NewLine + "Desea continuar con la impresión del Z fiscal?.";
                    else
                        _msjCbte = "Desea continuar con la impresión del Z fiscal?.";

                    if (MessageBox.Show(new Form { TopMost = true }, _msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        string _msj;

                        try
                        {
                            _msj = Cierres.ImprimirCierreZ(_ip, _hasarLog, Convert.ToInt32(_caja), _conn);
                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, 
                                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
                else
                    MessageBox.Show(new Form { TopMost = true }, "No posee permisos para realizar esta acción." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

        }

        private void btCierreX_Click(object sender, EventArgs e)
        {

            if (_KeyIsOk)
            {
                DialogResult result = MessageBox.Show(new Form { TopMost = true }, "Seguro que desea realizar un Cierre X?.", "ICG - Argentina",
                                            MessageBoxButtons.YesNo);
                switch (result)
                {
                    case DialogResult.Yes:
                        {
                            hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();

                            string _msj;

                            try
                            {
                                //Conectamos.
                                RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                                //ejecutamos el cierre X.
                                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                                _equis = _cierre.X;

                                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                  "Cierre 'X' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                                  "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                                  "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DF Total                 =[" + _equis.getDF_Total() + "]" + Environment.NewLine +
                                  "DF Total IVA             =[" + _equis.getDF_TotalIVA() + "]" + Environment.NewLine +
                                  "DF Total Otros Tributos  =[" + _equis.getDF_TotalTributos() + "]" + Environment.NewLine +
                                  "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "NC Total                 =[" + _equis.getNC_Total() + "]" + Environment.NewLine +
                                  "NC Total IVA             =[" + _equis.getNC_TotalIVA() + "]" + Environment.NewLine +
                                  "NC Total Otros Tributos  =[" + _equis.getNC_TotalTributos() + "]" + Environment.NewLine +
                                  "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]";

                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                            }
                            catch (Exception ex)
                            {
                                RestHasar2G.Impresiones.Cancelar(hasar);
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }

                            break;
                        }
                    case DialogResult.No:
                        {
                            this.Text = "[Cancel]";
                            break;
                        }
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btInvocar_Click(object sender, EventArgs e)
        {
            if (_KeyIsOk)
            {
                bool _rtaComprobantesOk = false;

                using (SqlConnection _con = new SqlConnection(strConnection))
                {
                    _con.Open();

                    _rtaComprobantesOk = RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_con, _caja);

                    if (_con.State == System.Data.ConnectionState.Open)
                        _con.Close();
                }

                if (!_rtaComprobantesOk)
                {
                    Process p = Process.GetProcessesByName("FrontRest").FirstOrDefault();
                    if (p != null)
                    {
                        string _msjCbte = "Desea continuar con la impresión del Z fiscal?.";
                        //Enviamos el CierreZ
                        if (MessageBox.Show(new Form { TopMost = true }, _msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {

                            //Invocamos al cierre de ICG, por medio del ShortCut.
                            IntPtr h = p.MainWindowHandle;
                            Program.SetForegroundWindow(h);
                            SendKeys.SendWait("%(z)");
                            SendKeys.Flush();
                            string _msj;

                            try
                            {
                                _msj = Cierres.ImprimirCierreZ(_ip, _hasarLog, Convert.ToInt32(_caja), _conn) ;
                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, 
                                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "La licencia de ICG Argentina no es correcta." + Environment.NewLine + "Por favor comuniquese con ICG Argentina",
                                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        private void btConfig_Click(object sender, EventArgs e)
        {
            bool _pasoOk = true;

            if (!String.IsNullOrEmpty(_password))
            {
                frmPassword frm = new frmPassword();
                frm.ShowDialog();
                string _super = frm._Text;
                if (_password != _super)
                    _pasoOk = false;

            }
            if (_pasoOk)
            {
                frmConfiguracionH2G frm = new frmConfiguracionH2G();
                frm.ShowDialog(this);
                frm.Dispose();
            }
        }

        private void cambiarFechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grImpresiones.SelectedRows.Count == 1)
                {
                    foreach (DataGridViewRow rw in grImpresiones.SelectedRows)
                    {
                        string _serie = rw.Cells["SERIE"].Value.ToString();
                        int _numero = Convert.ToInt32(rw.Cells["NUMERO"].Value);
                        string _n = rw.Cells[12].Value.ToString();
                        frmChangeDate _frm = new frmChangeDate(_serie, _numero, _n, _conn);
                        _frm.ShowDialog(this);
                        _frm.Dispose();
                    }
                    GetDataset(_conn);
                }
                else
                {
                    if (grImpresiones.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            _pass.Dispose();

        }

        private void borrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPassword _pass = new frmPassword();
            _pass.ShowDialog(this);
            string _key = _pass._Text;
            if (_key == "hoguera")
            {
                if (grImpresiones.SelectedRows.Count == 1)
                {
                    //Recupero las filas seleccionadas.
                    foreach (DataGridViewRow rw in grImpresiones.SelectedRows)
                    {
                        if (MessageBox.Show(new Form { TopMost = true },
                    "Desea BORRAR el comprobante seleccionado?.",
                    "ICG Argentina", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            try
                            {
                                int _fo = Convert.ToInt32(rw.Cells["FO"].Value);
                                string _serie = rw.Cells["SERIE"].Value.ToString();
                                int _numero = Convert.ToInt32(rw.Cells["NUMERO"].Value);
                                string _n = rw.Cells[12].Value.ToString();
                                int _codigoVendedor = Convert.ToInt32(rw.Cells["CODVENDEDOR"].Value);
                                //Invocamos la transaccion Black
                                Cabecera.TransaccionBlack(Convert.ToInt32(_fo), _serie, Convert.ToInt32(_numero), _n, strConnection);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Se produjo el siguiente Error: " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    GetDataset(_conn);
                }
                else
                {
                    if (grImpresiones.SelectedRows.Count == 0)
                    {
                        MessageBox.Show("Debe seleccionar por lo menos un Comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Debe seleccionar un Comprobante. El proceso no soporta mas de un comprobante.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void ValidateKey()
        {
            if (DateTime.Now.Day < 10)
            {
                //Recuperamos los Datos del controlador.
                RestHasar2G.DatosControlador _datosControlador = new RestHasar2G.DatosControlador();
                try
                {
                    _datosControlador = RestHasar2G.FuncionesVarias.GetDatosControlador(_ip);
                }
                catch
                {
                    MessageBox.Show("No ha sido posible conectar con el controlador fiscal " + Environment.NewLine +
                            "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                       "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //Validamos licencia via WebService.
                try
                {
                    RestHasar2G.Licencia lic = new RestHasar2G.Licencia();
                    lic.ClientCuit = _datosControlador.Cuit;
                    lic.ClientName = _datosControlador.RazonSocial;
                    lic.ClientRazonSocial = _datosControlador.RazonSocial;

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = Form1._conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "select VALOR from PARAMETROS where TERMINAL = 'VersionBD' and CLAVE = 'VersionBD'";

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    lic.Version = reader["VALOR"].ToString();
                                }
                            }
                        }
                    }
                    lic.ClientKey = IcgVarios.LicenciaIcg.Value();
                    lic.password = "Pinsua.2730";
                    lic.Plataforma = "REST";
                    lic.Release = Application.ProductVersion;
                    lic.user = "pinsua@yahoo.com";
                    lic.Tipo = "FC H2G";
                    lic.TerminalName = Environment.MachineName;
                    lic.PointOfSale = _datosControlador.NroPos;
                    var json = JsonConvert.SerializeObject(lic);
#if DEBUG
                    var client = new RestClient("http://localhost:18459/api/Licencias/GetKey");
#else
                        var client = new RestClient("http://licencias.icgargentina.com.ar:11100/api/Licencias/GetKey");
#endif
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", json, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        RestHasar2G.ResponcePostLicencia res = JsonConvert.DeserializeObject<RestHasar2G.ResponcePostLicencia>(response.Content);

                        if (String.IsNullOrEmpty(res.Key))
                        {
                            //Recupero los datos.
                            XmlDocument xDocNew = new XmlDocument();
                            xDocNew.Load("Config2daGeneracion.xml");
                            //keyICG
                            XmlNodeList _KEYICG = xDocNew.GetElementsByTagName("keyICG");
                            if (_KEYICG.Count > 0)
                                _KEYICG[0].InnerXml = "";
                            else
                            {
                                XmlElement elem = xDocNew.CreateElement("keyICG");
                                XmlText text = xDocNew.CreateTextNode("");
                                xDocNew.DocumentElement.AppendChild(elem);
                                xDocNew.DocumentElement.LastChild.AppendChild(text);
                            }
                            //Guardamos los datos.
                            xDocNew.Save("Config2daGeneracion.xml");
                            //Muestro un Mensaje.
                            MessageBox.Show(new Form { TopMost = true }, "Su licencia no esta habilitada." + Environment.NewLine +
                                    "Por favor comuniquese con ICG Argentina", "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                        response.Content, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se produjo el siguiente error: " + Environment.NewLine +
                        ex.Message + Environment.NewLine + "Por favor comuníquese con ICG Argentina para obtener una licencia.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
