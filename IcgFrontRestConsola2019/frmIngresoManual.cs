﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgFrontRestConsola2019
{
    public partial class frmIngresoManual : Form
    {
        public int _ptoVta = 0;
        public int _nroCbte = 0;
        public string _tipoComp;

        public frmIngresoManual(string tComp)
        {
            InitializeComponent();
            _tipoComp = tComp;
        }



        private void txtPtoVta_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            _ptoVta = 0;
            _nroCbte = 0;
            this.Close();
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            if (txtPtoVta.Text.Length > 0 && txtNro.Text.Length > 0)
            {
                _ptoVta = Convert.ToInt32(txtPtoVta.Text);
                _nroCbte = Convert.ToInt32(txtNro.Text);

                this.Close();
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "Debe ingresar un valor en el Punto de Venta y en el Numero de Comprobante.!",
                                      "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }
    }
}
