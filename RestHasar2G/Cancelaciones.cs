﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace RestHasar2G
{
    class Cancelaciones
    {
        public static bool TransaccionCancelacion(Cabecera _cabecera, string _nroPos, string _tipoComp, int _nroComp, int _nroZeta, SqlConnection _Conn)
        {
            SqlTransaction _tran;

            //Recupero el Nro de comprobante.
            Series _serie = Series.GetSerieBySerie(_cabecera.serie, _Conn);
            string _tipoDoc = _cabecera.serie.Substring(0, 1).ToUpper();
            int _numeroSerie;
            switch(_tipoDoc)
            {
                case "T":
                    {
                        if (_cabecera.n == "B")
                        {
                            _serie.numtiqb = _serie.numtiqb + 1;
                            _numeroSerie = _serie.numtiqb;
                        }
                        else
                        {
                            _serie.numtiqn = _serie.numtiqn + 1;
                            _numeroSerie = _serie.numtiqn;
                        }
                        break;
                    }
                case "F":
                    {
                        if (_cabecera.n == "B")
                        {
                            _serie.numfacvb = _serie.numfacvb + 1;
                            _numeroSerie = _serie.numfacvb;
                        }
                        else
                        {
                            _serie.numfacvn = _serie.numfacvn + 1;
                            _numeroSerie = _serie.numfacvn;
                        }
                        break;
                    }
                case "X":
                    {
                        if (_cabecera.n == "B")
                        {
                            _serie.numtiqb = _serie.numtiqb + 1;
                            _numeroSerie = _serie.numtiqb;
                        }
                        else
                        {
                            _serie.numtiqn = _serie.numtiqn + 1;
                            _numeroSerie = _serie.numtiqn;
                        }
                        break;
                    }
                case "Z":
                    {
                        if (_cabecera.n == "B")
                        {
                            _serie.numfacvb = _serie.numfacvb + 1;
                            _numeroSerie = _serie.numfacvb;
                        }
                        else
                        {
                            _serie.numfacvn = _serie.numfacvn + 1;
                            _numeroSerie = _serie.numfacvn;
                        }
                        break;
                    }
                default:
                    {
                        throw new Exception("TransaccionCancelacion. Serie no contemplada.");
                    }
            }
            //Comienzo la transaccion
            _tran = _Conn.BeginTransaction();
            //
            try
            {
                //Inserto TiquetsCab
                if (TiquetsCab.InsertCancelacion(_cabecera.fo, _cabecera.serie, _cabecera.numero,_cabecera.n, _nroPos, _tipoComp + "_Cancelada",_nroComp, _numeroSerie, _nroZeta, _Conn, _tran))
                {
                    //Inserto tiquetspag.
                    if (Pagos.InsertCancelacion(_cabecera.fo, _cabecera.serie, _cabecera.numero, _cabecera.n, _numeroSerie, _Conn, _tran))
                    {
                        //Actualizo el contador
                        if (!Series.UpdateSerieBySerie(_serie, _Conn, _tran))
                                throw new Exception("No se pudo Actualizar SeriesDoc.");                        
                    }
                    else
                        throw new Exception("No se pudo grabar TiquetsCab.");
                }
                else
                    throw new Exception("No se pudo grabar TiquetsCab.");
                //Actualizo los dato en TiquetsCab.
                TiquetsCab.GrabarNumeroTiquet(_cabecera.fo, _cabecera.serie, _cabecera.numero, _cabecera.n, "", "Sin Fiscalizar", 0, _Conn, _tran);
                //Grabo el Nro de Zeta en campos libres
                Cabecera.GrabarZetaCampoLibres(_cabecera.fo, _cabecera.serie, _numeroSerie, _cabecera.n, _nroZeta, _Conn,_tran);
                //Comiteo los cambios
                _tran.Commit();

            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw new Exception("TransaccionCancelacion" + ex.Message);
            }
            //Limpio la transaccion.
            _tran = null;
            //Insertamos en rem_transacciones.
            string _terminal = Environment.MachineName;
            if (!Rem_transacciones.InsertRemTransaccionesCancelacion(_terminal, _cabecera.serie, _numeroSerie, _cabecera.n, _Conn))
                throw new Exception("No se pudo grabar RemTransacciones.");

            return true;
        }
    }
}
