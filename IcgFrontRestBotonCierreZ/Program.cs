﻿using RestHasar2G;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgFrontRestBotonCierreZ
{
    static class Program
    {
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Invocamos el proceso.
            varios.CierreZ();
        }

        
    }
    public class varios
    {
        public static hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();

        public static void CierreZ()
        {
            string _caja= "";
            string _ip = "";
            string _password = "";
            string _serverConfig = "";
            string _userConfig = "";
            string _catalogConfig = "";
            bool _monotributo = false;
            bool _hasarLog = false;
            string _tktregalo1 = "";
            string _tktregalo2 = "";
            string _tktregalo3 = "";
            InfoIrsa _irsa = new InfoIrsa();
            string _keyIcg = "";
            string _codigoFudex = "";
            //SiTef
            SiTefService.InfoSitef _inforSiTef = new SiTefService.InfoSitef();

            try
            {
                //Leemos el archivo de configuracion
                if (RestHasar2G.FuncionesVarias.LeerXmlConfiguracion(out _ip, out _caja, out _password, out _serverConfig,
                        out _userConfig, out _catalogConfig, out _monotributo, out _tktregalo1, out _tktregalo2,
                        out _tktregalo3, out _hasarLog, out _keyIcg, out _irsa, out _codigoFudex, out _inforSiTef))
                {
                    //Armamos el string connection.
                    string _strConnection = "Data Source=" + _serverConfig + ";Initial Catalog=" + _catalogConfig + ";User Id=" + _userConfig + ";Password=masterkey;";
                    string _msjCbte = "";
                    bool _rtaComprobantesOk = false;
                    //Instanciamos la conexion
                    using (SqlConnection _con = new SqlConnection(_strConnection))
                    {
                        _con.Open();
                        //Vemos si tenemos comprobantes sin imprimir.
                        _rtaComprobantesOk = RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_con, _caja);

                        if (!_rtaComprobantesOk)
                        {
                            Process p = Process.GetProcessesByName("FrontRest").FirstOrDefault();
                            if (p != null)
                            {
                                _msjCbte = "Desea continuar con la impresión del Z fiscal?.";
                                //Enviamos el CierreZ
                                if (MessageBox.Show(new Form { TopMost = true }, _msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    //Invocamos al cierre de ICG, por medio del ShortCut.
                                    IntPtr h = p.MainWindowHandle;
                                    Program.SetForegroundWindow(h);
                                    SendKeys.SendWait("%(z)");
                                    SendKeys.Flush();
                                    string _msj;

                                    try
                                    {
                                        _msj = Cierres.ImprimirCierreZ(_ip, _hasarLog, Convert.ToInt32(_caja), _con);
                                        MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK,
                                            MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuracióm. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + ". Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }

        }

    }
}
