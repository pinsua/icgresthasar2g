﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgFrontRestCierresH2G
{
    public partial class Form1 : Form
    {
        public string _caja;
        public string _ip;
        public string _serverConfig;
        public string _userConfig;    
        public string _catalogConfig;
        public string strConnection;
        public string _password;
        public bool _monotributo;
        public string _terminal;
        public string _tktregalo1;
        public string _tktregalo2;
        public string _tktregalo3;
        public bool _hasarLog;
        public string _pathIrsa;

        public Form1()
        {
            InitializeComponent();
            this.Text = this.Text + " - V." + Application.ProductVersion;
        }

        private void btSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //Leemos el archivo de configuracion
                if (RestHasar2G.FuncionesVarias.LeerXmlConfiguracion(out _ip, out _caja, out _password, out _serverConfig,
                    out _userConfig, out _catalogConfig, out _monotributo, out _terminal, out _tktregalo1, out _tktregalo2,
                    out _tktregalo3, out _hasarLog, out _pathIrsa))
                {

                    //Recuperado los datos vamos por la conexion.
                    //Armamos el stringConnection.
                    strConnection = "Data Source=" + _serverConfig + ";Initial Catalog=" + _catalogConfig + ";User Id=" + _userConfig + ";Password=masterkey;";

                }
                else
                {
                    DeshabilitarBotones();
                    MessageBox.Show(new Form { TopMost = true }, "No se encuentra el archivo de configuracióm. Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message + ". Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void DeshabilitarBotones()
        {
            btCierreX.Enabled = false;
            btCierreZ.Enabled = false;
            btSalir.Enabled = true;
        }

        private void btCierreX_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(new Form { TopMost = true }, "Seguro que desea realizar un Cierre X?.", "ICG - Argentina",
               MessageBoxButtons.YesNo);
            switch (result)
            {
                case DialogResult.Yes:
                    {
                        hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                        hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                        hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX _equis = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX();

                        string _msj;

                        try
                        {
                            //Conectamos.
                            RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                            //Conectar(hasar);
                            //ejecutamos el cierre Z.
                            _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X);
                            _equis = _cierre.X;

                            _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                              "Cierre 'X' Nº            =[" + _equis.getNumero() + "]" + Environment.NewLine +
                              "Fecha del Cierre         =[" + _equis.getFechaCierre() + "]" + Environment.NewLine +
                              "DF Emitidos              =[" + _equis.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                              "DF Total                 =[" + _equis.getDF_Total() + "]" + Environment.NewLine +
                              "DF Total IVA             =[" + _equis.getDF_TotalIVA() + "]" + Environment.NewLine +
                              "DF Total Otros Tributos  =[" + _equis.getDF_TotalTributos() + "]" + Environment.NewLine +
                              "NC Emitidas              =[" + _equis.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                              "NC Total                 =[" + _equis.getNC_Total() + "]" + Environment.NewLine +
                              "NC Total IVA             =[" + _equis.getNC_TotalIVA() + "]" + Environment.NewLine +
                              "NC Total Otros Tributos  =[" + _equis.getNC_TotalTributos() + "]" + Environment.NewLine +
                              "DNFH Emitidos            =[" + _equis.getDNFH_CantidadEmitidos() + "]";

                            MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);


                        }
                        catch (Exception ex)
                        {
                            RestHasar2G.Impresiones.Cancelar(hasar);
                            //Cancelar(hasar);
                            MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }

                        break;
                    }
                case DialogResult.No:
                    {
                        this.Text = "[Cancel]";
                        break;
                    }
            }
        }

        private void btCierreZ_Click(object sender, EventArgs e)
        {
            frmCierreZ frm = new frmCierreZ(_ip, strConnection, _caja, _password, _hasarLog);
            frm.ShowDialog(this);
            frm.Dispose();
        }

        private void btReimpresion_Click(object sender, EventArgs e)
        {
            frmReimpresionZ frm = new frmReimpresionZ(_ip, strConnection, _caja, _hasarLog);
            frm.ShowDialog(this);
            frm.Dispose();
        }
    }
}
