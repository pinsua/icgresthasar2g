﻿namespace IcgFrontRestConsola
{
    partial class frmReimpresionZ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCierres = new System.Windows.Forms.GroupBox();
            this.btClose = new System.Windows.Forms.Button();
            this.btCierreZ = new System.Windows.Forms.Button();
            this.gbGrid = new System.Windows.Forms.GroupBox();
            this.grCierre = new System.Windows.Forms.DataGridView();
            this.arqueoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cajaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroHasarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtsReimpresion = new System.Data.DataSet();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.gbCierres.SuspendLayout();
            this.gbGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grCierre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsReimpresion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            this.SuspendLayout();
            // 
            // gbCierres
            // 
            this.gbCierres.Controls.Add(this.btClose);
            this.gbCierres.Controls.Add(this.btCierreZ);
            this.gbCierres.Dock = System.Windows.Forms.DockStyle.Right;
            this.gbCierres.Location = new System.Drawing.Point(587, 0);
            this.gbCierres.Name = "gbCierres";
            this.gbCierres.Size = new System.Drawing.Size(158, 261);
            this.gbCierres.TabIndex = 3;
            this.gbCierres.TabStop = false;
            this.gbCierres.Text = "Cierres";
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(42, 93);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(75, 23);
            this.btClose.TabIndex = 1;
            this.btClose.Text = "Salir";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btCierreZ
            // 
            this.btCierreZ.Location = new System.Drawing.Point(42, 35);
            this.btCierreZ.Name = "btCierreZ";
            this.btCierreZ.Size = new System.Drawing.Size(75, 23);
            this.btCierreZ.TabIndex = 0;
            this.btCierreZ.Text = "Cierre Z";
            this.btCierreZ.UseVisualStyleBackColor = true;
            this.btCierreZ.Click += new System.EventHandler(this.btCierreZ_Click);
            // 
            // gbGrid
            // 
            this.gbGrid.Controls.Add(this.grCierre);
            this.gbGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbGrid.Location = new System.Drawing.Point(0, 0);
            this.gbGrid.Name = "gbGrid";
            this.gbGrid.Size = new System.Drawing.Size(587, 261);
            this.gbGrid.TabIndex = 4;
            this.gbGrid.TabStop = false;
            this.gbGrid.Text = "Cierres de Caja";
            // 
            // grCierre
            // 
            this.grCierre.AllowUserToAddRows = false;
            this.grCierre.AllowUserToDeleteRows = false;
            this.grCierre.AutoGenerateColumns = false;
            this.grCierre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grCierre.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.arqueoDataGridViewTextBoxColumn,
            this.cajaDataGridViewTextBoxColumn,
            this.fechaDataGridViewTextBoxColumn,
            this.numeroDataGridViewTextBoxColumn,
            this.numeroHasarDataGridViewTextBoxColumn,
            this.cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn});
            this.grCierre.DataMember = "Reimpresion";
            this.grCierre.DataSource = this.dtsReimpresion;
            this.grCierre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grCierre.Location = new System.Drawing.Point(3, 16);
            this.grCierre.Name = "grCierre";
            this.grCierre.ReadOnly = true;
            this.grCierre.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grCierre.Size = new System.Drawing.Size(581, 242);
            this.grCierre.TabIndex = 0;
            this.grCierre.SelectionChanged += new System.EventHandler(this.grCierre_SelectionChanged);
            // 
            // arqueoDataGridViewTextBoxColumn
            // 
            this.arqueoDataGridViewTextBoxColumn.DataPropertyName = "Arqueo";
            this.arqueoDataGridViewTextBoxColumn.HeaderText = "Arqueo";
            this.arqueoDataGridViewTextBoxColumn.Name = "arqueoDataGridViewTextBoxColumn";
            this.arqueoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cajaDataGridViewTextBoxColumn
            // 
            this.cajaDataGridViewTextBoxColumn.DataPropertyName = "Caja";
            this.cajaDataGridViewTextBoxColumn.HeaderText = "Caja";
            this.cajaDataGridViewTextBoxColumn.Name = "cajaDataGridViewTextBoxColumn";
            this.cajaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fechaDataGridViewTextBoxColumn
            // 
            this.fechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha";
            this.fechaDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn.Name = "fechaDataGridViewTextBoxColumn";
            this.fechaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numeroDataGridViewTextBoxColumn
            // 
            this.numeroDataGridViewTextBoxColumn.DataPropertyName = "Numero";
            this.numeroDataGridViewTextBoxColumn.HeaderText = "Numero";
            this.numeroDataGridViewTextBoxColumn.Name = "numeroDataGridViewTextBoxColumn";
            this.numeroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numeroHasarDataGridViewTextBoxColumn
            // 
            this.numeroHasarDataGridViewTextBoxColumn.DataPropertyName = "NumeroHasar";
            this.numeroHasarDataGridViewTextBoxColumn.HeaderText = "NumeroHasar";
            this.numeroHasarDataGridViewTextBoxColumn.Name = "numeroHasarDataGridViewTextBoxColumn";
            this.numeroHasarDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn
            // 
            this.cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn.DataPropertyName = "CLEANCASHCONTROLCODE1";
            this.cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn.HeaderText = "CLEANCASHCONTROLCODE1";
            this.cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn.Name = "cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn";
            this.cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtsReimpresion
            // 
            this.dtsReimpresion.DataSetName = "NewDataSet";
            this.dtsReimpresion.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable2});
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11});
            this.dataTable2.TableName = "Reimpresion";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "Arqueo";
            this.dataColumn6.ColumnName = "Arqueo";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "Caja";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "Fecha";
            this.dataColumn8.DataType = typeof(System.DateTime);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Numero";
            this.dataColumn9.DataType = typeof(int);
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "NumeroHasar";
            this.dataColumn10.DataType = typeof(int);
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "CLEANCASHCONTROLCODE1";
            // 
            // frmReimpresionZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 261);
            this.Controls.Add(this.gbGrid);
            this.Controls.Add(this.gbCierres);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmReimpresionZ";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina - Reimpresión Cierres Z";
            this.Load += new System.EventHandler(this.frmReimpresionZ_Load);
            this.gbCierres.ResumeLayout(false);
            this.gbGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grCierre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsReimpresion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCierres;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btCierreZ;
        private System.Windows.Forms.GroupBox gbGrid;
        private System.Windows.Forms.DataGridView grCierre;
        private System.Windows.Forms.DataGridViewTextBoxColumn arqueoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cajaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroHasarDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cLEANCASHCONTROLCODE1DataGridViewTextBoxColumn;
        private System.Data.DataSet dtsReimpresion;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
    }
}