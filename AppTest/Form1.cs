﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestHasar2G;

namespace AppTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //Ponemos los valores por default.
            txtIdTienda.Text = "ARICG000";
            txtIdTerminal.Text = "ICG00000";
            txtCuitTienda.Text = "30711277249";
            txtCuitIsv.Text = "30711277249";
            txtPathEnvio.Text = @"C:\prueba\SiTef\";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Cargo los datos de Sitef
            SiTefService.InfoSitef _infoSitef = new SiTefService.InfoSitef();
            _infoSitef.pathSendInvoice = txtPathEnvio.Text;
            _infoSitef.sitefCuit = txtCuitTienda.Text;
            _infoSitef.sitefCuitIsv = txtCuitIsv.Text;
            _infoSitef.sitefIdTerminal = txtIdTerminal.Text;
            _infoSitef.sitefIdTienda = txtIdTienda.Text;

            string strConnection = "Data Source=" + Properties.Settings.Default.Server + ";Initial Catalog=" + Properties.Settings.Default.DataBase + ";User Id=" + Properties.Settings.Default.User + ";Password=masterkey;";

            if (Directory.Exists(_infoSitef.pathSendInvoice))
            {
                try
                {
                    using (SqlConnection _connection = new SqlConnection(strConnection))
                    {
                        _connection.Open();

                        //SiTefService.SendInvoices.SendNormalInvoice(txtSerie.Text, txtNumero.Text, txtN.Text, _infoSitef.sitefIdTienda,
                        //    _infoSitef.sitefIdTerminal, _infoSitef.sitefCuit, _infoSitef.sitefCuitIsv, _infoSitef.pathSendInvoice, false, _connection);
                        //SiTefService.SendInvoices.SendNormalInvoiceSql12(txtSerie.Text, txtNumero.Text, txtN.Text, _infoSitef.sitefIdTienda,
                        //    _infoSitef.sitefIdTerminal, _infoSitef.sitefCuit, _infoSitef.sitefCuitIsv, _infoSitef.pathSendInvoice, false, _connection);
                        SiTefService.SendInvoices.SendCanceledInvoiceSql12(txtSerie.Text, txtNumero.Text, txtN.Text, _infoSitef.sitefIdTienda,
                            _infoSitef.sitefIdTerminal, _infoSitef.sitefCuit, _infoSitef.sitefCuitIsv, _infoSitef.pathSendInvoice, _connection);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error: " + ex.Message,
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "No existe el directorio de Salida para informar a SITEF.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }
    }
}
