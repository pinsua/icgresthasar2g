﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgFrontRestCierresH2G
{
    public partial class frmCierreZ : Form
    {
        private string _ip;

        private string _Caja;

        private string _connection;

        public SqlConnection _conn = new SqlConnection();

        private string _pass;
        private bool _hasarLog;

        public static hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();

        public frmCierreZ(string sIp, string sConnection, string sCaja, string sPass, bool _hasarlog)
        {
            InitializeComponent();
            //paso las variables
            _ip = sIp;
            _connection = sConnection;
            _Caja = sCaja;
            _pass = sPass;
            _hasarLog = _hasarlog;
            //
            this.Text = this.Text + " - V." + Application.ProductVersion;
        }

        private void frmCierreZ_Load(object sender, EventArgs e)
        {
            try
            {
                //Conectamos.
                _conn.ConnectionString = _connection;

                _conn.Open();

                Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);

                if (grCierre.Rows.Count == 0)
                {
                    btCierreZ.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                DeshabilitarBotones();
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguente error: " + ex.Message + ". Por favor comuniquese con ICG Argentina.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void DeshabilitarBotones()
        {
            btCierreZ.Enabled = !btCierreZ.Enabled;
            //btCierreX.Enabled = !btCierreX.Enabled;
        }

        private void Conectar()
        {
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion _respVersion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion();
            try
            {
                //Abrimos el log.
                hasar.archivoRegistro("HasarLog.log");
                //Conectamos con la hasar.
                hasar.conectar(_ip);
            }
            catch (Exception ex)
            {
                throw new Exception("No se pudo conectar con la Controladora Fiscal.");
            }
        }


        private void btCierreZ_Click(object sender, EventArgs e)
        {
            if (grCierre.SelectedRows.Count > 0)
            {
                if (RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_conn, _Caja))
                {
                    if (MessageBox.Show(new Form { TopMost = true }, "Existen ventas sin fiscalizar. Desea continuar con la impresión del Z fiscal?.",
                       "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        //Vemos si tenemos algo en la Password
                        if (!String.IsNullOrEmpty(_pass))
                        {
                            frmPassword frm = new frmPassword();
                            frm.ShowDialog(this);
                            string _pp = frm._Text;
                            frm.Dispose();
                            if (_pass == _pp)
                            {
                                //Recupero los datos del grid.
                                string _cierreTipo = grCierre.SelectedRows[0].Cells[0].Value.ToString();
                                string _numero = grCierre.SelectedRows[0].Cells[1].Value.ToString();
                                string _fecha = grCierre.SelectedRows[0].Cells[2].Value.ToString();
                                string _hora = grCierre.SelectedRows[0].Cells[3].Value.ToString();

                                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                                hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                                string _msj;

                                try
                                {
                                    //Conectamos.
                                    RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                                    //ejecutamos el cierre Z.
                                    _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                                    _zeta = _cierre.Z;

                                    _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                      "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                                      "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                                      "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                      "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                                      "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                                      "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                                      "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                      "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                                      "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                                      "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                                      "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                      "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                                      "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                                      "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                                      "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                      "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                                      "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                                      "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                                      "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                                      "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                                    MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                                    //Modificamos la tabla Arqueos.
                                    string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _zeta.getDF_Total();
                                    int _rta = Arqueo.UpdateArqueos("Z", Convert.ToInt32(_numero), Convert.ToDateTime(_fecha), _hora, _dato, _conn);
                                }
                                catch (Exception ex)
                                {
                                    //Cancelar();
                                    RestHasar2G.Impresiones.Cancelar(hasar);
                                    MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                                }
                            }
                        }
                        else
                        {
                            //Recupero los datos del grid.
                            string _cierreTipo = grCierre.SelectedRows[0].Cells[0].Value.ToString();
                            string _numero = grCierre.SelectedRows[0].Cells[1].Value.ToString();
                            string _fecha = grCierre.SelectedRows[0].Cells[2].Value.ToString();
                            string _hora = grCierre.SelectedRows[0].Cells[3].Value.ToString();

                            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                            string _msj;

                            try
                            {

                                //Conectamos.
                                RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                                //ejecutamos el cierre Z.
                                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                                _zeta = _cierre.Z;

                                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                  "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                                  "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                                  "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                                  "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                                  "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                                  "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                  "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                                  "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                                  "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                                  "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                                  "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                                  "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                  "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                                  "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                                  "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                                //Modificamos la tabla Arqueos.
                                string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _zeta.getDF_Total();
                                int _rta = Arqueo.UpdateArqueos("Z", Convert.ToInt32(_numero), Convert.ToDateTime(_fecha), _hora, _dato, _conn);
                            }
                            catch (Exception ex)
                            {
                                //Cancelar();
                                RestHasar2G.Impresiones.Cancelar(hasar);
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }
                        }
                    }
                }
                else
                {
                    //Recupero los datos del grid.
                    string _cierreTipo = grCierre.SelectedRows[0].Cells[0].Value.ToString();
                    string _numero = grCierre.SelectedRows[0].Cells[1].Value.ToString();
                    string _fecha = grCierre.SelectedRows[0].Cells[2].Value.ToString();
                    string _hora = grCierre.SelectedRows[0].Cells[3].Value.ToString();

                    hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                    hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                    string _msj;

                    try
                    {
                        //Conectamos.
                        RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                        //ejecutamos el cierre Z.
                        _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                        _zeta = _cierre.Z;

                        _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                          "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                          "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                          "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                          "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                          "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                          "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                          "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                          "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                          "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                          "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                          "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                          "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                          "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                          "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                          "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                          "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                          "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                          "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                          "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                          "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                        MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                        //Modificamos la tabla Arqueos.
                        string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _zeta.getDF_Total();
                        int _rta = Arqueo.UpdateArqueos("Z", Convert.ToInt32(_numero), Convert.ToDateTime(_fecha), _hora, _dato, _conn);
                    }
                    catch (Exception ex)
                    {
                        //Cancelar();
                        RestHasar2G.Impresiones.Cancelar(hasar);
                        MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                            "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
                Arqueo.GetArqueo(dtsCierres, "Cierres", "Z", _Caja, _conn);
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "Debe seleccionar un Cierre de la tabla.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
