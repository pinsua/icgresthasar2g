﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace IcgFrontRestConsola
{
    public partial class frmConfiguracionH2G : Form
    {
        public frmConfiguracionH2G()
        {
            InitializeComponent();
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            if (File.Exists("Config2daGeneracion.xml"))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("Config2daGeneracion.xml");
                //Datos de la configuracion.
                XmlNodeList _List1 = xDoc.SelectNodes("/config");
                foreach (XmlNode xn in _List1)
                {
                    txtIp.Text = xn["ip"].InnerText;
                    txtCaja.Text = xn["caja"].InnerText;
                    txtTicketReg1.Text = xn["tktregalo1"].InnerText;
                    txtTicketReg2.Text = xn["tktregalo2"].InnerText;
                    txtTicketReg3.Text = xn["tktregalo3"].InnerText;
                    //_password = xn["super"].InnerText;
                    //_hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                    txtLicenciaIcg.Text = xn["keyICG"].InnerText;
                    //txtPtoVta.Text = xn["ptovtamanual"].InnerText;
                }
                //Datos de conexion a la base de datos
                XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                foreach (XmlNode xn in _Listdb)
                {
                    txtServidor.Text = xn["server"].InnerText;
                    txtUsuario.Text = xn["user"].InnerText;
                    txtBaseDatos.Text = xn["database"].InnerText;
                }
                //Datos de Irsa.
                XmlNodeList _ListIrsa = xDoc.SelectNodes("/config/Irsa");
                foreach (XmlNode xn in _ListIrsa)
                {
                    txtContratoIrsa.Text = xn["Contrato"].InnerText;
                    txtLocalIrsa.Text = xn["Local"].InnerText;
                    txtPathIrsa.Text = xn["PathSalida"].InnerText;
                    txtPosIrsa.Text = xn["Pos"].InnerText;
                    txtRubroIrsa.Text = xn["Rubro"].InnerText;
                }
                //Datos Caballito
                XmlNodeList _ListCaballito = xDoc.SelectNodes("/config/ShoppingCaballito");
                foreach (XmlNode xn in _ListCaballito)
                {
                    txtPathCaballito.Text = xn["PathSalida"].InnerText;
                }
                //Datos Olmos
                XmlNodeList _ListOlmos = xDoc.SelectNodes("/config/ShoppingOlmos");
                foreach (XmlNode xn in _ListOlmos)
                {
                    txtPathOlmos.Text = xn["PathSalida"].InnerText;
                }
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "No existe el archivo de configuración." +
                        Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Leo XML.
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("Config2daGeneracion.xml");

                XmlNode _config = xDoc.SelectSingleNode("/config");
                _config.ChildNodes[0].InnerText = txtIp.Text;
                _config.ChildNodes[1].InnerText = txtCaja.Text;
                //Datos Ticket
                _config.ChildNodes[2].InnerText = txtTicketReg1.Text;
                _config.ChildNodes[3].InnerText = txtTicketReg2.Text;
                _config.ChildNodes[4].InnerText = txtTicketReg3.Text;
                //Licencia
                _config.ChildNodes[8].InnerText = txtLicenciaIcg.Text;
                //Pto Vta Manual
                //_config.ChildNodes[8].InnerText = txtPtoVta.Text;
                ///Base de datos.
                XmlNode _configDB = xDoc.SelectSingleNode("/config/bd");
                _configDB.ChildNodes[0].InnerText = txtServidor.Text;
                _configDB.ChildNodes[1].InnerText = txtBaseDatos.Text;
                _configDB.ChildNodes[2].InnerText = txtUsuario.Text;
                //Shopping IRSA.
                XmlNode _configIrsa = xDoc.SelectSingleNode("/config/Irsa");
                _configIrsa.ChildNodes[0].InnerText = txtContratoIrsa.Text;
                _configIrsa.ChildNodes[1].InnerText = txtLocalIrsa.Text;
                _configIrsa.ChildNodes[2].InnerText = txtPathIrsa.Text;
                _configIrsa.ChildNodes[3].InnerText = txtPosIrsa.Text;
                _configIrsa.ChildNodes[4].InnerText = txtRubroIrsa.Text;
                ////Shopping Caballito
                //XmlNode _configCab = xDoc.SelectSingleNode("/config/ShoppingCaballito");
                //_configCab.ChildNodes[0].InnerText = txtPathCaballito.Text;
                ////Shopping Olmos
                //XmlNode _configOlm = xDoc.SelectSingleNode("/config/ShoppingOlmos");
                //_configOlm.ChildNodes[0].InnerText = txtPathCaballito.Text;


                xDoc.Save("Config2daGeneracion.xml");

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente error al intentar guardar el archivo de configuración." +
                        Environment.NewLine + "Error: " + ex.Message + Environment.NewLine + "Por favor comuniquese con ICG Argentina.",
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLicencia_Click(object sender, EventArgs e)
        {
            frmKeyDisplay _frm = new frmKeyDisplay();
            _frm.ShowDialog(this);
            _frm.Dispose();
        }
    }
}
