﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RestHasar2G
{
    public class FuncionesVarias
    {
        /// <summary>
        /// Calcula el dígito verificador dado un CUIT completo o sin él.
        /// </summary>
        /// <param name="cuit">El CUIT como String sin guiones</param>
        /// <returns>El valor del dígito verificador calculado.</returns>
        public static int CalcularDigitoCuit(string cuit)
        {
            int[] mult = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
            char[] nums = cuit.ToCharArray();
            int total = 0;
            for (int i = 0; i < mult.Length; i++)
            {
                total += int.Parse(nums[i].ToString()) * mult[i];
            }
            int resto = total % 11;
            return resto == 0 ? 0 : resto == 1 ? 9 : 11 - resto;
        }

        public static bool FechaOk(hfl.argentina.HasarImpresoraFiscalRG3561 _cf, Cabecera _cab)
        {
            bool _rta = false;
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora _respFechaHora = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora();
            _respFechaHora = _cf.ConsultarFechaHora();

            DateTime _fecha = _respFechaHora.getFecha();

            TimeSpan _hora = _respFechaHora.getHora();

            if (_cab.fecha.Date == _fecha.Date)
                _rta = true;
            else
            {
                if (_cab.fecha.Date >= _fecha.Date.AddDays(-3) && _cab.fecha.Date < _fecha.Date)
                    _rta = true;
            }

            return _rta;
        }

        public static bool TengoComprobantesSinImprimmir(SqlConnection _conn, string _caja)
        {
            bool _rta = true;

            string _sql = "SELECT count(*) FROM TIQUETSCAB WHERE(TIQUETSCAB.NUMEROFISCAL is null or TIQUETSCAB.NUMEROFISCAL = '') " +
                "AND (year(TIQUETSCAB.FECHAANULACION) <= 1900) AND TIQUETSCAB.N = 'B' " +
                "AND (SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'G' AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'S' AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'I') " +
                "AND TIQUETSCAB.SUBTOTAL = 'F' AND TIQUETSCAB.CAJA = @caja AND TIQUETSCAB.FECHA >= @date";

            using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
            {
                _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date.AddDays(-3));
                _cmd.Parameters.AddWithValue("@caja", _caja);

                object _resp = _cmd.ExecuteScalar();

                if (_resp == null)
                    _rta = false;
                else
                {
                    if (Convert.ToInt32(_resp) > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }

            return _rta;
        }

        public static bool TengoComprobantesSinImprimmir(string _strConn, string _caja)
        {
            bool _rta = true;

            string _sql = "SELECT count(*) FROM TIQUETSCAB WHERE(TIQUETSCAB.NUMEROFISCAL is null or TIQUETSCAB.NUMEROFISCAL = '') " +
                 "AND (year(TIQUETSCAB.FECHAANULACION) <= 1900) AND TIQUETSCAB.N = 'B' " +
                 "AND (SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'G' AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'S' AND  SUBSTRING(TIQUETSCAB.SERIE,1,1) <> 'I') " +
                 "AND TIQUETSCAB.SUBTOTAL = 'F' AND TIQUETSCAB.CAJA = @caja AND TIQUETSCAB.FECHA >= @date";

            using (SqlConnection _conn = new SqlConnection(_strConn))
            {
                _conn.Open();

                using (SqlCommand _cmd = new SqlCommand(_sql, _conn))
                {
                    _cmd.Parameters.AddWithValue("@date", DateTime.Now.Date.AddDays(-3));
                    _cmd.Parameters.AddWithValue("@caja", _caja);

                    object _resp = _cmd.ExecuteScalar();

                    if (_resp == null)
                        _rta = false;
                    else
                    {
                        if (Convert.ToInt32(_resp) > 0)
                            _rta = true;
                        else
                            _rta = false;
                    }
                }
            }

            return _rta;
        }

        public static bool FaltaPapel(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            if (_cf.ObtenerUltimoEstadoImpresora().getFaltaPapelReceipt())
            {
                return true;
            }
            else
                return false;
        }

        public static bool LeerXmlConfiguracion(out string _ip, out string _caja,
            out string _password, out string _server, out string _user, out string _catalog, out bool _monotributo
            ,out string _terminal, out string _tktregalo1, out string _tktregalo2, out string _tktregalo3,
            out bool _hasarLog, out string _keyICG, out InfoIrsa _infoIrsa)  
        {
            bool _rta = false;
            _ip = "";
            _caja = "";
            _password = "";
            _server = "";
            _user = "";
            _catalog = "";
            _monotributo = false;
            _hasarLog = false;
            _terminal = "";
            _tktregalo1 = "";
            _tktregalo2 = "";
            _tktregalo3 = "";
            //_pathIrsa = "";
            _keyICG = "";
            _infoIrsa = new InfoIrsa();

            try
            {
                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config2daGeneracion.xml");

                    XmlNodeList _List1 = xDoc.SelectNodes("/config");
                    foreach (XmlNode xn in _List1)
                    {
                        _ip = xn["ip"].InnerText;
                        _caja = xn["caja"].InnerText;
                        _password = xn["super"].InnerText;
                        _monotributo = Convert.ToBoolean(xn["monotributo"].InnerText);
                        _terminal = xn["terminal"].InnerText;
                        _tktregalo1 = xn["tktregalo1"].InnerText;
                        _tktregalo2 = xn["tktregalo2"].InnerText;
                        _tktregalo3 = xn["tktregalo3"].InnerText;
                        _hasarLog = Convert.ToBoolean(xn["hasarlog"].InnerText);
                        //_pathIrsa = xn["pathIrsa"].InnerText;
                        _keyICG = xn["keyICG"].InnerText;
                    }

                    XmlNodeList _Listdb = xDoc.SelectNodes("/config/bd");
                    foreach (XmlNode xn in _Listdb)
                    {
                        _server = xn["server"].InnerText;
                        _user = xn["user"].InnerText;
                        _catalog = xn["database"].InnerText;
                    }

                    //Nuevo Nodo de IRSA.
                    XmlNodeList _ListIrsa = xDoc.SelectNodes("/config/Irsa");
                    foreach (XmlNode xn in _ListIrsa)
                    {
                        _infoIrsa.contrato = xn["Contrato"].InnerText;
                        _infoIrsa.local = xn["Local"].InnerText;
                        _infoIrsa.pathSalida = xn["PathSalida"].InnerText;
                        _infoIrsa.pos = xn["Pos"].InnerText;
                        _infoIrsa.rubro = xn["Rubro"].InnerText;
                    }

                    _rta = true;
                }
                else
                    _rta = false;
            }
            catch(Exception)
            {
                throw new Exception("Se produjo un error leyendo el archivo de configuracion." + Environment.NewLine + "Por favor revise el archivo.");
            }
            return _rta;
        }

        public static bool LeerXmlConfiguracion(out string _ip, out string _caja,
            out string _password, out string _server, out string _user, out string _catalog, out bool _monotributo
            , out string _tktregalo1, out string _tktregalo2, out string _tktregalo3,
            out bool _hasarLog, out string _keyICG, out InfoIrsa _infoIrsa, out string _codigoFudex, out SiTefService.InfoSitef _infoSitef)   
        {
            bool _rta = false;
            _ip = "";
            _caja = "";
            _password = "";
            _server = "";
            _user = "";
            _catalog = "";
            _monotributo = false;
            _hasarLog = false;
            _tktregalo1 = "";
            _tktregalo2 = "";
            _tktregalo3 = "";
            _keyICG = "";
            _codigoFudex = "";
            _infoIrsa = new InfoIrsa();
            _infoSitef = new SiTefService.InfoSitef();

            try
            {
                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config2daGeneracion.xml");
                    //IP
                    XmlNodeList _IP = xDoc.GetElementsByTagName("ip");
                    if (_IP.Count > 0)
                        _ip = _IP[0].InnerXml;
                    else
                        _ip = "";
                    //CAJA
                    XmlNodeList _CAJA = xDoc.GetElementsByTagName("caja");
                    if (_CAJA.Count > 0)
                        _caja = _CAJA[0].InnerXml;
                    else
                        _caja = "";
                    //CAJA
                    XmlNodeList _PASSWORD = xDoc.GetElementsByTagName("super");
                    if (_CAJA.Count > 0)
                        _password = _PASSWORD[0].InnerXml;
                    else
                        _password = "";
                    //Monotributo
                    XmlNodeList _MONOTRIBUTO = xDoc.GetElementsByTagName("monotributo");
                    if (_MONOTRIBUTO.Count > 0)
                        _monotributo = Convert.ToBoolean(_MONOTRIBUTO[0].InnerXml);
                    else
                        _monotributo = false;
                    //tktregalo1
                    XmlNodeList _TKTREGALO1 = xDoc.GetElementsByTagName("tktregalo1");
                    if (_TKTREGALO1.Count > 0)
                        _tktregalo1 = _TKTREGALO1[0].InnerXml;
                    else
                        _tktregalo1 = "";
                    //tktregalo2
                    XmlNodeList _TKTREGALO2 = xDoc.GetElementsByTagName("tktregalo2");
                    if (_TKTREGALO2.Count > 0)
                        _tktregalo2 = _TKTREGALO2[0].InnerXml;
                    else
                        _tktregalo2 = "";
                    //tktregalo3
                    XmlNodeList _TKTREGALO3 = xDoc.GetElementsByTagName("tktregalo3");
                    if (_TKTREGALO3.Count > 0)
                        _tktregalo3 = _TKTREGALO3[0].InnerXml;
                    else
                        _tktregalo3 = "";
                    //keyICG
                    XmlNodeList _KEYICG = xDoc.GetElementsByTagName("keyICG");
                    if (_KEYICG.Count > 0)
                        _keyICG = _KEYICG[0].InnerXml;
                    else
                        _keyICG = "";
                    //HasarLog
                    XmlNodeList _HASARLOG = xDoc.GetElementsByTagName("hasarlog");
                    if (_HASARLOG.Count > 0)
                        _hasarLog = Convert.ToBoolean(_HASARLOG[0].InnerXml);
                    else
                        _hasarLog = false;
                    //CodigoFudex
                    XmlNodeList _CODIGOFUDEX = xDoc.GetElementsByTagName("codigoFudex");
                    if (_CODIGOFUDEX.Count > 0)
                        _codigoFudex = _CODIGOFUDEX[0].InnerXml;
                    else
                        _codigoFudex = "";
                    #region BBDD
                    //SEver
                    XmlNodeList _SEVER = xDoc.GetElementsByTagName("server");
                    if (_SEVER.Count > 0)
                        _server = _SEVER[0].InnerXml;
                    else
                        _server = "";
                    //USER
                    XmlNodeList _USER = xDoc.GetElementsByTagName("user");
                    if (_USER.Count > 0)
                        _user = _USER[0].InnerXml;
                    else
                        _user = "";
                    //CATALOG
                    XmlNodeList _CATALOG = xDoc.GetElementsByTagName("database");
                    if (_CATALOG.Count > 0)
                        _catalog = _CATALOG[0].InnerXml;
                    else
                        _catalog = "";
                    #endregion

                    #region IRSA
                    //IRSA CATALOG
                    XmlNodeList _CONTRATO = xDoc.GetElementsByTagName("Contrato");
                    if (_CONTRATO.Count > 0)
                        _infoIrsa.contrato = _CONTRATO[0].InnerXml;
                    else
                        _infoIrsa.contrato = "";
                    //IRSA CATALOG
                    XmlNodeList _LOCAL = xDoc.GetElementsByTagName("Local");
                    if (_LOCAL.Count > 0)
                        _infoIrsa.local = _LOCAL[0].InnerXml;
                    else
                        _infoIrsa.local = "";
                    //IRSA CATALOG
                    XmlNodeList PATHSALIDA = xDoc.GetElementsByTagName("PathSalida");
                    if (PATHSALIDA.Count > 0)
                        _infoIrsa.pathSalida = PATHSALIDA[0].InnerXml;
                    else
                        _infoIrsa.pathSalida = "";
                    //IRSA POS
                    XmlNodeList POS = xDoc.GetElementsByTagName("Pos");
                    if (POS.Count > 0)
                        _infoIrsa.pos = POS[0].InnerXml;
                    else
                        _infoIrsa.pos = "";
                    //IRSA RUBRO
                    XmlNodeList RUBRO = xDoc.GetElementsByTagName("Rubro");
                    if (RUBRO.Count > 0)
                        _infoIrsa.rubro = RUBRO[0].InnerXml;
                    else
                        _infoIrsa.rubro = "";
                    #endregion

                    #region Sitef
                    //Terminal
                    XmlNodeList SitefTerminal = xDoc.GetElementsByTagName("IdTerminal");
                    if (SitefTerminal.Count > 0)
                        _infoSitef.sitefIdTerminal = SitefTerminal[0].InnerXml;
                    else
                        _infoSitef.sitefIdTerminal = "";
                    //Tienda
                    XmlNodeList SitefTienda = xDoc.GetElementsByTagName("IdTienda");
                    if (SitefTienda.Count > 0)
                        _infoSitef.sitefIdTienda = SitefTienda[0].InnerXml;
                    else
                        _infoSitef.sitefIdTienda = "";
                    //Cuit
                    XmlNodeList SitefCuit = xDoc.GetElementsByTagName("Cuit");
                    if (SitefCuit.Count > 0)
                        _infoSitef.sitefCuit = SitefCuit[0].InnerXml;
                    else
                        _infoSitef.sitefCuit = "";
                    //CuitIsv
                    XmlNodeList SitefCuitIsv = xDoc.GetElementsByTagName("CuitIsv");
                    if (SitefCuitIsv.Count > 0)
                        _infoSitef.sitefCuitIsv = SitefCuitIsv[0].InnerXml;
                    else
                        _infoSitef.sitefCuitIsv = "";
                    //CuitIsv
                    XmlNodeList SitefPath = xDoc.GetElementsByTagName("PathSendInvioce");
                    if (SitefPath.Count > 0)
                        _infoSitef.pathSendInvoice = SitefPath[0].InnerXml;
                    else
                        _infoSitef.pathSendInvoice = "";
                    #endregion

                    _rta = true;
                }
                else
                    _rta = false;
            }
            catch (Exception)
            {
                throw new Exception("Se produjo un error leyendo el archivo de configuracion." + Environment.NewLine + "Por favor revise el archivo.");
            }
            return _rta;
        }

        public static bool LeerXmlConfiguracionPlugin(out string _ip, out string _caja, out bool _monotributo, out bool _hasarLog,
            out string _codigoFudex, out InfoIrsa _infoIrsa, out SiTefService.InfoSitef _infoSitef)
        { 
            bool _rta = false;
            _ip = "";
            _caja = "";
            _monotributo = false;
            _hasarLog = false;
            _codigoFudex = "";
            _infoIrsa = new InfoIrsa();
            _infoSitef = new SiTefService.InfoSitef();

            try
            {
                if (File.Exists("Config2daGeneracion.xml"))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Config2daGeneracion.xml");
                    //IP
                    XmlNodeList _IP = xDoc.GetElementsByTagName("ip");
                    if (_IP.Count > 0)
                        _ip = _IP[0].InnerXml;
                    else
                        throw new Exception("No esta Definida la IP del controlador fiscal.");
                        //_ip = "";
                    //CAJA
                    XmlNodeList _CAJA = xDoc.GetElementsByTagName("caja");
                    if (_CAJA.Count > 0)
                        _caja = _CAJA[0].InnerXml;
                    else
                        throw new Exception("No esta Definida la caja del sistema.");
                    //_caja = "";
                    //Monotributo
                    XmlNodeList _MONOTRIBUTO = xDoc.GetElementsByTagName("monotributo");
                    if (_MONOTRIBUTO.Count > 0)
                        _monotributo = Convert.ToBoolean(_MONOTRIBUTO[0].InnerXml);
                    else
                        _monotributo = false;
                    //HasarLog
                    XmlNodeList _HASARLOG = xDoc.GetElementsByTagName("hasarlog");
                    if (_HASARLOG.Count > 0)
                        _hasarLog = Convert.ToBoolean(_HASARLOG[0].InnerXml);
                    else
                        _hasarLog = false;
                    //CodigoFudex
                    XmlNodeList _CODIGOFUDEX = xDoc.GetElementsByTagName("codigoFudex");
                    if (_CODIGOFUDEX.Count > 0)
                        _codigoFudex = _CODIGOFUDEX[0].InnerXml;
                    else
                        _codigoFudex = "";

                    #region IRSA
                    //IRSA CATALOG
                    XmlNodeList _CONTRATO = xDoc.GetElementsByTagName("Contrato");
                    if (_CONTRATO.Count > 0)
                        _infoIrsa.contrato = _CONTRATO[0].InnerXml;
                    else
                        _infoIrsa.contrato = "";
                    //IRSA CATALOG
                    XmlNodeList _LOCAL = xDoc.GetElementsByTagName("Local");
                    if (_LOCAL.Count > 0)
                        _infoIrsa.local = _LOCAL[0].InnerXml;
                    else
                        _infoIrsa.local = "";
                    //IRSA CATALOG
                    XmlNodeList PATHSALIDA = xDoc.GetElementsByTagName("PathSalida");
                    if (PATHSALIDA.Count > 0)
                        _infoIrsa.pathSalida = PATHSALIDA[0].InnerXml;
                    else
                        _infoIrsa.pathSalida = "";
                    //IRSA POS
                    XmlNodeList POS = xDoc.GetElementsByTagName("Pos");
                    if (POS.Count > 0)
                        _infoIrsa.pos = POS[0].InnerXml;
                    else
                        _infoIrsa.pos = "";
                    //IRSA RUBRO
                    XmlNodeList RUBRO = xDoc.GetElementsByTagName("Rubro");
                    if (RUBRO.Count > 0)
                        _infoIrsa.rubro = RUBRO[0].InnerXml;
                    else
                        _infoIrsa.rubro = "";
                    #endregion

                    #region Sitef
                    //Terminal
                    XmlNodeList SitefTerminal = xDoc.GetElementsByTagName("IdTerminal");
                    if (SitefTerminal.Count > 0)
                        _infoSitef.sitefIdTerminal = SitefTerminal[0].InnerXml;
                    else
                        _infoSitef.sitefIdTerminal = "";
                    //Tienda
                    XmlNodeList SitefTienda = xDoc.GetElementsByTagName("IdTienda");
                    if (SitefTienda.Count > 0)
                        _infoSitef.sitefIdTienda = SitefTienda[0].InnerXml;
                    else
                        _infoSitef.sitefIdTienda = "";
                    //Cuit
                    XmlNodeList SitefCuit = xDoc.GetElementsByTagName("Cuit");
                    if (SitefCuit.Count > 0)
                        _infoSitef.sitefCuit = SitefCuit[0].InnerXml;
                    else
                        _infoSitef.sitefCuit = "";
                    //CuitIsv
                    XmlNodeList SitefCuitIsv = xDoc.GetElementsByTagName("CuitIsv");
                    if (SitefCuitIsv.Count > 0)
                        _infoSitef.sitefCuitIsv = SitefCuitIsv[0].InnerXml;
                    else
                        _infoSitef.sitefCuitIsv = "";
                    //CuitIsv
                    XmlNodeList SitefPath = xDoc.GetElementsByTagName("PathSendInvioce");
                    if (SitefPath.Count > 0)
                        _infoSitef.pathSendInvoice = SitefPath[0].InnerXml;
                    else
                        _infoSitef.pathSendInvoice = "";
                    //Usa Clover
                    XmlNodeList UsaClover = xDoc.GetElementsByTagName("UsaClover");
                    if (UsaClover.Count > 0)
                        _infoSitef.usaClover = String.IsNullOrEmpty(UsaClover[0].InnerXml) ? false : Convert.ToBoolean(UsaClover[0].InnerXml);
                    else
                        _infoSitef.usaClover = false;
                    #endregion

                    _rta = true;
                }
                else
                    _rta = false;
            }
            catch (Exception)
            {
                throw new Exception("Se produjo un error leyendo el archivo de configuracion." + Environment.NewLine + "Por favor revise el archivo.");
            }
            return _rta;
        }

        public static DateTime GetFechaCF(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora _respFechaHora = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora();
            _respFechaHora = _cf.ConsultarFechaHora();

            DateTime _fecha = _respFechaHora.getFecha();

            TimeSpan _hora = _respFechaHora.getHora();

            _fecha = _fecha.Add(_hora);
            
            return _fecha;
        }

        public static bool GenerarAuditoria(hfl.argentina.HasarImpresoraFiscalRG3561 _cf)
        {
            bool _rta = false;
            string _xml = "";

            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueAuditoria _rtaAuditoria = 
                new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueAuditoria();
            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueAuditoria _rtaAuditoria2 =
                new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueAuditoria();

            _rtaAuditoria = _cf.ObtenerPrimerBloqueAuditoria("01/04/18", "15/04/18", hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ.REPORTE_Z_FECHA, hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.COMPRIME, hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.XML_UNICO);

            if(_rtaAuditoria.getRegistro() == hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_INFORMACION)
            {
                _xml = _rtaAuditoria.getInformacion();
                _rtaAuditoria2 = _cf.ObtenerSiguienteBloqueAuditoria();

                while( _rtaAuditoria2.getRegistro() != hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                {
                    _xml = _xml + _rtaAuditoria2.getInformacion();
                    _rtaAuditoria2 = _cf.ObtenerSiguienteBloqueAuditoria();
                }


            }

            return _rta;
        }

        public static string QueComprobanteEs(string fo, string serie, string numero, string n, string vendedor,
            SqlConnection _con)
        {
            string _sql = @"SELECT CASE WHEN TIQUETSCAB.TOTALBRUTO >= 0 THEN CASE WHEN CLIENTES.REGIMFACT = 4 THEN '006 Factura B' ELSE '001 Factura A' END 
                    ELSE CASE WHEN CLIENTES.REGIMFACT = 4 THEN '008 Nota Credito B' ELSE '003 Nota Credito A' END END AS DESCRIPCION
                    FROM TIQUETSCAB inner join CLIENTES on TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE
                    WHERE TIQUETSCAB.fo = @fo AND TIQUETSCAB.SERIE = @serie AND TIQUETSCAB.NUMERO = @numero AND TIQUETSCAB.N = @n
                    AND TIQUETSCAB.CODVENDEDOR = @codVendedor";

            string _rta = "";

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@fo", fo);
                _cmd.Parameters.AddWithValue("@serie", serie);
                _cmd.Parameters.AddWithValue("@n", n);
                _cmd.Parameters.AddWithValue("@numero", numero);
                _cmd.Parameters.AddWithValue("@codVendedor", vendedor);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _rta = _reader["DESCRIPCION"].ToString();
                        }
                    }
                }
            }

            return _rta;
        }

        public static bool ValidarColumna(string _table, string _columna, SqlConnection _sqlConn)
        {
            string _sql = @"IF NOT EXISTS(SELECT column_name FROM information_schema.columns WHERE table_name=@table and column_name=@column )
                BEGIN SELECT 'NO EXISTE' END ELSE BEGIN SELECT 'EXISTE' END";
            bool _rta = false;

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _sqlConn;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;
                _cmd.Parameters.AddWithValue("@table", _table);
                _cmd.Parameters.AddWithValue("column", _columna);

                if (_cmd.ExecuteScalar().ToString() == "EXISTE")
                    _rta = true;
            }
            return _rta;
        }

        public static DatosControlador GetDatosControlador(string _ip)
        {
            try
            {
                DatosControlador _controlador = new DatosControlador();
                hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion _respConsultarDatosInicializacion = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion();
                //Conectamos con la hasar.
                hasar.conectar(_ip);
                //Recupero los datos de la Inicializacion.
                _respConsultarDatosInicializacion = hasar.ConsultarDatosInicializacion();
                //Recupero los datos del Punto de venta.
                _controlador.NroPos = _respConsultarDatosInicializacion.getNumeroPos();
                //Recupero el CUIT
                _controlador.Cuit = _respConsultarDatosInicializacion.getCUIT();
                //Recupero la Razon Social
                _controlador.RazonSocial = _respConsultarDatosInicializacion.getRazonSocial();

                return _controlador;
            }
            catch 
            {
                throw new Exception("Error al recuperar los datos del Controlador Fiscal.");
            }
        }
    }

    public class DatosControlador
    {
        public int NroPos { get; set; }
        public string Cuit { get; set; }
        public string RazonSocial { get; set; }
    }
    public class Licencia
    {
        public string user { get; set; }
        public string password { get; set; }
        public string ClientKey { get; set; }
        public string Release { get; set; }
        public string Plataforma { get; set; }
        public string Version { get; set; }
        public string ClientName { get; set; }
        public string ClientCuit { get; set; }
        public string ClientRazonSocial { get; set; }
        public string Tipo { get; set; }
        public string TerminalName { get; set; }
        public bool Enabled { get; set; }
        public int PointOfSale { get; set; }
    }

    public class ResponcePostLicencia
    {
        public string Key { get; set; }
        public string Resultado { get; set; }
    }
}
