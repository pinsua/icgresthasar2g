﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestHasar2G
{
    public class Pagos
    {
        public string descripcion { get; set; }
        public string tipopago { get; set; }
        public decimal monto { get; set; }
        public string cuotas { get; set; }
        public string cupon { get; set; }
        public decimal entregado { get; set; }
        public string CodTarjeta { get; set; }
        public string FormaPago { get; set; }
        public bool AbrirCajon { get; set; }

        public static List<Pagos> GetPagos(string _serie, int _numero, string _N, SqlConnection _con)
        {
            string _sql = "SELECT TIQUETSPAG.IMPORTE, FORMASPAGO.DESCRIPCION as TARJETA, FORMASPAGO.METALICO, TIQUETSPAG.ENTREGADO, " +
                "FORMASPAGO.TEXTOIMP as CodTarjeta, FORMASPAGO.MARCASTARJETA as FormaPago, FORMASPAGO.ABRIRCAJON " +
               "FROM TIQUETSPAG INNER JOIN FORMASPAGO ON TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO " +
               "WHERE TIQUETSPAG.serie = @serie AND TIQUETSPAG.NUMERO = @numero AND TIQUETSPAG.N = @n";

            List<Pagos> _pagos = new List<Pagos>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@n", _N);
                _cmd.Parameters.AddWithValue("@numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Pagos _cls = new Pagos();
                            _cls.descripcion = _reader["TARJETA"].ToString();
                            _cls.tipopago = _reader["METALICO"].ToString();
                            _cls.monto = Convert.ToDecimal(_reader["IMPORTE"]);
                            _cls.cuotas = "";
                            _cls.cupon = "";
                            _cls.entregado = Convert.ToDecimal(_reader["ENTREGADO"]);
                            _cls.CodTarjeta = _reader["CodTarjeta"] == null ? "" : _reader["CodTarjeta"].ToString();
                            _cls.FormaPago = _reader["FormaPago"] == null ? "" : _reader["FormaPago"].ToString();
                            _cls.AbrirCajon = Convert.ToBoolean(_reader["AbrirCajon"]);

                            _pagos.Add(_cls);
                        }
                    }
                }
            }

            return _pagos;
        }

        public static bool InsertCancelacion(int _fo, string _serie, int _numero, string _n, 
           int _nroNuevo, SqlConnection _con, SqlTransaction _tran)
        {
            string _sql = @"INSERT INTO TIQUETSPAG(FO, SERIE,NUMERO, N, CODFORMAPAGO, CODMONEDA, FECHAVENCIM)
                    SELECT FO, SERIE, @numeroNew, N, CODFORMAPAGO, CODMONEDA, GETDATE() FROM TIQUETSPAG
                    WHERE FO = @fo and SERIE = @serie and NUMERO = @numero and N = @n";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@numeroNew", _nroNuevo);
                    
                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("InsertCancelacion. " + ex.Message);
            }
        }
    }
}
