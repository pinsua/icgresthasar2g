﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace RestHasar2G
{
    public class Promociones
    {
        public int numlinea { get; set;}
        public string textoImprimir { get; set; }
        public decimal dto { get; set; }

        public static List<Promociones> GetPromociones(string _serie, int _numero, string _n, SqlConnection _con)
        {
            string _sql = @"SELECT TIQUETSLINPROMOCIONES.NUMLINEA, PROMOCIONES.TEXTOIMPRIMIR, TIQUETSLINPROMOCIONES.DTO, TIQUETSLINPROMOCIONES.IMPORTEDTO, TIQUETSLINPROMOCIONES.IMPORTEDTOIVA,
                TIQUETSLINPROMOCIONES.IMPORTEPROMOCION, TIQUETSLINPROMOCIONES.IMPORTEPROMOCIONIVA
                from TIQUETSLINPROMOCIONES 
                LEFT JOIN PROMOCIONES ON TIQUETSLINPROMOCIONES.IDPROMOCION = PROMOCIONES.IDPROMOCION 
                WHERE TIQUETSLINPROMOCIONES.SERIE = @serie and TIQUETSLINPROMOCIONES.N = @N and TIQUETSLINPROMOCIONES.NUMERO = @numero 
                ORDER BY TIQUETSLINPROMOCIONES.NUMLINEA Desc";

            List<Promociones> _lst = new List<Promociones>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Promociones _cls = new Promociones();
                            _cls.dto = Convert.ToDecimal(_reader["DTO"]);
                            _cls.numlinea = Convert.ToInt32(_reader["NUMLINEA"]);
                            _cls.textoImprimir = _reader["TEXTOIMPRIMIR"].ToString();

                            _lst.Add(_cls);
                        }
                    }
                }
            }

            return _lst;
        }
    }
}
