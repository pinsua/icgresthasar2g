﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace RestHasar2G
{
    public class Rem_transacciones
    {
        public static bool InsertRemTransacciones(string _terminal, string _serie, int _numero, string _N, SqlConnection _con, SqlTransaction _transac)
        {
            bool _rta = false;
            try
            {
                string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJANUM, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                select @Terminal, 0, 1, t.CAJA, case when isnull(t.Z, 0) = 0 then z.Z + 1 else t.Z end, t.SERIE, t.NUMERO, t.N, '0', 1
                from TIQUETSCAB t left join (select Caja, max(Numero) Z from ARQUEOS where Arqueo = 'Z' group by Caja) z on z.Caja = t.CAJA
                where t.SERIE = @Serie and t.NUMERO = @Numero and t.N = @N";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transac;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@Serie", _serie);
                    _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                    _cmd.Parameters.AddWithValue("@N", _N);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static bool InsertRemTransaccionesCancelacion(string _terminal, string _serie, int _numero, string _N, SqlConnection _con)
        {
            bool _rta = false;
            try
            {
                string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJANUM, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                select @Terminal, 55, 0, t.CAJA, case when isnull(t.Z, 0) = 0 then z.Z + 1 else t.Z end, t.SERIE, t.NUMERO, t.N, '0', 1
                from TIQUETSCAB t left join (select Caja, max(Numero) Z from ARQUEOS where Arqueo = 'Z' group by Caja) z on z.Caja = t.CAJA
                where t.SERIE = @Serie and t.NUMERO = @Numero and t.N = @N";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@Serie", _serie);
                    _cmd.Parameters.AddWithValue("@Numero", Convert.ToInt32(_numero));
                    _cmd.Parameters.AddWithValue("@N", _N);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static bool InsertRemTransaccionesCierreZ(string _terminal, int _NroZ, int _caja, SqlConnection _con)
        {
            bool _rta = false;
            
            try
            {
                string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJANUM, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                select @Terminal, 0, 1, t.CAJA, case when isnull(t.Z, 0) = 0 then z.Z + 1 else t.Z end, t.SERIE, t.NUMERO, t.N, '0', 1
                from TIQUETSCAB t left join (select Caja, max(Numero) Z from ARQUEOS where Arqueo = 'Z' group by Caja) z on z.Caja = t.CAJA
                inner join (select FO, SERIE, Max(NUMERO) as Numero, N from TIQUETSVENTACAMPOSLIBRES where Z_NRO = @NroZ group by FO, SERIE, N) cl 
                on t.SERIE =  cl.SERIE and t.NUMERO = cl.NUMERO and t.N = cl.N where t.caja = @Caja";

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@NroZ", _NroZ);
                    _cmd.Parameters.AddWithValue("@Caja", _caja);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }

        public static bool InsertRemTransaccionesCierreZ_X(string _terminal, int _NroZ, int _caja, SqlConnection _con)
        {
            bool _rta = false;

            try
            {
                string _sql = @"insert into rem_transacciones(TERMINAL, TIPO, ACCION, CAJANUM, Z, SERIE, NUMERO, N, FO, IDCENTRAL)
                VALUES (@Terminal, 125, 0, @Caja, @NroZ, '', -1, '', '0',1)"; 

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@Terminal", _terminal.ToUpper());
                    _cmd.Parameters.AddWithValue("@NroZ", _NroZ);
                    _cmd.Parameters.AddWithValue("@Caja", _caja);

                    _cmd.ExecuteNonQuery();
                }
                _rta = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _rta;
        }
    }
}
