﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RestHasar2G
{
    public class SiTefService
    {
        public class SendInvoices
        {
            public static bool SendNormalInvoice(string pSerie, string pNumero, string pN, string pIdTienda,
                string pIdTerminal, string pCUIT, string pCuitIsv, string pPathSend, bool pesClover, SqlConnection pConn)
            {
                string _delivery = "ELECTRONIC_INVOICE";
                string _letraFactura;
                string _tipoDocumento;
                int nrofiscal;
                string saleTicketType;

                string _cabecera = GetCabecera(pSerie, pNumero, pN, "N", pCuitIsv, pesClover, pConn,
                    out nrofiscal, out _letraFactura, out _tipoDocumento);
                string _fileName = "invoice_" + pSerie + pNumero + pN;

                if (_tipoDocumento == "EFACTURA")
                    saleTicketType = "SALE";
                else
                    saleTicketType = "RETURN";

                string _drta = GetJson(nrofiscal, pIdTienda, pCUIT, "CUIT", saleTicketType, pCuitIsv, pIdTerminal, _letraFactura,
                    _delivery, _tipoDocumento, pSerie, pNumero, pN, pConn);                

                pPathSend = pPathSend + _fileName + ".txt";

                _drta = Regex.Replace(_drta, @"\t|\n|\r", "");
                string envio = _cabecera + "ANF" + _drta.Length.ToString().PadLeft(5, '0') + _drta;
                envio = Regex.Replace(envio, @"\t|\n|\r", "");

                using (StreamWriter writer = new StreamWriter(pPathSend))
                {
                    writer.WriteLine(envio);
                }

                return true;
            }

            public static bool SendCanceledInvoice(string pSerie, string pNumero, string pN, string pIdTienda,
                string pIdTerminal, string pCUIT, string pCuitIsv, string pPathSend, SqlConnection pConn)
            {
                string _delivery = "ELECTRONIC_INVOICE";
                string _letraFactura;
                string _tipoDocumento;
                int nrofiscal;
                string saleTicketType;
                int numeroNuevo = Convert.ToInt32(pNumero) + 1;

                string _cabecera = GetCabecera(pSerie, numeroNuevo.ToString(), pN, "N", pConn,
                    out nrofiscal, out _letraFactura, out _tipoDocumento);                

                saleTicketType = "CANCELLED";                

                string _fileName = "invoice_" + pSerie + numeroNuevo.ToString() + pN;

                string _drta = GetJsonCancelacion(nrofiscal, pIdTienda, pCUIT, "CUIT", saleTicketType, pCuitIsv, pIdTerminal, _letraFactura,
                    _delivery, _tipoDocumento, pSerie, numeroNuevo.ToString(), pN, pConn);

                pPathSend = pPathSend + _fileName + ".txt";

                string envio = _cabecera + "ANF" + _drta.Length.ToString().PadLeft(5, '0') + _drta;
                envio = Regex.Replace(envio, @"\t|\n|\r", "");

                using (StreamWriter writer = new StreamWriter(pPathSend))
                {
                    writer.WriteLine(envio);
                }

                return true;
            }

            public static bool SendNormalInvoiceSql12(string pSerie, string pNumero, string pN, string pIdTienda,
                string pIdTerminal, string pCUIT, string pCuitIsv, string pPathSend, bool pesClover, SqlConnection pConn)
            {
                string _delivery = "ELECTRONIC_INVOICE";
                string _letraFactura;
                string _tipoDocumento;
                int nrofiscal;
                string saleTicketType;

                string _cabecera = GetCabecera(pSerie, pNumero, pN, "N", pCuitIsv, pesClover, pConn,
                    out nrofiscal, out _letraFactura, out _tipoDocumento);
                string _fileName = "invoice_" + pSerie + pNumero + pN;

                if (_tipoDocumento == "EFACTURA")
                    saleTicketType = "SALE";
                else
                    saleTicketType = "RETURN";

                string _drta = GetJsonInvoice(nrofiscal, pIdTienda, pCUIT, "CUIT", saleTicketType, pCuitIsv, pIdTerminal, _letraFactura,
                    _delivery, _tipoDocumento, pSerie, pNumero, pN, pConn);

                pPathSend = pPathSend + _fileName + ".txt";

                _drta = Regex.Replace(_drta, @"\t|\n|\r", "");
                _drta = Regex.Replace(_drta, "(\"(?:[^\"\\\\]|\\\\.)*\")|\\s+", "$1");
                string envio = _cabecera + "ANF" + _drta.Length.ToString().PadLeft(5, '0') + _drta;
                envio = Regex.Replace(envio, @"\t|\n|\r", "");
                //envio = Regex.Replace(envio, @"\s+", "");

                using (StreamWriter writer = new StreamWriter(pPathSend))
                {
                    writer.WriteLine(envio);
                }

                return true;
            }

            public static bool SendCanceledInvoiceSql12(string pSerie, string pNumero, string pN, string pIdTienda,
                string pIdTerminal, string pCUIT, string pCuitIsv, string pPathSend, SqlConnection pConn)
            {
                string _delivery = "ELECTRONIC_INVOICE";
                string _letraFactura;
                string _tipoDocumento;
                int nrofiscal;
                string saleTicketType;
                int numeroNuevo = Convert.ToInt32(pNumero) + 1;

                string _cabecera = GetCabecera(pSerie, numeroNuevo.ToString(), pN, "N", pConn,
                    out nrofiscal, out _letraFactura, out _tipoDocumento);

                saleTicketType = "CANCELLED";

                string _fileName = "invoice_" + pSerie + numeroNuevo.ToString() + pN;

                string _drta = GetJsonCancelacionSql12(nrofiscal, pIdTienda, pCUIT, "CUIT", saleTicketType, pCuitIsv, pIdTerminal, _letraFactura,
                    _delivery, _tipoDocumento, pSerie, numeroNuevo.ToString(), pN, pConn);

                pPathSend = pPathSend + _fileName + ".txt";

                _drta = Regex.Replace(_drta, "(\"(?:[^\"\\\\]|\\\\.)*\")|\\s+", "$1");
                string envio = _cabecera + "ANF" + _drta.Length.ToString().PadLeft(5, '0') + _drta;
                envio = Regex.Replace(envio, @"\t|\n|\r", "");
                //envio = Regex.Replace(envio, @"\s+", "");

                using (StreamWriter writer = new StreamWriter(pPathSend))
                {
                    writer.WriteLine(envio);
                }

                return true;
            }

            private static string GetCabecera(string serie, string numero, string n, string tipoOperacion, SqlConnection conection,
                out int numeroFiscal, out string letraFactura, out string tipoDocumento)
            {
                string _sql = @"SELECT CONVERT(varchar(8), TIQUETSCAB.FECHA,112) as FECHA, 
                    replace(convert(varchar, TIQUETSCAB.HORAFIN, 108),':','') as hora, 
                    TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.SERIEFISCAL2, 
                    replace(trim(str(Round(ABS(TIQUETSCAB.TOTALNETO), 2), 10, 2)), '.', '') as total, 
                    replace(trim(str(Round(ABS(TIQUETSPAG.IMPORTE), 2), 10, 2)), '.', '') as totalpag, 
                    FORMASPAGO.DESCRIPCION as tipopago, TIQUETSPAG.TITULARTARJETA, FORMASPAGO.METALICO, FORMASPAGO.TEXTOIMP, TIQUETSPAG.NUMLINEA 
                    FROM TIQUETSCAB Left join TIQUETSPAG on TIQUETSCAB.SERIE = TIQUETSPAG.SERIE and TIQUETSCAB.NUMERO = TIQUETSPAG.NUMERO 
                    and TIQUETSCAB.N = TIQUETSPAG.N Left join FORMASPAGO on TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                    WHERE TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";

                string rta = "";
                string nroFac = "";
                string fecha = "";
                string hora = "";
                string tipoVenta = "";
                string tipoOper = "";
                string icgRef = serie + numero + n;
                numeroFiscal = 0;
                letraFactura = "";
                tipoDocumento = "";
                string tipoOpe = "";

                if (conection.State == ConnectionState.Closed)
                    conection.Open();
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = conection;
                    _cmd.CommandType = CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@serie", serie);
                    _cmd.Parameters.AddWithValue("@numero", numero);
                    _cmd.Parameters.AddWithValue("@n", n);

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Agrupador
                                string _agrup = "";
                                //Vemos si es pago en Efectivo o no
                                bool esEfectivo = reader["METALICO"].ToString().ToUpper() == "T" ? true : false;
                                //Recuperamos el valor de TextImp
                                string textImp = reader["TEXTOIMP"] == DBNull.Value ? "" : reader["TEXTOIMP"].ToString();
                                bool esOtro = textImp == "O" ? true : false; 
                                //Vemos si es con clover o no.
                                bool esClover = false;                              
                                //Nro de Factura. TAG 004. Solo cuando no es clover
                                if (!esClover)
                                {
                                    if (String.IsNullOrEmpty(nroFac))
                                    {                                        
                                        string p1 = reader["NUMEROFISCAL"].ToString().PadLeft(8, '0');
                                        string p2 = reader["SERIEFISCAL"].ToString().PadLeft(5, '0');
                                        string p3 = p2 + p1;
                                        numeroFiscal = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                        nroFac = "004" + p3.Length.ToString().PadLeft(5, '0') + p3;
                                        _agrup = nroFac;
                                    }
                                }
                                //Fecha. TAG 005. Solo cuando no es clover.
                                if (!esClover)
                                {
                                    if (String.IsNullOrEmpty(fecha))
                                    {
                                        string p1 = reader["FECHA"].ToString();
                                        fecha = "005" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                        _agrup = _agrup + fecha;
                                    }
                                }
                                //Hora. TAG 006. Solo cuando no es Clover.
                                if (!esClover)
                                {
                                    if (String.IsNullOrEmpty(hora))
                                    {
                                        string p1 = reader["hora"].ToString();
                                        hora = "006" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                        _agrup = _agrup + hora;
                                    }
                                }
                                //Tipo de Venta. Tag 007
                                if (!esClover)
                                {
                                    if (String.IsNullOrEmpty(tipoVenta))
                                    {
                                        string p1 = reader["SERIEFISCAL2"].ToString();                                        
                                        if (p1.Substring(0, 3) == "006" || p1.Substring(0, 3) == "001")
                                        {
                                            string p2 = "C";
                                            tipoVenta = "007" + p2.Length.ToString().PadLeft(5, '0') + p2;
                                        }
                                        else
                                        {
                                            string p2 = "D";
                                            tipoVenta = "007" + p2.Length.ToString().PadLeft(5, '0') + p2;
                                        }
                                        _agrup = _agrup + tipoVenta;
                                    }
                                }
                                //Tipo de Operación. TAG 009.
                                if (!esClover)
                                {
                                    if (String.IsNullOrEmpty(tipoOper))
                                    {
                                        switch (reader["SERIEFISCAL2"].ToString().Substring(0, 3))
                                        {
                                            case "001": letraFactura = "A"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "002": case "003": letraFactura = "A"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                            case "006": letraFactura = "B"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "007": case "008": letraFactura = "B"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                            case "011": letraFactura = "C"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "012": case "013": letraFactura = "C"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        }
                                        tipoOper = "00900001" + tipoOpe;
                                        _agrup = _agrup + tipoOper;
                                    }
                                }
                                else
                                {
                                    switch (reader["SERIEFISCAL2"].ToString().Substring(0, 3))
                                    {
                                        case "001": letraFactura = "A"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "002": case "003": letraFactura = "A"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        case "006": letraFactura = "B"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "007": case "008": letraFactura = "B"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        case "011": letraFactura = "C"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "012": case "013": letraFactura = "C"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                    }
                                }
                                //Total de cada pago. CUANDO NO ES CLOVER. TAG 013
                                if (!esClover)
                                {
                                    string pago = reader["totalpag"].ToString();
                                    _agrup = _agrup + "013" + pago.Length.ToString().PadLeft(5, '0') + pago;
                                }
                                //Tipo de pago. Solo se envia si NO ES con CLOVER. TAG 014
                                if (!esClover)
                                {
                                    string p2 = "";

                                    if (esEfectivo)
                                        p2 = "E";
                                    else
                                    {
                                        if (esOtro)
                                            p2 = "O";
                                        else
                                            p2 = "T";
                                    }
                                    _agrup = _agrup + "014" + p2.Length.ToString().PadLeft(5, '0') + p2;
                                }
                                //Tipo de Tarjeta. Solo se envia SI NO ES Clover. 015
                                if (!esClover)
                                {
                                    if (!esEfectivo)
                                    {
                                        if (!esOtro)
                                        {
                                            string p1 = reader["TEXTOIMP"] == null ? "" : reader["TEXTOIMP"].ToString();
                                            if (!String.IsNullOrEmpty(p1))
                                            {
                                                _agrup = _agrup + "015" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                            }
                                        }
                                    }
                                }
                                //Referencia. SOLO VA CUANDO NO ES CLOVER. TAG 025.
                                if (!esClover)
                                {
                                    string numlin = reader["NUMLINEA"].ToString();
                                    string titular2 = reader["TITULARTARJETA"] == null ? "" : reader["TITULARTARJETA"].ToString();
                                    if (String.IsNullOrEmpty(titular2))
                                    {
                                        string _ref = icgRef + numlin.Trim();
                                        _agrup = _agrup + "025" + _ref.Length.ToString().PadLeft(5, '0') + _ref;
                                    }
                                    else
                                    {
                                        string p1 = titular2.Split('|')[1].ToString();
                                        if (!String.IsNullOrEmpty(p1))
                                        {
                                            _agrup = _agrup + "025" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                        }
                                    }
                                }
                                rta = rta + _agrup;
                            }
                        }
                    }
                }

                return rta;
            }

            private static string GetCabecera(string serie, string numero, string n, string tipoOperacion, string cuitIsv, bool esClover, SqlConnection conection,
                out int numeroFiscal, out string letraFactura, out string tipoDocumento)
            {
                string _sql = @"SELECT CONVERT(varchar(8), TIQUETSCAB.FECHA,112) as FECHA, 
                    replace(convert(varchar, TIQUETSCAB.HORAFIN, 108),':','') as hora, 
                    TIQUETSCAB.SERIEFISCAL, TIQUETSCAB.NUMEROFISCAL, TIQUETSCAB.SERIEFISCAL2, 
                    replace(LTRIM(RTRIM(str(Round(ABS(TIQUETSCAB.TOTALNETO), 2), 10, 2))), '.', '') as total, 
                    replace(LTRIM(RTRIM(str(Round(ABS(TIQUETSPAG.IMPORTE), 2), 10, 2))), '.', '') as totalpag, 
                    FORMASPAGO.DESCRIPCION as tipopago, TIQUETSPAG.TITULARTARJETA, FORMASPAGO.METALICO, FORMASPAGO.TEXTOIMP, TIQUETSPAG.NUMLINEA 
                    FROM TIQUETSCAB Left join TIQUETSPAG on TIQUETSCAB.SERIE = TIQUETSPAG.SERIE and TIQUETSCAB.NUMERO = TIQUETSPAG.NUMERO 
                    and TIQUETSCAB.N = TIQUETSPAG.N Left join FORMASPAGO on TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                    WHERE TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";

                string t002 = "";
                string rta = "";
                string nroFac = "";
                string fecha = "";
                string hora = "";
                string tipoVenta = "";
                string tipoOper = "";
                string icgRef = serie + numero + n;
                numeroFiscal = 0;
                letraFactura = "";
                tipoDocumento = "";
                string tipoOpe = "";

                if (conection.State == ConnectionState.Closed)
                    conection.Open();
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = conection;
                    _cmd.CommandType = CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@serie", serie);
                    _cmd.Parameters.AddWithValue("@numero", numero);
                    _cmd.Parameters.AddWithValue("@n", n);

                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Agrupador
                                string _agrup = "";
                                //Vemos si es pago en Efectivo o no
                                bool esEfectivo = reader["METALICO"].ToString().ToUpper() == "T" ? true : false;
                                //Recuperamos el valor de TextImp
                                string textImp = reader["TEXTOIMP"] == DBNull.Value ? "" : reader["TEXTOIMP"].ToString();
                                bool esOtro = textImp == "O" ? true : false;
                                //Ahora tenemos que poner el TAG 002. MerchantID
                                if (String.IsNullOrEmpty(t002))
                                {
                                    t002 = "002" + cuitIsv.Length.ToString().PadLeft(5, '0') + cuitIsv;
                                    _agrup = t002;
                                }

                                //Nro de Factura. TAG 004. Solo cuando no es clover. Ahora va siempre.
                                if (String.IsNullOrEmpty(nroFac))
                                {
                                    string p1 = reader["NUMEROFISCAL"].ToString().PadLeft(8, '0');
                                    string p2 = reader["SERIEFISCAL"].ToString().PadLeft(5, '0');
                                    string p3 = p2 + p1;
                                    numeroFiscal = Convert.ToInt32(reader["NUMEROFISCAL"]);
                                    nroFac = "004" + p3.Length.ToString().PadLeft(5, '0') + p3;
                                    _agrup = _agrup + nroFac;
                                }
                                if (String.IsNullOrEmpty(fecha))
                                {
                                    string p1 = reader["FECHA"].ToString();
                                    fecha = "005" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                    _agrup = _agrup + fecha;
                                }
                                //Hora. TAG 006. Solo cuando no es Clover. Ahora va siempre.
                                if (String.IsNullOrEmpty(hora))
                                {
                                    string p1 = reader["hora"].ToString();
                                    hora = "006" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                    _agrup = _agrup + hora;
                                }
                                //Tipo de Venta. Tag 007
                                if (!esClover)
                                {
                                    if (String.IsNullOrEmpty(tipoVenta))
                                    {
                                        string p1 = reader["SERIEFISCAL2"].ToString();
                                        if (p1.Substring(0, 3) == "006" || p1.Substring(0, 3) == "001")
                                        {
                                            string p2 = "C";
                                            tipoVenta = "007" + p2.Length.ToString().PadLeft(5, '0') + p2;
                                        }
                                        else
                                        {
                                            string p2 = "D";
                                            tipoVenta = "007" + p2.Length.ToString().PadLeft(5, '0') + p2;
                                        }
                                        _agrup = _agrup + tipoVenta;
                                    }
                                }
                                //Tipo de Operación. TAG 009.
                                if (!esClover || esEfectivo)
                                {
                                    if (String.IsNullOrEmpty(tipoOper))
                                    {
                                        switch (reader["SERIEFISCAL2"].ToString().Substring(0, 3))
                                        {
                                            case "001": letraFactura = "A"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "002": case "003": letraFactura = "A"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                            case "006": letraFactura = "B"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "007": case "008": letraFactura = "B"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                            case "011": letraFactura = "C"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                            case "012": case "013": letraFactura = "C"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        }
                                        tipoOper = "00900001" + tipoOpe;
                                        _agrup = _agrup + tipoOper;
                                    }
                                }
                                else
                                {
                                    switch (reader["SERIEFISCAL2"].ToString().Substring(0, 3))
                                    {
                                        case "001": letraFactura = "A"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "002": case "003": letraFactura = "A"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        case "006": letraFactura = "B"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "007": case "008": letraFactura = "B"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                        case "011": letraFactura = "C"; tipoDocumento = "EFACTURA"; tipoOpe = "N"; break;
                                        case "012": case "013": letraFactura = "C"; tipoDocumento = "NC_EFACTURA"; tipoOpe = "A"; break;
                                    }
                                }
                                //Total de cada pago. CUANDO NO ES CLOVER. TAG 013
                                if (!esClover || esEfectivo)
                                {
                                    string pago = reader["totalpag"].ToString();
                                    _agrup = _agrup + "013" + pago.Length.ToString().PadLeft(5, '0') + pago;
                                }
                                //Tipo de pago. Solo se envia si NO ES con CLOVER. TAG 014
                                if (!esClover || esEfectivo)
                                {
                                    string p2 = "";

                                    if (esEfectivo)
                                        p2 = "E";
                                    else
                                    {
                                        if (esOtro)
                                            p2 = "O";
                                        else
                                            p2 = "T";
                                    }
                                    _agrup = _agrup + "014" + p2.Length.ToString().PadLeft(5, '0') + p2;
                                }
                                //Tipo de Tarjeta. Solo se envia SI NO ES Clover. 015
                                if (!esClover)
                                {
                                    if (!esEfectivo)
                                    {
                                        if (!esOtro)
                                        {
                                            string p1 = reader["TEXTOIMP"] == null ? "" : reader["TEXTOIMP"].ToString();
                                            if (!String.IsNullOrEmpty(p1))
                                            {
                                                _agrup = _agrup + "015" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                            }
                                        }
                                    }
                                }
                                //Referencia. SOLO VA CUANDO NO ES CLOVER. TAG 025.
                                string numlin = reader["NUMLINEA"].ToString();
                                string titular2 = reader["TITULARTARJETA"] == null ? "" : reader["TITULARTARJETA"].ToString();
                                if (String.IsNullOrEmpty(titular2))
                                {
                                    string _ref = icgRef + numlin.Trim();
                                    _agrup = _agrup + "025" + _ref.Length.ToString().PadLeft(5, '0') + _ref;
                                }
                                else
                                {
                                    string p1 = titular2.Split('|')[1].ToString();
                                    if (!String.IsNullOrEmpty(p1))
                                    {
                                        _agrup = _agrup + "025" + p1.Length.ToString().PadLeft(5, '0') + p1;
                                    }
                                }
                                rta = rta + _agrup;
                            }
                        }
                    }
                }

                return rta;
            }

            private static string GetJson(int _nroFac, string _sitefCode, string _merchantDoc, string _merchantDocType, string saleTicketType,
                string merchantId, string terminalId, string letraFac, string invoiceDelivery, string invoicedocType,
                string serie, string numero, string n, SqlConnection connection)
            {
                string _json;
                string _sql = @"SELECT '2' as [SEVersion.Number], 
                    '#CODELOJA#' as [Merchant.SiTefCode], 
                    '#DocMerchant#' as [Merchant.DocumentMerchant], 
                    '#CUIT#' as [Merchant.DocumentMerchantType], 
                    '#SALE#' as [saleTicketType], 
                    '#MerchantId#' as [merchantId], 
                    '#TerminalClover#' as [Terminal], 
                    (SELECT distinct '#LetraFac#' as [InvoiceNumber.Type], 
                    REPLACE(STR(TIQUETSCAB.SERIEFISCAL, 5), SPACE(1), '0') as [InvoiceNumber.FiscalPointSaleNumber], 
                    REPLACE(STR(TIQUETSCAB.NUMEROFISCAL, 8), SPACE(1), '0') as [InvoiceNumber.Number], 
                    '#InvoiceDeliveryMethod#' as [InvoiceNumber.InvoiceDeliveryMethod], 
                    '#documentType#' as [InvoiceNumber.documentType], 
                    CONVERT(varchar(8), TIQUETSCAB.FECHA, 112) as [Date], 
                    replace(convert(varchar, TIQUETSCAB.HORAFIN, 108), ':', '') as [Time], 
                    'Publico' as [PriceList], 
                    case when TIQUETSCAB.CODCLIENTE = 0 then 'Consumidor Final' else clientes.nombrecliente end as 'Customer.Name', 
                    case when TIQUETSCAB.CODCLIENTE = 0 then 'Consumidor Final' else REGIMENFACTURACION.DESCRIPCION end as 'Customer.FiscalSituation', 
                    case when TIQUETSCAB.CODCLIENTE = 0 then '0' else clientes.cif end as 'Customer.Number', 
                    case when TIQUETSCAB.CODCLIENTE = 0 then 'DNI' else 'DNI' end as 'Customer.DocumentType', 
                    (SELECT trim(STR(TIQUETSLIN.CODARTICULO)) as 'ProductSKU', 
                    TIQUETSLIN.DESCRIPCION as 'ProductDescription', 
                    trim(str(Cast(TIQUETSLIN.UNIDADES as decimal(10, 2)), 10, 2)) as 'Quantity', 
                    trim(str(Round(ABS(TIQUETSLIN.PRECIOIVA), 2), 10, 2)) as 'Price', 
                    trim(str(round(ABS(TIQUETSLIN.DTO), 2), 10, 2)) as 'DiscountPercentage', 
                    trim(str(round(ABS(TIQUETSLIN.PRECIOIVA) * ABS(TIQUETSLIN.DTO) / 100, 2), 10, 2)) as  'DiscountAmount', 
                    trim(str(Round(ABS(TIQUETSLIN.PRECIOIVA) * TIQUETSLIN.UNIDADES, 2) - round(ABS(TIQUETSLIN.PRECIOIVA) * ABS(TIQUETSLIN.DTO) / 100, 2), 10, 2)) as 'Amount' FROM TIQUETSLIN 
                    WHERE TIQUETSCAB.NUMERO = TIQUETSLIN.NUMERO and TIQUETSCAB.SERIE = TIQUETSLIN.SERIE and TIQUETSCAB.N = TIQUETSLIN.N 
                    FOR JSON PATH)[Products], 
                    (SELECT 
                    CASE WHEN FORMASPAGO.METALICO = 'T' THEN 'P' ELSE FORMASPAGO.TEXTOIMP + '1' END as 'PaymentCode', 
                    CASE WHEN FORMASPAGO.METALICO = 'T' THEN 'Pesos' ELSE TRIM(FORMASPAGO.DESCRIPCION) END AS 'PaymentDescription', 
                    CASE WHEN FORMASPAGO.METALICO = 'T' THEN 'P' ELSE FORMASPAGO.TEXTOIMP END as 'CardBrand', 
                    CONVERT(varchar(8), TIQUETSCAB.FECHA, 112) as 'Date', 
                    replace(convert(varchar, TIQUETSCAB.HORAFIN, 108), ':', '') as 'Time', 
                    FORMASPAGO.DESCIDCOBRO AS 'Type',
                    '$' as 'Symbol',
                    trim(str(Round(ABS(TIQUETSPAG.IMPORTE), 2), 10, 2)) as 'Amount', 
                    case when trim(TIQUETSPAG.NUMTXNTEF) = '' then TIQUETSCAB.SERIE + trim(str(TIQUETSCAB.NUMERO)) + TIQUETSCAB.N + trim(str(TIQUETSPAG.NUMLINEA)) 
                    else SUBSTRING(TIQUETSPAG.TITULARTARJETA,CHARINDEX('|',TIQUETSPAG.TITULARTARJETA)+1,LEN(TIQUETSPAG.TITULARTARJETA)-CHARINDEX('|',TIQUETSPAG.TITULARTARJETA)) end as 'PaymentID' 
                    FROM TIQUETSPAG INNER JOIN FORMASPAGO ON TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                    WHERE TIQUETSPAG.NUMERO = TIQUETSCAB.NUMERO and TIQUETSPAG.SERIE = TIQUETSCAB.SERIE and TIQUETSPAG.N = TIQUETSCAB.N 
                    FOR JSON PATH )[PaymentValues], 
                    trim(str(Round(ABS(TIQUETSCAB.TOTALBRUTO) - (ABS(TIQUETSTOT.TOTDTOCOMERC) + ABS(TIQUETSTOT.TOTDTOPP)), 2), 10, 2)) as [SubTotal], 
                    trim(str(Round(ABS(TIQUETSTOT.DTOCOMERC) + ABS(TIQUETSTOT.DTOPP), 2), 10, 2)) as [DiscountPercentage], 
                    trim(str(Round(ABS(TIQUETSTOT.TOTDTOCOMERC) + ABS(TIQUETSTOT.TOTDTOPP), 2), 10, 2)) as [DiscountAmount], 
                    (SELECT trim(str(Round(ABS(TIQUETSTOT.IVA), 2), 10, 2)) as 'Percentage', 
                    trim(str(Round(ABS(TIQUETSTOT.TOTIVA), 2), 10, 2)) as 'Amount' 
                    FROM TIQUETSTOT 
                    WHERE TIQUETSTOT.IVA >= 0 AND TIQUETSTOT.CODDTO = -1 AND TIQUETSTOT.SERIE = TIQUETSCAB.SERIE 
                    AND TIQUETSTOT.NUMERO = TIQUETSCAB.NUMERO AND TIQUETSTOT.N = TIQUETSCAB.N 
                    FOR JSON PATH) [IVA], 
                    trim(str(Round(0, 2), 10, 2)) as [OtherTaxes], 
                    trim(str(Round(isnull(ABS(TIQUETSCAB.TOTALNETO), 0), 2), 10, 2)) as [AmountTotal] 
                    from TIQUETSCAB Inner join TIQUETSLIN On TIQUETSCAB.NUMERO = TIQUETSLIN.NUMERO and TIQUETSCAB.SERIE = TIQUETSLIN.SERIE and TIQUETSCAB.N = TIQUETSLIN.N 
                    LEFT join TIQUETSTOT On TIQUETSCAB.SERIE = TIQUETSTOT.SERIE and TIQUETSCAB.NUMERO = TIQUETSTOT.NUMERO and TIQUETSCAB.N = TIQUETSTOT.N 
                    Left join TIQUETSPAG ON TIQUETSCAB.SERIE = TIQUETSPAG.SERIE and TIQUETSCAB.NUMERO = TIQUETSPAG.NUMERO and TIQUETSCAB.N = TIQUETSPAG.N 
                    LEFT join CLIENTES on  TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE 
                    left join REGIMENFACTURACION on CLIENTES.REGIMFACT = REGIMENFACTURACION.CODREGIMEN 
                    where TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n 
                    FOR JSON PATH)[Invoice] for json path, Without_Array_Wrapper";

                _sql = _sql.Replace("#CODELOJA#", _sitefCode);
                _sql = _sql.Replace("#DocMerchant#", _merchantDoc);
                _sql = _sql.Replace("#CUIT#", _merchantDocType);
                _sql = _sql.Replace("#SALE#", saleTicketType);
                _sql = _sql.Replace("#MerchantId#", merchantId);
                _sql = _sql.Replace("#TerminalClover#", terminalId);
                _sql = _sql.Replace("#LetraFac#", letraFac);
                _sql = _sql.Replace("#InvoiceDeliveryMethod#", invoiceDelivery);
                _sql = _sql.Replace("#documentType#", invoicedocType);

                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = connection;
                    _cmd.CommandType = CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@serie", serie);
                    _cmd.Parameters.AddWithValue("@numero", numero);
                    _cmd.Parameters.AddWithValue("@n", n);

                    StringBuilder sb = new StringBuilder();
                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            sb.Append(reader.GetSqlString(0).Value);
                    }
                    _json = sb.ToString();
                }

                return _json;
            }

            private static string GetJsonCancelacion(int _nroFac, string _sitefCode, string _merchantDoc, string _merchantDocType, string saleTicketType,
                string merchantId, string terminalId, string letraFac, string invoiceDelivery, string invoicedocType,
                string serie, string numero, string n, SqlConnection connection)
            {
                string _json;

                string _sql = "SELECT '2' as [SEVersion.Number], '#CODELOJA#' as [Merchant.SiTefCode], '#DocMerchant#' as [Merchant.DocumentMerchant], " +
                    "'#CUIT#' as [Merchant.DocumentMerchantType], '#SALE#' as [saleTicketType], '#MerchantId#' as [merchantId], " +
                    "'#TerminalClover#' as [Terminal], " +
                    "(SELECT '#LetraFac#' as [InvoiceNumber.Type], REPLACE(STR(TIQUETSCAB.SERIEFISCAL, 5), SPACE(1), '0') as [InvoiceNumber.FiscalPointSaleNumber], " +
                    "REPLACE(STR(TIQUETSCAB.NUMEROFISCAL, 8), SPACE(1), '0') as [InvoiceNumber.Number], '#InvoiceDeliveryMethod#' as [InvoiceNumber.InvoiceDeliveryMethod], " +
                    "'#documentType#' as [InvoiceNumber.documentType], CONVERT(varchar(8), TIQUETSCAB.FECHA, 112) as [Date], " +
                    "replace(convert(varchar, TIQUETSCAB.HORAFIN, 108), ':', '') as [Time], 'Publico' as [PriceList], " +
                    "'' as 'Customer.Name', '' as 'Customer.FiscalSituation', '0' as 'Customer.Number', 'DNI' as 'Customer.DocumentType', " +
                    "(SELECT '' as 'ProductSKU', '' as 'ProductDescription', '0' as 'Quantity', '0' as 'Price', '0' as 'DiscountPercentage', " +
                    "'0' as  'DiscountAmount', '0' as 'Amount' FOR JSON PATH)[Products], " +
                    "(SELECT '0' as 'Date', '0' as 'Time', '0' as 'Amount', '#PaymentID#' as 'PaymentID' FOR JSON PATH )[PaymentValues], " +
                    "'0' as [SubTotal], '0' as [DiscountPercentage], '0' as [DiscountAmount], " +
                    "(SELECT '0' as 'Percentage', '0' as 'Amount' FOR JSON PATH) [IVA], " +
                    "'0' as [OtherTaxes], '0' as [AmountTotal] " +
                    "from TIQUETSCAB where TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n " +
                    "FOR JSON PATH)[Invoice] for json path, Without_Array_Wrapper;";

                _sql = _sql.Replace("#CODELOJA#", _sitefCode);
                _sql = _sql.Replace("#DocMerchant#", _merchantDoc);
                _sql = _sql.Replace("#CUIT#", _merchantDocType);
                _sql = _sql.Replace("#SALE#", saleTicketType);
                _sql = _sql.Replace("#MerchantId#", merchantId);
                _sql = _sql.Replace("#TerminalClover#", terminalId);
                _sql = _sql.Replace("#LetraFac#", letraFac);
                _sql = _sql.Replace("#InvoiceDeliveryMethod#", invoiceDelivery);
                _sql = _sql.Replace("#documentType#", invoicedocType);
                _sql = _sql.Replace("#PaymentID#", serie + numero + n + "0");

                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = connection;
                    _cmd.CommandType = CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Parameters.AddWithValue("@serie", serie);
                    _cmd.Parameters.AddWithValue("@numero", numero);
                    _cmd.Parameters.AddWithValue("@n", n);

                    StringBuilder sb = new StringBuilder();
                    using (SqlDataReader reader = _cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            sb.Append(reader.GetSqlString(0).Value);
                    }
                    _json = sb.ToString();
                }

                return _json;
            }

            private static string GetJsonCancelacionSql12(int _nroFac, string _sitefCode, string _merchantDoc, string _merchantDocType, string saleTicketType,
                string merchantId, string terminalId, string letraFac, string invoiceDelivery, string invoicedocType,
                string serie, string numero, string n, SqlConnection connection)
            {
                string _json;
                string _ptoVta = "";
                string _numeroFiscal ="";
                string _paymentId = serie + numero + n + "0";
                string _date = "";
                string _time = "";
                string _sql = @"select SERIEFISCAL, SERIEFISCAL2, NUMEROFISCAL, CONVERT(varchar(8), TIQUETSCAB.FECHA, 112) as [Date],
                    replace(convert(varchar, TIQUETSCAB.HORAFIN, 108), ':', '') as [Time] from TIQUETSCAB 
                    where TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";

                try
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sql;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    _ptoVta = reader["SERIEFISCAL"].ToString();
                                    _numeroFiscal = reader["NUMEROFISCAL"].ToString().PadLeft(8, '0');
                                    _date = reader["Date"].ToString();
                                    _time = reader["Time"].ToString();
                                }
                            }
                        }
                    }
                    InvoiceJson _invoiceJson = new InvoiceJson();
                    SEVersion sEVersion = new SEVersion { Number = "2" };
                    Merchant merchant = new Merchant { DocumentMerchant = _merchantDoc, DocumentMerchantType = _merchantDocType, SiTefCode = _sitefCode };
                    _invoiceJson.SEVersion = sEVersion;
                    _invoiceJson.Merchant = merchant;
                    _invoiceJson.saleTicketType = saleTicketType;
                    _invoiceJson.merchantId = merchantId;
                    _invoiceJson.Terminal = terminalId;
                    Invoice invoice = new Invoice();
                    InvoiceNumber invoiceNumber = new InvoiceNumber { Type = letraFac, Number = _numeroFiscal, documentType = invoicedocType, FiscalPointSaleNumber = _ptoVta, InvoiceDeliveryMethod = invoiceDelivery };
                    Customer customer = new Customer { DocumentType = "DNI", FiscalSituation = "", Name = "", Number = "" };
                    Product product = new Product { Amount = "0", DiscountAmount = "0", DiscountPercentage = "0", Price = "0", ProductDescription = "", ProductSKU = "", Quantity = "0" };
                    PaymentValue paymentValue = new PaymentValue { Date = "0", Time = "0", Amount = "0", PaymentID = _paymentId };
                    Iva iva = new Iva { Amount = "0", Percentage = "0" };
                    invoice.AmountTotal = "0";
                    invoice.Customer = customer;
                    invoice.Date = _date;
                    invoice.DiscountAmount = "0";
                    invoice.DiscountPercentage = "0";
                    invoice.InvoiceNumber = invoiceNumber;
                    List<Iva> ivas = new List<Iva>();
                    ivas.Add(iva);
                    invoice.IVA = ivas;
                    invoice.OtherTaxes = "0";
                    List<PaymentValue> paymentValues = new List<PaymentValue>();
                    paymentValues.Add(paymentValue);
                    invoice.PaymentValues = paymentValues;
                    invoice.PriceList = "Publico";
                    List<Product> products = new List<Product>();
                    products.Add(product);
                    invoice.Products = products;
                    invoice.SubTotal = "0";
                    invoice.Time = _time;

                    List<Invoice> _lst = new List<Invoice>();
                    _lst.Add(invoice);
                    _invoiceJson.Invoice = _lst;

                    _json = JsonConvert.SerializeObject(_invoiceJson, Formatting.Indented);
                }
                catch 
                {
                    _json = "";
                }

                return _json;
            }

            private static string GetJsonInvoice(int _nroFac, string _sitefCode, string _merchantDoc, string _merchantDocType, string saleTicketType,
                string merchantId, string terminalId, string letraFac, string invoiceDelivery, string invoicedocType,
                string serie, string numero, string n, SqlConnection connection)
            {
                string json;

                string _sqlInvoiceCustomer = @"SELECT REPLACE(STR(TIQUETSCAB.SERIEFISCAL, 5), SPACE(1), '0') as [InvoiceNumber.FiscalPointSaleNumber], 
                    REPLACE(STR(TIQUETSCAB.NUMEROFISCAL, 8), SPACE(1), '0') as [InvoiceNumber.Number], 
                    CONVERT(varchar(8), TIQUETSCAB.FECHA, 112) as [Date], 
                    REPLACE(convert(varchar, TIQUETSCAB.HORAFIN, 108), ':', '') as [Time], 
                    CASE WHEN TIQUETSCAB.CODCLIENTE = 0 THEN 'Consumidor Final' ELSE clientes.nombrecliente END as 'Customer.Name', 
                    CASE WHEN TIQUETSCAB.CODCLIENTE = 0 THEN 'Consumidor Final' ELSE REGIMENFACTURACION.DESCRIPCION END as 'Customer.FiscalSituation', 
                    CASE WHEN TIQUETSCAB.CODCLIENTE = 0 THEN '0' ELSE clientes.cif END as 'Customer.Number', 
                    CASE WHEN TIQUETSCAB.CODCLIENTE = 0 THEN 'DNI' else 'DNI' END as 'Customer.DocumentType', 
					LTRIM(RTRIM(str(Round(isnull(ABS(TIQUETSCAB.TOTALNETO), 0), 2), 10, 2))) as [AmountTotal], 
                    LTRIM(RTRIM(str(Round(0, 2), 10, 2))) as [OtherTaxes]
                    FROM TIQUETSCAB LEFT JOIN CLIENTES ON TIQUETSCAB.CODCLIENTE = CLIENTES.CODCLIENTE
                    LEFT JOIN REGIMENFACTURACION ON CLIENTES.REGIMFACT = REGIMENFACTURACION.CODREGIMEN
                    WHERE TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";
                string _sqlProducts = @"SELECT LTRIM(RTRIM(STR(TIQUETSLIN.CODARTICULO))) as 'ProductSKU', 
                    TIQUETSLIN.DESCRIPCION as 'ProductDescription', 
                    LTRIM(RTRIM(str(Cast(TIQUETSLIN.UNIDADES as decimal(10, 2)), 10, 2))) as 'Quantity', 
                    LTRIM(RTRIM(str(Round(ABS(TIQUETSLIN.PRECIOIVA), 2), 10, 2))) as 'Price', 
                    LTRIM(RTRIM(str(round(ABS(TIQUETSLIN.DTO), 2), 10, 2))) as 'DiscountPercentage', 
                    LTRIM(RTRIM(str(round(ABS(TIQUETSLIN.PRECIOIVA) * ABS(TIQUETSLIN.DTO) / 100, 2), 10, 2))) as  'DiscountAmount', 
                    LTRIM(RTRIM(str(Round(ABS(TIQUETSLIN.PRECIOIVA) * TIQUETSLIN.UNIDADES, 2) - round(ABS(TIQUETSLIN.PRECIOIVA) * ABS(TIQUETSLIN.DTO) / 100, 2), 10, 2))) as 'Amount' 
                    FROM TIQUETSCAB inner join TIQUETSLIN on TIQUETSCAB.NUMERO = TIQUETSLIN.NUMERO and TIQUETSCAB.SERIE = TIQUETSLIN.SERIE and TIQUETSCAB.N = TIQUETSLIN.N 
                    WHERE TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";
                string _sqlPayments = @"SELECT CASE WHEN FORMASPAGO.METALICO = 'T' THEN 'P' ELSE FORMASPAGO.TEXTOIMP + '1' END as 'PaymentCode', 
                    CASE WHEN FORMASPAGO.METALICO = 'T' THEN 'Pesos' ELSE LTRIM(RTRIM(FORMASPAGO.DESCRIPCION)) END AS 'PaymentDescription', 
                    CASE WHEN FORMASPAGO.METALICO = 'T' THEN 'P' ELSE FORMASPAGO.TEXTOIMP END as 'CardBrand', 
                    FORMASPAGO.DESCIDCOBRO AS 'Type',
                    '$' as 'Symbol',
                    LTRIM(RTRIM(str(Round(ABS(TIQUETSPAG.IMPORTE), 2), 10, 2))) as 'Amount',
                    TIQUETSCAB.SERIE + LTRIM(RTRIM(str(TIQUETSCAB.NUMERO))) + TIQUETSCAB.N + LTRIM(RTRIM(str(TIQUETSPAG.NUMLINEA))) as 'PaymentID1', 
                    SUBSTRING(TIQUETSPAG.TITULARTARJETA,CHARINDEX('|',TIQUETSPAG.TITULARTARJETA)+1,LEN(TIQUETSPAG.TITULARTARJETA)-CHARINDEX('|',TIQUETSPAG.TITULARTARJETA)) as 'PaymentID2' 
                    FROM TIQUETSCAB inner join TIQUETSPAG on TIQUETSPAG.NUMERO = TIQUETSCAB.NUMERO and TIQUETSPAG.SERIE = TIQUETSCAB.SERIE and TIQUETSPAG.N = TIQUETSCAB.N
                    INNER JOIN FORMASPAGO ON TIQUETSPAG.CODFORMAPAGO = FORMASPAGO.CODFORMAPAGO 
                    WHERE TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";
                string _sqlIva = @"SELECT LTRIM(RTRIM(str(Round(ABS(TIQUETSTOT.IVA), 2), 10, 2))) as 'Percentage', 
                    LTRIM(RTRIM(str(Round(ABS(TIQUETSTOT.TOTIVA), 2), 10, 2))) as 'Amount' 
                    FROM TIQUETSCAB inner join TIQUETSTOT on TIQUETSCAB.SERIE =TIQUETSTOT.SERIE 
                    AND TIQUETSCAB.NUMERO = TIQUETSTOT.NUMERO AND TIQUETSCAB.N = TIQUETSTOT.N 
                    WHERE TIQUETSTOT.IVA >= 0 AND TIQUETSTOT.CODDTO = -1 and
                    TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";
                string _sqlTotales = @"SELECT  LTRIM(RTRIM(str(Round(ABS(TIQUETSCAB.TOTALBRUTO) - (ABS(TIQUETSTOT.TOTDTOCOMERC) + ABS(TIQUETSTOT.TOTDTOPP)), 2), 10, 2))) as [SubTotal], 
                    LTRIM(RTRIM(str(Round(ABS(TIQUETSTOT.DTOCOMERC) + ABS(TIQUETSTOT.DTOPP), 2), 10, 2))) as [DiscountPercentage], 
                    LTRIM(RTRIM(str(Round(ABS(TIQUETSTOT.TOTDTOCOMERC) + ABS(TIQUETSTOT.TOTDTOPP), 2), 10, 2))) as [DiscountAmount]
                    FROM TIQUETSCAB inner join TIQUETSTOT on TIQUETSCAB.SERIE =TIQUETSTOT.SERIE 
                    AND TIQUETSCAB.NUMERO = TIQUETSTOT.NUMERO AND TIQUETSCAB.N = TIQUETSTOT.N 
                    WHERE TIQUETSCAB.SERIE = @serie and TIQUETSCAB.NUMERO = @numero and TIQUETSCAB.N = @n";

                try
                {
                    InvoiceJson _invoiceJson = new InvoiceJson();
                    SEVersion sEVersion = new SEVersion { Number = "2" };
                    Merchant merchant = new Merchant { DocumentMerchant = _merchantDoc, DocumentMerchantType = _merchantDocType, SiTefCode = _sitefCode };
                    _invoiceJson.SEVersion = sEVersion;
                    _invoiceJson.Merchant = merchant;
                    _invoiceJson.saleTicketType = saleTicketType;
                    _invoiceJson.merchantId = merchantId;
                    _invoiceJson.Terminal = terminalId;
                    Invoice invoice = new Invoice();
                    InvoiceNumber invoiceNumber = new InvoiceNumber();
                    Customer customer = new Customer();

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    //InvoiceNumber y Customer.
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlInvoiceCustomer;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    invoiceNumber.documentType = invoicedocType;
                                    invoiceNumber.FiscalPointSaleNumber = reader["InvoiceNumber.FiscalPointSaleNumber"].ToString();
                                    invoiceNumber.InvoiceDeliveryMethod = invoiceDelivery;
                                    invoiceNumber.Number = reader["InvoiceNumber.Number"].ToString();
                                    invoiceNumber.Type = letraFac;

                                    invoice.Date = reader["Date"].ToString();
                                    invoice.Time = reader["Time"].ToString();
                                    invoice.PriceList = "Publico";
                                    invoice.AmountTotal = reader["AmountTotal"].ToString();
                                    invoice.OtherTaxes = reader["OtherTaxes"].ToString();

                                    customer.DocumentType = reader["Customer.DocumentType"].ToString();
                                    customer.FiscalSituation = reader["Customer.FiscalSituation"].ToString();
                                    customer.Name = reader["Customer.Name"].ToString();
                                    customer.Number = reader["Customer.Number"].ToString();
                                }
                            }
                        }
                    }
                    //Productos
                    List<Product> products = new List<Product>();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlProducts;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    Product product = new Product();
                                    product.Amount = reader["Amount"].ToString();
                                    product.DiscountAmount = reader["DiscountAmount"].ToString();
                                    product.DiscountPercentage = reader["DiscountPercentage"].ToString();
                                    product.Price = reader["Price"].ToString();
                                    product.ProductDescription = reader["ProductDescription"].ToString();
                                    product.ProductSKU = reader["ProductSKU"].ToString();
                                    product.Quantity = reader["Quantity"].ToString();

                                    products.Add(product);
                                }
                            }
                        }
                    }
                    //Pagos
                    List<PaymentValue> paymentValues = new List<PaymentValue>();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlPayments;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    PaymentValue paymentValue = new PaymentValue();
                                    paymentValue.Amount = reader["Amount"].ToString();
                                    paymentValue.CardBrand = reader["CardBrand"].ToString();
                                    paymentValue.Date = invoice.Date;
                                    paymentValue.PaymentCode = reader["PaymentCode"].ToString();
                                    paymentValue.PaymentDescription = reader["PaymentDescription"].ToString();
                                    paymentValue.PaymentID = String.IsNullOrEmpty(reader["PaymentID2"].ToString()) ? reader["PaymentID1"].ToString() : reader["PaymentID2"].ToString();
                                    paymentValue.Symbol = reader["Symbol"].ToString();
                                    paymentValue.Time = invoice.Time;

                                    paymentValues.Add(paymentValue);
                                }
                            }
                        }
                    }
                    //IVAS
                    List<Iva> ivas = new List<Iva>();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlIva;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    Iva iva = new Iva();
                                    iva.Amount = reader["Amount"].ToString();
                                    iva.Percentage = reader["Percentage"].ToString();

                                    ivas.Add(iva);
                                }
                            }
                        }
                    }
                    //Totales
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.Connection = connection;
                        _cmd.CommandType = CommandType.Text;
                        _cmd.CommandText = _sqlTotales;
                        _cmd.Parameters.AddWithValue("@serie", serie);
                        _cmd.Parameters.AddWithValue("@numero", numero);
                        _cmd.Parameters.AddWithValue("@n", n);

                        using (SqlDataReader reader = _cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    invoice.SubTotal = reader["SubTotal"].ToString();
                                    invoice.DiscountPercentage = reader["DiscountPercentage"].ToString();
                                    invoice.DiscountAmount = reader["DiscountAmount"].ToString();
                                }
                            }
                        }
                    }

                    invoice.InvoiceNumber = invoiceNumber;
                    invoice.Customer = customer;
                    invoice.Products = products;
                    invoice.PaymentValues = paymentValues;
                    invoice.IVA = ivas;
                    List<Invoice> _lst = new List<Invoice>();
                    _lst.Add(invoice);
                    _invoiceJson.Invoice = _lst;

                    json = JsonConvert.SerializeObject(_invoiceJson, Formatting.Indented);
                }
                catch 
                {

                    json = "";
                }

                return json;
            }
        }

        public class InfoSitef
        {
            /// <summary>
            /// Id de la terminal
            /// </summary>
            public string sitefIdTerminal { get; set; }
            /// <summary>
            /// Id de la tienda
            /// </summary>
            public string sitefIdTienda { get; set; }
            /// <summary>
            /// Cuit de la tienda
            /// </summary>
            public string sitefCuit { get; set; }
            /// <summary>
            /// Cuit del ISV
            /// </summary>
            public string sitefCuitIsv { get; set; }
            /// <summary>
            /// Path donde se dejaran las invoice a enviar a la pasarela.
            /// </summary>
            public string pathSendInvoice { get; set; }
            /// <summary>
            /// Indica si el cliente USA o NO un clover.
            /// </summary>
            public bool usaClover { get; set; }
        }

        public class InvoiceJson
        {
            public SEVersion SEVersion { get; set; }
            public Merchant Merchant { get; set; }
            public string saleTicketType { get; set; }
            public string merchantId { get; set; }
            public string Terminal { get; set; }
            public List<Invoice> Invoice { get; set; }
        }

        public class SEVersion
        {
            public string Number { get; set; }
        }

        public class Merchant
        {
            public string SiTefCode { get; set; }
            public string DocumentMerchant { get; set; }
            public string DocumentMerchantType { get; set; }
        }

        public class Invoice
        {
            public InvoiceNumber InvoiceNumber { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string PriceList { get; set; }
            public Customer Customer { get; set; }
            public List<Product> Products { get; set; }
            public List<PaymentValue> PaymentValues { get; set; }
            public string SubTotal { get; set; }
            public string DiscountPercentage { get; set; }
            public string DiscountAmount { get; set; }
            public List<Iva> IVA { get; set; }
            public string OtherTaxes { get; set; }
            public string AmountTotal { get; set; }
        }

        public class InvoiceNumber
        {
            public string Type { get; set; }
            public string FiscalPointSaleNumber { get; set; }
            public string Number { get; set; }
            public string InvoiceDeliveryMethod { get; set; }
            public string documentType { get; set; }
        }

        public class Customer
        {
            public string Name { get; set; }
            public string FiscalSituation { get; set; }
            public string Number { get; set; }
            public string DocumentType { get; set; }
        }

        public class Product
        {
            public string ProductSKU { get; set; }
            public string ProductDescription { get; set; }
            public string Quantity { get; set; }
            public string Price { get; set; }
            public string DiscountPercentage { get; set; }
            public string DiscountAmount { get; set; }
            public string Amount { get; set; }
        }

        public class PaymentValue
        {
            public string PaymentCode { get; set; }
            public string PaymentDescription { get; set; }
            public string CardBrand { get; set; }
            public string Date { get; set; }            public string Time { get; set; }            public string Symbol { get; set; }            public string Amount { get; set; }            public string PaymentID { get; set; }        }

        public class Iva
        {
            public string Percentage { get; set; }
            public string Amount { get; set; }
        }
    }
}
