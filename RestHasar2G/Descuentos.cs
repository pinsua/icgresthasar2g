﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestHasar2G
{
    public class Descuentos
    {
        public int _numlinea { get; set; }
        public double _bruto { get; set; }
        public double _dtoComercial { get; set; }
        public double _dtoPp { get; set; }
        public double _totDtoComer { get; set; }
        public double _totDtoPp { get; set; }

        public static List<Descuentos> GetDescuentos(int _fo, string _serie, int _numero, string _N, SqlConnection _con)
        {
            string _sql = "SELECT TIQUETSTOT.numlinea, TIQUETSTOT.BRUTO, TIQUETSTOT.DTOCOMERC, TIQUETSTOT.TOTDTOCOMERC, TIQUETSTOT.DTOPP, TIQUETSTOT.TOTDTOPP " +
                "FROM TIQUETSTOT WHERE TIQUETSTOT.FO = @fo AND TIQUETSTOT.SERIE = @serie AND TIQUETSTOT.NUMERO = @numero AND TIQUETSTOT.N = @n";

            List<Descuentos> _lst = new List<Descuentos>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@fo", _fo);
                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@n", _N);
                _cmd.Parameters.AddWithValue("@numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Descuentos _cls = new Descuentos();
                            _cls._numlinea = _reader.GetInt32(0);
                            _cls._bruto = _reader.GetDouble(1);
                            _cls._dtoComercial = _reader.GetDouble(2);
                            _cls._totDtoComer = _reader.GetDouble(3);
                            _cls._dtoPp = _reader.GetDouble(4);                            
                            _cls._totDtoPp = _reader.GetDouble(5);

                            _lst.Add(_cls);
                        }
                    }
                }
            }

            return _lst;
        }
    }
}
