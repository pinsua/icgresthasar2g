﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestHasar2G;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace IcgInvocarCierreZ
{
    static class Program
    {
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public static hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            

            string _ip = "";
            string _caja = "";
            string _password = "";
            string _serverConfig = "";
            string _userConfig = "";
            string _catalogConfig = "";
            bool _monotributo = false;
            bool _hasarLog = false;
            string _terminal = "";
            string _tktregalo1 = "";
            string _tktregalo2 = "";
            string _tktregalo3 = "";
            string _pathIrsa = "";
            bool _rtaComprobantesOk = false;

            //Leemos el archivo de configuracion
            if (RestHasar2G.FuncionesVarias.LeerXmlConfiguracion(out _ip, out _caja, out _password, out _serverConfig,
                out _userConfig, out _catalogConfig, out _monotributo, out _terminal, out _tktregalo1, out _tktregalo2,
                out _tktregalo3, out _hasarLog, out _pathIrsa))
            {
                //Recuperado los datos vamos por la conexion.
                //Armamos el stringConnection.
                string strConnection = "Data Source=" + _serverConfig + ";Initial Catalog=" + _catalogConfig + ";User Id=" + _userConfig + ";Password=masterkey;";

                using (SqlConnection _con = new SqlConnection(strConnection))
                {
                    _con.Open();

                    _rtaComprobantesOk = RestHasar2G.FuncionesVarias.TengoComprobantesSinImprimmir(_con, _caja);

                    if (_con.State == System.Data.ConnectionState.Open)
                        _con.Close();
                }

                if (!_rtaComprobantesOk)
                {
                    Process p = Process.GetProcessesByName("FrontRest").FirstOrDefault();
                    if (p != null)
                    {
                        string _msjCbte = "Desea continuar con la impresión del Z fiscal?.";
                        //Enviamos el CierreZ
                        if (MessageBox.Show(new Form { TopMost = true }, _msjCbte, "ICG Argentina", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {

                            //Invocamos al cierre de ICG, por medio del ShortCut.
                            IntPtr h = p.MainWindowHandle;
                            SetForegroundWindow(h);
                            SendKeys.SendWait("%(z)");
                            SendKeys.Flush();

                        
                            hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal _cierre = new hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal();
                            hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ _zeta = new hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ();
                            string _msj;

                            try
                            {
                                //Conectamos.
                                RestHasar2G.Impresiones.Conectar(hasar, _hasarLog, _ip);
                                //ejecutamos el cierre Z.
                                _cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z);
                                _zeta = _cierre.Z;

                                _msj = "INFORME DIARIO DE CIERRE :" + Environment.NewLine +
                                  "Cierre 'Z' Nº            =[" + _zeta.getNumero() + "]" + Environment.NewLine +
                                  "Fecha del Cierre         =[" + _zeta.getFecha() + "]" + Environment.NewLine +
                                  "DF Emitidos              =[" + _zeta.getDF_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DF Cancelados            =[" + _zeta.getDF_CantidadCancelados() + "]" + Environment.NewLine +
                                  "DF Total                 =[" + string.Format("{0:0.00}", _zeta.getDF_Total() / 100) + "]" + Environment.NewLine +
                                  "DF Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getDF_TotalGravado() / 100) + "]" + Environment.NewLine +
                                  "DF Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getDF_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                  "DF Total Exento          =[" + string.Format("{0:0.00}", _zeta.getDF_TotalExento() / 100) + "]" + Environment.NewLine +
                                  "DF Total IVA             =[" + string.Format("{0:0.00}", _zeta.getDF_TotalIVA() / 100) + "]" + Environment.NewLine +
                                  "DF Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getDF_TotalTributos() / 100) + "]" + Environment.NewLine +
                                  "NC Emitidas              =[" + _zeta.getNC_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "NC Canceladas            =[" + _zeta.getNC_CantidadCancelados() + "]" + Environment.NewLine +
                                  "NC Total                 =[" + string.Format("{0:0.00}", _zeta.getNC_Total() / 100) + "]" + Environment.NewLine +
                                  "NC Total Gravado         =[" + string.Format("{0:0.00}", _zeta.getNC_TotalGravado() / 100) + "]" + Environment.NewLine +
                                  "NC Total No Gravado      =[" + string.Format("{0:0.00}", _zeta.getNC_TotalNoGravado() / 100) + "]" + Environment.NewLine +
                                  "NC Total Exento          =[" + string.Format("{0:0.00}", _zeta.getNC_TotalExento() / 100) + "]" + Environment.NewLine +
                                  "NC Total IVA             =[" + string.Format("{0:0.00}", _zeta.getNC_TotalIVA() / 100) + "]" + Environment.NewLine +
                                  "NC Total Otros Tributos  =[" + string.Format("{0:0.00}", _zeta.getNC_TotalTributos() / 100) + "]" + Environment.NewLine +
                                  "DNFH Emitidos            =[" + _zeta.getDNFH_CantidadEmitidos() + "]" + Environment.NewLine +
                                  "DNFH Total               =[" + _zeta.getDNFH_Total() + "]";

                                MessageBox.Show(new Form { TopMost = true }, _msj, "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

                                ///Modificamos la tabla Arqueos.
                                //string _dato = _zeta.getNumero() + ";" + _zeta.getDF_CantidadEmitidos() + ";" + _zeta.getDF_Total();

                                //int _rta = Arqueo.UpdateArqueos("Z", Convert.ToInt32(_numero), Convert.ToDateTime(_fecha), _hora, _dato, _conn);
                            }
                            catch (Exception ex)
                            {
                                RestHasar2G.Impresiones.Cancelar(hasar);
                                MessageBox.Show(new Form { TopMost = true }, "Se produjo el siguiente Error : " + ex.Message,
                                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Posee comprobantes sin fiscalizar. Por favor fiscalize o anule los comprobantes para poder realizar el cierre Z.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "No se encontro el archivo XML, de Configuración.",
                    "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
