﻿using hfl.argentina;
using RestHasar2G;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgFrontRestAperturaCajon
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            string _ip = "";
            string _caja = "";
            string _password = "";
            string _serverConfig = "";
            string _userConfig = "";
            string _catalogConfig = "";
            bool _monotributo = false;
            bool _hasarLog = false;
            string _tktregalo1 = "";
            string _tktregalo2 = "";
            string _tktregalo3 = "";
            string _keyIcg = "";
            string _codigoFudex = "";
            InfoIrsa _infoIrsa = new InfoIrsa();
            //SiTef
            SiTefService.InfoSitef _inforSiTef = new SiTefService.InfoSitef();
            //Leemos el archivo de configuracion
            if (FuncionesVarias.LeerXmlConfiguracion(out _ip, out _caja, out _password, out _serverConfig,
           out _userConfig, out _catalogConfig, out _monotributo, out _tktregalo1, out _tktregalo2,
           out _tktregalo3,out _hasarLog, out _keyIcg, out _infoIrsa, out _codigoFudex, out _inforSiTef))
            {
                string _key = IcgVarios.LicenciaIcg.Value();
                string _cod = IcgVarios.NuevaLicencia.Value(_key);

                if (_keyIcg == _cod)
                {
                    try
                    {
                        //aca Comienzo
                        hfl.argentina.HasarImpresoraFiscalRG3561 hasar = new hfl.argentina.HasarImpresoraFiscalRG3561();
                        //Conectamos con la hasar.
                        Impresiones.Conectar(hasar, _hasarLog, _ip);
                        hasar.AbrirCajonDinero();
                    }
                    catch (HasarException hx)
                    {
                        MessageBox.Show(new Form { TopMost = true }, hx.Message);
                    }
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Licencia Invalida." +  Environment.NewLine +
                        "Por favor comuniquese con ICG Argentina", "ICG Argentina", MessageBoxButtons.OK, 
                        MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            else
                MessageBox.Show(new Form { TopMost = true }, "No se encontro el archivo XML, de Configuración.",
                                "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }
    }
}
