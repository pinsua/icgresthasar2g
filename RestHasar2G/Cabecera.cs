﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestHasar2G
{
    public class Cabecera
    {
        public int fo { get; set; }
        public string serie { get; set; }
        public int numero { get; set; }
        public string n { get; set; }
        public double totalbruto { get; set; }
        public double totalneto { get; set; }
        public int codcliente { get; set; }
        public int codvendedor { get; set; }
        public DateTime fecha { get; set; }
        public double entregado { get; set; }
        public double propina { get; set; }
        public string serieFiscal { get; set; }
        public string serieFiscal2 { get; set; }
        public int numeroFiscal { get; set; }
        public string nomVendedor { get; set; }
        public int sala { get; set; }
        public int mesa { get; set; }
        public double BaseImp { get; set; }  
        public DateTime hora { get; set; }
        public string aliasespera { get; set; }
        public string supedido { get; set; }

        public static Cabecera GetCabecera(int _fo, string _serie, int _numero, string _N, int _codVendedor, SqlConnection _con)
        {

            //Consulta con modificacion
            string _sql = @"SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.TOTALBRUTO, TIQUETSCAB.TOTALNETO, 
                TIQUETSCAB.CODCLIENTE, TIQUETSCAB.CODVENDEDOR, TIQUETSCAB.FECHA, TIQUETSCAB.ENTREGADO, TIQUETSCAB.PROPINA,TIQUETSCAB.SERIEFISCAL 
                , TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, VENDEDORES.NOMBREVENDEDOR, TIQUETSCAB.SALA, TIQUETSCAB.MESA, TIQUETSCAB.HORAFIN, 
                TIQUETSCAB.BASEIMP1, TIQUETSCAB.SUPEDIDO, TIQUETSCAB.ALIASESPERA
                FROM TIQUETSCAB inner join VENDEDORES on TIQUETSCAB.CODVENDEDOR = VENDEDORES.CODVENDEDOR
                WHERE TIQUETSCAB.fo = @fo AND TIQUETSCAB.SERIE = @serie AND TIQUETSCAB.NUMERO = @numero AND TIQUETSCAB.N = @n 
                AND TIQUETSCAB.CODVENDEDOR = @codVendedor";

            Cabecera _cabecera = new Cabecera();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@fo", _fo);
                _cmd.Parameters.AddWithValue("@serie", _serie);
                _cmd.Parameters.AddWithValue("@n", _N);
                _cmd.Parameters.AddWithValue("@numero", _numero);
                _cmd.Parameters.AddWithValue("@codVendedor", _codVendedor);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cabecera.fo = Convert.ToInt32(_reader["Fo"]);//.GetInt32(0);
                            _cabecera.serie = _reader.GetString(1);
                            _cabecera.numero = _reader.GetInt32(2);
                            _cabecera.n = _reader.GetString(3);
                            _cabecera.totalbruto = _reader.GetDouble(4);
                            _cabecera.totalneto = _reader.GetDouble(5);
                            _cabecera.codcliente = _reader.GetInt32(6);
                            _cabecera.codvendedor = _reader.GetInt32(7);
                            _cabecera.fecha = _reader.GetDateTime(8);
                            _cabecera.entregado = _reader.GetDouble(9);
                            _cabecera.propina = _reader.GetDouble(10);
                            _cabecera.serieFiscal = _reader["SERIEFISCAL"] == DBNull.Value ? "": _reader.GetString(11);
                            _cabecera.serieFiscal2 = _reader["SERIEFISCAL2"] == DBNull.Value ? "" : _reader.GetString(12);
                            _cabecera.numeroFiscal = _reader.GetInt32(13);
                            _cabecera.nomVendedor = _reader.GetString(14);
                            _cabecera.sala = Convert.ToInt32(_reader[15]);
                            _cabecera.mesa = Convert.ToInt32(_reader[16]);
                            _cabecera.hora = _reader.GetDateTime(17);
                            _cabecera.BaseImp = _reader.GetDouble(18);
                            _cabecera.aliasespera = _reader["ALIASESPERA"] == DBNull.Value ? "": _reader["ALIASESPERA"].ToString();
                            _cabecera.supedido = _reader["SUPEDIDO"] == DBNull.Value ? "" : _reader["SUPEDIDO"].ToString();
                        }
                    }
                }
            }

            return _cabecera;
        }

        public static Cabecera GetCabecera(string _nroFiscal, string _ptoVta, SqlConnection _con)
        {
            //Consulta con modificacion
            string _sql = @"SELECT TIQUETSCAB.FO, TIQUETSCAB.SERIE, TIQUETSCAB.NUMERO, TIQUETSCAB.N, TIQUETSCAB.TOTALBRUTO, TIQUETSCAB.TOTALNETO, 
                    TIQUETSCAB.CODCLIENTE, TIQUETSCAB.CODVENDEDOR, TIQUETSCAB.FECHA, TIQUETSCAB.ENTREGADO, TIQUETSCAB.PROPINA,TIQUETSCAB.SERIEFISCAL 
                    , TIQUETSCAB.SERIEFISCAL2, TIQUETSCAB.NUMEROFISCAL, VENDEDORES.NOMBREVENDEDOR, TIQUETSCAB.SALA, TIQUETSCAB.MESA, TIQUETSCAB.HORAFIN, 
                    TIQUETSCAB.BASEIMP1, TIQUETSCAB.SUPEDIDO, TIQUETSCAB.ALIASESPERA
                    FROM TIQUETSCAB inner join VENDEDORES on TIQUETSCAB.CODVENDEDOR = VENDEDORES.CODVENDEDOR
                    WHERE TIQUETSCAB.NUMEROFISCAL = @NroFiscal and TIQUETSCAB.SERIEFISCAL = @PtoVta";

            Cabecera _cabecera = new Cabecera();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@NroFiscal", _nroFiscal);
                _cmd.Parameters.AddWithValue("@PtoVta", _ptoVta);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            _cabecera.fo = Convert.ToInt32(_reader["Fo"]);//.GetInt32(0);
                            _cabecera.serie = _reader.GetString(1);
                            _cabecera.numero = _reader.GetInt32(2);
                            _cabecera.n = _reader.GetString(3);
                            _cabecera.totalbruto = _reader.GetDouble(4);
                            _cabecera.totalneto = _reader.GetDouble(5);
                            _cabecera.codcliente = _reader.GetInt32(6);
                            _cabecera.codvendedor = _reader.GetInt32(7);
                            _cabecera.fecha = _reader.GetDateTime(8);
                            _cabecera.entregado = _reader.GetDouble(9);
                            _cabecera.propina = _reader.GetDouble(10);
                            _cabecera.serieFiscal = _reader.GetString(11);
                            _cabecera.serieFiscal2 = _reader.GetString(12);
                            _cabecera.numeroFiscal = _reader.GetInt32(13);
                            _cabecera.nomVendedor = _reader.GetString(14);
                            _cabecera.sala = Convert.ToInt32(_reader[15]);
                            _cabecera.mesa = Convert.ToInt32(_reader[16]);
                            _cabecera.hora = _reader.GetDateTime(17);
                            _cabecera.BaseImp = _reader.GetDouble(18);
                            _cabecera.aliasespera = _reader[19] == null ? "" : _reader[19].ToString();
                            _cabecera.supedido = _reader[20] == null ? "" : _reader[20].ToString();
                        }
                    }
                }
            }

            return _cabecera;
        }

        public static bool GrabarZetaCampoLibres(int _fo, string _serie, int _numero, string _N, int _zeta, SqlConnection _con, SqlTransaction _transac)
        {
            string _sql = "UPDATE TIQUETSVENTACAMPOSLIBRES SET Z_NRO = @nroZ WHERE FO = @fo and SERIE = @serie and NUMERO = @numero and N = @n";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transac;

                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _N);
                    _cmd.Parameters.AddWithValue("@nroZ", _zeta);
                    _cmd.Parameters.AddWithValue("@fo", _fo);

                    int _insert = _cmd.ExecuteNonQuery();
                    _rta = true;
                }
            }
            catch (Exception)
            {
                _rta = false;
            }

            return _rta;
        }

        public static bool TransaccionOK(Cabecera _cab, int _nroZeta, string _ptoVta, string _serieFiscal2, int _nroTicket, DateTime _fechaCF, SqlConnection _connection)
        {
            bool _rta = false;
            bool _campoLibre = FuncionesVarias.ValidarColumna("TIQUETSVENTACAMPOSLIBRES", "Z_NRO", _connection);
            SqlTransaction _tran;
            //Comienzo la transaccion
            _tran = _connection.BeginTransaction();
            //
            try
            {
                //Inserto TiquetsCab
                if (TiquetsCab.GrabarNumeroTiquetFecha(_cab.fo, _cab.serie, _cab.numero, _cab.n, _ptoVta, _serieFiscal2,
                    _nroTicket, _fechaCF, _nroZeta, _connection, _tran))
                {
                    //Recupero el nombre de la terminal.
                    string _terminal = Environment.MachineName;
                    //Grabo el numero de Zeta.
                    //Lo sacamos porque no va mas.
                    if(_campoLibre)
                        Cabecera.GrabarZetaCampoLibres(_cab.fo, _cab.serie, _cab.numero, _cab.n, _nroZeta, _connection, _tran);
                    //Inserto en Rem_transacciones.
                    Rem_transacciones.InsertRemTransacciones(_terminal, _cab.serie, _cab.numero, _cab.n, _connection, _tran);

                    _rta = true;
                }
                else
                    _rta = false;

                //Comiteo los cambios
                _tran.Commit();

                return _rta;
            }
            catch (Exception ex)
            {
                _tran.Rollback();
                throw new Exception("TransaccionOK " + ex.Message);
            }
        }

        public static void TransaccionBlack(int _fo, string _serie, int _numero, string _n, string _connectionString)
        {
            //bool _rta = false;
            string _serieFiscal1 = "0000";
            string _serieFiscal2 = "";
            int _numeroFiscal = _serie.Substring(0, 1) == "I" ? -2 : -1;

            if (_n == "N")
            {
                _numeroFiscal = 0;
                _serieFiscal1 = "";
            }
            
            using (SqlConnection _connection = new SqlConnection(_connectionString))
            {
                SqlTransaction _tran;
                _connection.Open();
                //Comienzo la transaccion
                _tran = _connection.BeginTransaction();
                try
                {                                        
                    //Inserto TiquetsCab
                    if (TiquetsCab.GrabarNumeroTiquet(_fo, _serie, _numero, _n, _serieFiscal1, _serieFiscal2,
                        _numeroFiscal,_connection, _tran))
                    {
                        //Recupero el nombre de la terminal.
                        string _terminal = Environment.MachineName;
                        //Inserto en Rem_transacciones.
                        Rem_transacciones.InsertRemTransacciones(_terminal, _serie, _numero, _n, _connection, _tran);
                    }
                    //Comiteo los cambios
                    _tran.Commit();
               }
                catch (Exception ex)
                {
                    _tran.Rollback();
                    throw new Exception("TransaccionOK " + ex.Message);
                }
            }
        }
    }
}
