﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestHasar2G
{
    public class Items
    {
        public string _codArticulo { get; set; }
        public string _descripcion { get; set; }
        public decimal _cantidad { get; set; }
        public decimal _precio { get; set; }
        public decimal _iva { get; set; }
        public decimal _descuento { get; set; }
        public string _textoPromo { get; set; }
        public string _abonoDeNumseie { get; set; }
        public int _abonoDeNumAlbaran { get; set; }
        public string _abonoDe_N { get; set; }
        public int _detalleModif { get; set; }
        public int _numlinea { get; set; }
        public decimal _impuestoInterno { get; set; }
        //public string _referencia { get; set; }

        public static List<Items> GetItems(string _Serie, string _n, int _numero, SqlConnection _con)
        {
            string _sql = @"SELECT TIQUETSLIN.CODARTICULO, TIQUETSLIN.NUMLINEA, TIQUETSLIN.DESCRIPCION, TIQUETSLIN.UNIDADES, TIQUETSLIN.PRECIO, 
                    TIQUETSLIN.IVA, TIQUETSLIN.DTO, TIQUETSLIN.ABONODE_SERIE, TIQUETSLIN.ABONODE_NUMERO, 
                    TIQUETSLIN.ABONODE_N, TIQUETSLIN.DETALLEMODIF, TIQUETSLIN.REQ  
                    FROM TIQUETSLIN 
                    WHERE TIQUETSLIN.SERIE = @serie and TIQUETSLIN.N = @N and TIQUETSLIN.NUMERO = @numero 
                    ORDER BY TIQUETSLIN.UNIDADES Desc";

            List<Items> _items = new List<Items>();

            using (SqlCommand _cmd = new SqlCommand())
            {
                _cmd.Connection = _con;
                _cmd.CommandType = System.Data.CommandType.Text;
                _cmd.CommandText = _sql;

                _cmd.Parameters.AddWithValue("@serie", _Serie);
                _cmd.Parameters.AddWithValue("@N", _n);
                _cmd.Parameters.AddWithValue("@numero", _numero);

                using (SqlDataReader _reader = _cmd.ExecuteReader())
                {
                    if (_reader.HasRows)
                    {
                        while (_reader.Read())
                        {
                            Items _cls = new Items();
                            _cls._cantidad = Convert.ToDecimal(_reader["UNIDADES"]);
                            _cls._codArticulo = _reader["CODARTICULO"].ToString();
                            _cls._descripcion = _reader["DESCRIPCION"].ToString();
                            _cls._descuento = Convert.ToDecimal(_reader["DTO"]);
                            _cls._iva = Convert.ToDecimal(_reader["IVA"]);
                            _cls._precio = Convert.ToDecimal(_reader["PRECIO"]);
                            //_cls._textoPromo = _reader["TEXTOIMPRIMIR"] == null ? "" : _reader["TEXTOIMPRIMIR"].ToString();
                            _cls._abonoDeNumseie = _reader["ABONODE_SERIE"] == null ? "" : _reader["ABONODE_SERIE"].ToString();
                            _cls._abonoDeNumAlbaran = _reader["ABONODE_NUMERO"] == DBNull.Value ? 0 : Convert.ToInt32(_reader["ABONODE_NUMERO"]);
                            _cls._abonoDe_N = _reader["ABONODE_N"] == null ? "" : _reader["ABONODE_N"].ToString();
                            _cls._detalleModif = Convert.ToInt16(_reader["DETALLEMODIF"]);
                            //_cls._referencia = _reader["REFERENCIA"].ToString();
                            _cls._numlinea = Convert.ToInt32(_reader["NUMLINEA"]);
                            _cls._impuestoInterno = Convert.ToDecimal(_reader["REQ"]);

                            _items.Add(_cls);
                        }
                    }
                }
            }

            return _items;
        }
    }
}
