﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IcgFrontRestConsola2019
{
    public partial class frmChangeDate : Form
    {
        string _serie;
        int _numero;
        string _n;
        SqlConnection _conection;
        public frmChangeDate(string serie, int numero, string n, SqlConnection conection )
        {
            InitializeComponent();
            //pasamos las variables.
            _serie = serie;
            _numero = numero;
            _n = n;
            _conection = conection;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string _sql = "UPDATE TIQUETSCAB SET FECHA = @fecha WHERE SERIE = @serie AND NUMERO = @numero AND N = @n";
            //Instanciamos la conexion.                
            SqlCommand _Command = new SqlCommand(_sql);
            _Command.Connection = _conection;

            _Command.Parameters.AddWithValue("@serie", _serie);
            _Command.Parameters.AddWithValue("@numero", _numero);
            _Command.Parameters.AddWithValue("@n", _n);
            _Command.Parameters.AddWithValue("@fecha", dtpChange.Value.Date);

            try
            {
                _Command.ExecuteNonQuery();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente Error: " + ex.Message,
                        "ICG Argentina", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
