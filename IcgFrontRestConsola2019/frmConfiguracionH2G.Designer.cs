﻿namespace IcgFrontRestConsola2019
{
    partial class frmConfiguracionH2G
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRegestry = new System.Windows.Forms.Button();
            this.btnValidarCampos = new System.Windows.Forms.Button();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtBaseDatos = new System.Windows.Forms.TextBox();
            this.txtServidor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTicketReg3 = new System.Windows.Forms.TextBox();
            this.txtTicketReg2 = new System.Windows.Forms.TextBox();
            this.txtTicketReg1 = new System.Windows.Forms.TextBox();
            this.txtPtoVta = new System.Windows.Forms.TextBox();
            this.txtCaja = new System.Windows.Forms.TextBox();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtLicenciaIcg = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnLicencia = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tcShoping = new System.Windows.Forms.TabControl();
            this.tpTranComp = new System.Windows.Forms.TabPage();
            this.btnRegenerarTrancomp = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIrsaPath = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtIrsaPos = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtIrsaRubro = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtIrsaContrato = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtIrsaLocal = new System.Windows.Forms.TextBox();
            this.tpSitef = new System.Windows.Forms.TabPage();
            this.chUsaClover = new System.Windows.Forms.CheckBox();
            this.txtPathSitef = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCuitIsvSitef = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtCuitSitef = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtIdTerminalSitef = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtIdTiendaSitef = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tcShoping.SuspendLayout();
            this.tpTranComp.SuspendLayout();
            this.tpSitef.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRegestry);
            this.groupBox1.Controls.Add(this.btnValidarCampos);
            this.groupBox1.Controls.Add(this.txtUsuario);
            this.groupBox1.Controls.Add(this.txtBaseDatos);
            this.groupBox1.Controls.Add(this.txtServidor);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(18, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1164, 148);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base de Datos";
            // 
            // btnRegestry
            // 
            this.btnRegestry.Location = new System.Drawing.Point(212, 85);
            this.btnRegestry.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnRegestry.Name = "btnRegestry";
            this.btnRegestry.Size = new System.Drawing.Size(256, 35);
            this.btnRegestry.TabIndex = 7;
            this.btnRegestry.Text = "Recuperar Datos de Conexión";
            this.btnRegestry.UseVisualStyleBackColor = true;
            this.btnRegestry.Click += new System.EventHandler(this.btnRegestry_Click);
            // 
            // btnValidarCampos
            // 
            this.btnValidarCampos.Location = new System.Drawing.Point(540, 85);
            this.btnValidarCampos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnValidarCampos.Name = "btnValidarCampos";
            this.btnValidarCampos.Size = new System.Drawing.Size(256, 35);
            this.btnValidarCampos.TabIndex = 6;
            this.btnValidarCampos.Text = "Validar Campos Libres";
            this.btnValidarCampos.UseVisualStyleBackColor = true;
            this.btnValidarCampos.Click += new System.EventHandler(this.btnValidarCampos_Click);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(850, 38);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(240, 26);
            this.txtUsuario.TabIndex = 5;
            this.txtUsuario.TextChanged += new System.EventHandler(this.txtUsuario_TextChanged);
            // 
            // txtBaseDatos
            // 
            this.txtBaseDatos.Location = new System.Drawing.Point(507, 38);
            this.txtBaseDatos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBaseDatos.Name = "txtBaseDatos";
            this.txtBaseDatos.Size = new System.Drawing.Size(232, 26);
            this.txtBaseDatos.TabIndex = 4;
            // 
            // txtServidor
            // 
            this.txtServidor.Location = new System.Drawing.Point(87, 38);
            this.txtServidor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtServidor.Name = "txtServidor";
            this.txtServidor.Size = new System.Drawing.Size(268, 26);
            this.txtServidor.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(777, 43);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Usuario";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(382, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Base de Datos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Servidor";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTicketReg3);
            this.groupBox2.Controls.Add(this.txtTicketReg2);
            this.groupBox2.Controls.Add(this.txtTicketReg1);
            this.groupBox2.Controls.Add(this.txtPtoVta);
            this.groupBox2.Controls.Add(this.txtCaja);
            this.groupBox2.Controls.Add(this.txtIp);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(18, 175);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(1164, 192);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Controlador";
            // 
            // txtTicketReg3
            // 
            this.txtTicketReg3.Location = new System.Drawing.Point(142, 148);
            this.txtTicketReg3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTicketReg3.Name = "txtTicketReg3";
            this.txtTicketReg3.Size = new System.Drawing.Size(493, 26);
            this.txtTicketReg3.TabIndex = 11;
            // 
            // txtTicketReg2
            // 
            this.txtTicketReg2.Location = new System.Drawing.Point(142, 111);
            this.txtTicketReg2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTicketReg2.Name = "txtTicketReg2";
            this.txtTicketReg2.Size = new System.Drawing.Size(493, 26);
            this.txtTicketReg2.TabIndex = 10;
            // 
            // txtTicketReg1
            // 
            this.txtTicketReg1.Location = new System.Drawing.Point(142, 74);
            this.txtTicketReg1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTicketReg1.Name = "txtTicketReg1";
            this.txtTicketReg1.Size = new System.Drawing.Size(493, 26);
            this.txtTicketReg1.TabIndex = 9;
            // 
            // txtPtoVta
            // 
            this.txtPtoVta.Enabled = false;
            this.txtPtoVta.Location = new System.Drawing.Point(906, 35);
            this.txtPtoVta.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPtoVta.Name = "txtPtoVta";
            this.txtPtoVta.Size = new System.Drawing.Size(184, 26);
            this.txtPtoVta.TabIndex = 8;
            this.txtPtoVta.Visible = false;
            // 
            // txtCaja
            // 
            this.txtCaja.Location = new System.Drawing.Point(507, 35);
            this.txtCaja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCaja.Name = "txtCaja";
            this.txtCaja.Size = new System.Drawing.Size(232, 26);
            this.txtCaja.TabIndex = 7;
            // 
            // txtIp
            // 
            this.txtIp.Location = new System.Drawing.Point(87, 35);
            this.txtIp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(268, 26);
            this.txtIp.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 151);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 20);
            this.label9.TabIndex = 5;
            this.label9.Text = "Ticket Regalo 3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 115);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "Ticket Regalo 2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 78);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "Ticket Regalo 1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(777, 40);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Pto Vta Manual";
            this.label6.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(382, 40);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Caja ICG";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 40);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "IP";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(834, 686);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(159, 35);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Text = "Guardar y Salir";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtLicenciaIcg
            // 
            this.txtLicenciaIcg.Location = new System.Drawing.Point(159, 32);
            this.txtLicenciaIcg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLicenciaIcg.Name = "txtLicenciaIcg";
            this.txtLicenciaIcg.ReadOnly = true;
            this.txtLicenciaIcg.Size = new System.Drawing.Size(481, 26);
            this.txtLicenciaIcg.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 37);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Licencia ICG";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnLicencia);
            this.groupBox6.Controls.Add(this.txtLicenciaIcg);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Location = new System.Drawing.Point(18, 583);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Size = new System.Drawing.Size(1164, 82);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Licencia ICG";
            // 
            // btnLicencia
            // 
            this.btnLicencia.Location = new System.Drawing.Point(759, 29);
            this.btnLicencia.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnLicencia.Name = "btnLicencia";
            this.btnLicencia.Size = new System.Drawing.Size(267, 35);
            this.btnLicencia.TabIndex = 2;
            this.btnLicencia.Text = "Generar Key Para Licencia";
            this.btnLicencia.UseVisualStyleBackColor = true;
            this.btnLicencia.Click += new System.EventHandler(this.btnLicencia_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(33, 677);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(725, 75);
            this.label18.TabIndex = 7;
            this.label18.Text = "IMPORTANTE: Solo debe llenar los campos del Shopping que corresponda.\r\nPara gener" +
    "ar la licencia debe tener acceso a internet. Caso contrario comuniquese\r\ncon ICG" +
    " Argentina.";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(1017, 686);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(156, 35);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Salir sin Guardar";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tcShoping);
            this.groupBox5.Location = new System.Drawing.Point(18, 377);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Size = new System.Drawing.Size(1164, 194);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Datos IRSA (Dejar en blanco si no se usan)";
            // 
            // tcShoping
            // 
            this.tcShoping.Controls.Add(this.tpTranComp);
            this.tcShoping.Controls.Add(this.tpSitef);
            this.tcShoping.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcShoping.Location = new System.Drawing.Point(4, 24);
            this.tcShoping.Name = "tcShoping";
            this.tcShoping.SelectedIndex = 0;
            this.tcShoping.Size = new System.Drawing.Size(1156, 165);
            this.tcShoping.TabIndex = 0;
            // 
            // tpTranComp
            // 
            this.tpTranComp.Controls.Add(this.btnRegenerarTrancomp);
            this.tpTranComp.Controls.Add(this.label16);
            this.tpTranComp.Controls.Add(this.txtIrsaPath);
            this.tpTranComp.Controls.Add(this.label23);
            this.tpTranComp.Controls.Add(this.txtIrsaPos);
            this.tpTranComp.Controls.Add(this.label24);
            this.tpTranComp.Controls.Add(this.txtIrsaRubro);
            this.tpTranComp.Controls.Add(this.label25);
            this.tpTranComp.Controls.Add(this.txtIrsaContrato);
            this.tpTranComp.Controls.Add(this.label26);
            this.tpTranComp.Controls.Add(this.txtIrsaLocal);
            this.tpTranComp.Location = new System.Drawing.Point(4, 29);
            this.tpTranComp.Name = "tpTranComp";
            this.tpTranComp.Padding = new System.Windows.Forms.Padding(3);
            this.tpTranComp.Size = new System.Drawing.Size(1148, 132);
            this.tpTranComp.TabIndex = 0;
            this.tpTranComp.Text = "Trancomp";
            this.tpTranComp.UseVisualStyleBackColor = true;
            // 
            // btnRegenerarTrancomp
            // 
            this.btnRegenerarTrancomp.Location = new System.Drawing.Point(771, 75);
            this.btnRegenerarTrancomp.Name = "btnRegenerarTrancomp";
            this.btnRegenerarTrancomp.Size = new System.Drawing.Size(234, 35);
            this.btnRegenerarTrancomp.TabIndex = 27;
            this.btnRegenerarTrancomp.Text = "Regenerar Info";
            this.btnRegenerarTrancomp.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(56, 82);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(135, 20);
            this.label16.TabIndex = 25;
            this.label16.Text = "Ubicación Archivo";
            // 
            // txtIrsaPath
            // 
            this.txtIrsaPath.Location = new System.Drawing.Point(200, 78);
            this.txtIrsaPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaPath.Name = "txtIrsaPath";
            this.txtIrsaPath.Size = new System.Drawing.Size(483, 26);
            this.txtIrsaPath.TabIndex = 26;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(56, 46);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 20);
            this.label23.TabIndex = 21;
            this.label23.Text = "POS";
            // 
            // txtIrsaPos
            // 
            this.txtIrsaPos.Location = new System.Drawing.Point(181, 42);
            this.txtIrsaPos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaPos.Name = "txtIrsaPos";
            this.txtIrsaPos.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaPos.TabIndex = 22;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(554, 46);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 20);
            this.label24.TabIndex = 23;
            this.label24.Text = "Rubro";
            // 
            // txtIrsaRubro
            // 
            this.txtIrsaRubro.Location = new System.Drawing.Point(679, 42);
            this.txtIrsaRubro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaRubro.Name = "txtIrsaRubro";
            this.txtIrsaRubro.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaRubro.TabIndex = 24;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(56, 12);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 20);
            this.label25.TabIndex = 17;
            this.label25.Text = "Contrato";
            // 
            // txtIrsaContrato
            // 
            this.txtIrsaContrato.Location = new System.Drawing.Point(181, 8);
            this.txtIrsaContrato.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaContrato.Name = "txtIrsaContrato";
            this.txtIrsaContrato.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaContrato.TabIndex = 18;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(554, 12);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 20);
            this.label26.TabIndex = 19;
            this.label26.Text = "Local";
            // 
            // txtIrsaLocal
            // 
            this.txtIrsaLocal.Location = new System.Drawing.Point(679, 8);
            this.txtIrsaLocal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIrsaLocal.Name = "txtIrsaLocal";
            this.txtIrsaLocal.Size = new System.Drawing.Size(326, 26);
            this.txtIrsaLocal.TabIndex = 20;
            // 
            // tpSitef
            // 
            this.tpSitef.Controls.Add(this.chUsaClover);
            this.tpSitef.Controls.Add(this.txtPathSitef);
            this.tpSitef.Controls.Add(this.label27);
            this.tpSitef.Controls.Add(this.txtCuitIsvSitef);
            this.tpSitef.Controls.Add(this.label28);
            this.tpSitef.Controls.Add(this.txtCuitSitef);
            this.tpSitef.Controls.Add(this.label29);
            this.tpSitef.Controls.Add(this.txtIdTerminalSitef);
            this.tpSitef.Controls.Add(this.label30);
            this.tpSitef.Controls.Add(this.txtIdTiendaSitef);
            this.tpSitef.Controls.Add(this.label31);
            this.tpSitef.Location = new System.Drawing.Point(4, 29);
            this.tpSitef.Name = "tpSitef";
            this.tpSitef.Padding = new System.Windows.Forms.Padding(3);
            this.tpSitef.Size = new System.Drawing.Size(1148, 132);
            this.tpSitef.TabIndex = 1;
            this.tpSitef.Text = "SiTef";
            this.tpSitef.UseVisualStyleBackColor = true;
            // 
            // chUsaClover
            // 
            this.chUsaClover.AutoSize = true;
            this.chUsaClover.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chUsaClover.Location = new System.Drawing.Point(864, 64);
            this.chUsaClover.Name = "chUsaClover";
            this.chUsaClover.Size = new System.Drawing.Size(112, 24);
            this.chUsaClover.TabIndex = 26;
            this.chUsaClover.Text = "Usa Clover";
            this.chUsaClover.UseVisualStyleBackColor = true;
            // 
            // txtPathSitef
            // 
            this.txtPathSitef.Location = new System.Drawing.Point(160, 64);
            this.txtPathSitef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPathSitef.Name = "txtPathSitef";
            this.txtPathSitef.Size = new System.Drawing.Size(407, 26);
            this.txtPathSitef.TabIndex = 25;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 70);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(135, 20);
            this.label27.TabIndex = 24;
            this.label27.Text = "Ubicación Archivo";
            // 
            // txtCuitIsvSitef
            // 
            this.txtCuitIsvSitef.Location = new System.Drawing.Point(682, 64);
            this.txtCuitIsvSitef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCuitIsvSitef.Name = "txtCuitIsvSitef";
            this.txtCuitIsvSitef.ReadOnly = true;
            this.txtCuitIsvSitef.Size = new System.Drawing.Size(148, 26);
            this.txtCuitIsvSitef.TabIndex = 23;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(595, 67);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(77, 20);
            this.label28.TabIndex = 22;
            this.label28.Text = "CUIT ISV";
            // 
            // txtCuitSitef
            // 
            this.txtCuitSitef.Location = new System.Drawing.Point(723, 26);
            this.txtCuitSitef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCuitSitef.Name = "txtCuitSitef";
            this.txtCuitSitef.Size = new System.Drawing.Size(148, 26);
            this.txtCuitSitef.TabIndex = 21;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(614, 29);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(98, 20);
            this.label29.TabIndex = 20;
            this.label29.Text = "CUIT Tienda";
            // 
            // txtIdTerminalSitef
            // 
            this.txtIdTerminalSitef.Location = new System.Drawing.Point(438, 26);
            this.txtIdTerminalSitef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIdTerminalSitef.Name = "txtIdTerminalSitef";
            this.txtIdTerminalSitef.Size = new System.Drawing.Size(148, 26);
            this.txtIdTerminalSitef.TabIndex = 19;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(336, 29);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(91, 20);
            this.label30.TabIndex = 18;
            this.label30.Text = "Id. Terminal";
            // 
            // txtIdTiendaSitef
            // 
            this.txtIdTiendaSitef.Location = new System.Drawing.Point(160, 26);
            this.txtIdTiendaSitef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIdTiendaSitef.Name = "txtIdTiendaSitef";
            this.txtIdTiendaSitef.Size = new System.Drawing.Size(148, 26);
            this.txtIdTiendaSitef.TabIndex = 17;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(73, 29);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(79, 20);
            this.label31.TabIndex = 16;
            this.label31.Text = "Id. Tienda";
            // 
            // frmConfiguracionH2G
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1202, 772);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmConfiguracionH2G";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina. Configuración Hasar 2G";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.tcShoping.ResumeLayout(false);
            this.tpTranComp.ResumeLayout(false);
            this.tpTranComp.PerformLayout();
            this.tpSitef.ResumeLayout(false);
            this.tpSitef.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtBaseDatos;
        private System.Windows.Forms.TextBox txtServidor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTicketReg3;
        private System.Windows.Forms.TextBox txtTicketReg2;
        private System.Windows.Forms.TextBox txtTicketReg1;
        private System.Windows.Forms.TextBox txtPtoVta;
        private System.Windows.Forms.TextBox txtCaja;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtLicenciaIcg;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnLicencia;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnValidarCampos;
        private System.Windows.Forms.Button btnRegestry;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TabControl tcShoping;
        private System.Windows.Forms.TabPage tpTranComp;
        private System.Windows.Forms.Button btnRegenerarTrancomp;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtIrsaPath;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtIrsaPos;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtIrsaRubro;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtIrsaContrato;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtIrsaLocal;
        private System.Windows.Forms.TabPage tpSitef;
        private System.Windows.Forms.CheckBox chUsaClover;
        private System.Windows.Forms.TextBox txtPathSitef;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCuitIsvSitef;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtCuitSitef;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtIdTerminalSitef;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtIdTiendaSitef;
        private System.Windows.Forms.Label label31;
    }
}