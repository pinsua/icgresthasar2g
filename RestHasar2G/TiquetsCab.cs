﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestHasar2G
{
    public class TiquetsCab
    {
        public static bool GrabarNumeroTiquet(int _fo, string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroFiscal, SqlConnection _con)
        {
            string _sql = "UPDATE TIQUETSCAB SET SERIEFISCAL = @serieFiscal, SERIEFISCAL2 = @serieFiscal2, NUMEROFISCAL = @numeroFiscal " +
                "WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _numeroFiscal);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }
            catch (Exception)
            {
                _rta = false;
            }

            return _rta;
        }

        public static bool GrabarNumeroTiquet(int _fo, string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroFiscal, SqlConnection _con, SqlTransaction _tran)
        {
            string _sql = "UPDATE TIQUETSCAB SET SERIEFISCAL = @serieFiscal, SERIEFISCAL2 = @serieFiscal2, NUMEROFISCAL = @numeroFiscal " +
                "WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _numeroFiscal);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }
            catch (Exception)
            {
                _rta = false;
            }

            return _rta;
        }

        public static bool GrabarNumeroTiquetFecha(int _fo, string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroFiscal, DateTime _fechaCF, int _nroZ, SqlConnection _con, SqlTransaction _transac)
        {
            string _sql = "UPDATE TIQUETSCAB SET SERIEFISCAL = @serieFiscal, SERIEFISCAL2 = @serieFiscal2, " +
                "NUMEROFISCAL = @numeroFiscal, " +
                "FECHAREAL = @fechaReal, CLEANCASHCONTROLCODE1 = @nroZ " +
                "WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n";

            bool _rta = false;

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _transac;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@numeroFiscal", _numeroFiscal);
                    _cmd.Parameters.AddWithValue("@fechaReal", _fechaCF);
                    _cmd.Parameters.AddWithValue("@nroZ", _nroZ);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
            }
            catch (Exception )
            {
                _rta = false;
            }

            return _rta;
        }

        public static bool GrabarNumeroTiquetCancelado(int _fo, string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroCancel, SqlConnection _con)
        {
            string _sql = "UPDATE TIQUETSCAB SET SERIEFISCAL = @serieFiscal, SERIEFISCAL2 = @serieFiscal2, NUMEROFISCAL = 0 " +
                "WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n";

            //string _sql2 = "UPDATE TIQUETSVENTACAMPOSLIBRES SET ANULACION_IRSA = @numeroCancel "+
            //    "WHERE FO = @fo AND SERIE = @serie AND NUMERO = @numero AND N = @n";

            bool _rta = false;

            try
            {
                //TiquetsCab
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    
                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        _rta = true;
                    else
                        _rta = false;
                }
                ////TiquetsVentaCamposLibres
                //using (SqlCommand _cmd = new SqlCommand())
                //{
                //    _cmd.Connection = _con;
                //    _cmd.CommandType = System.Data.CommandType.Text;
                //    _cmd.CommandText = _sql2;

                //    _cmd.Parameters.AddWithValue("@fo", _fo);
                //    _cmd.Parameters.AddWithValue("@serie", _serie);
                //    _cmd.Parameters.AddWithValue("@numero", _numero);
                //    _cmd.Parameters.AddWithValue("@n", _n);
                //    _cmd.Parameters.AddWithValue("@numeroCancel", _numeroCancel);

                //    int _update = _cmd.ExecuteNonQuery();
                //    if (_update > 0)
                //        _rta = true;
                //    else
                //        _rta = false;
                //}
            }
            catch (Exception )
            {
                _rta = false;
            }

            return _rta;
        }

        public static bool InsertCancelacion(int _fo, string _serie, int _numero, string _n, string _serieFiscal1,
            string _serieFiscal2, int _numeroFiscal, int _nroNuevo, int _nroZeta, SqlConnection _con, SqlTransaction _tran)
        {
            string _sql = @"INSERT INTO TIQUETSCAB (FO, SERIE, NUMERO, N,MESA, FECHA, HORAINI, HORAFIN, DIASEMANA, CODCLIENTE, CODVENDEDOR, CAJA, Z, IVAINC, CODTARIFA, ESBARRA, SUBTOTAL, 
                IMPRESIONES, CODTIPOTASA1, AUTOMATICO, ABONADO,CODALMACEN, FECHAANULACION, SERIEFISCAL, SERIEFISCAL2, NUMEROFISCAL, MESALIBERADA, TIQUETCOMP, 
                AGRUPADO, IDMOTIVODTO, FECHAREAL, HORATOTAL, FECHAINI, FECHAFIN, CLEANCASHCONTROLCODE1)
                SELECT FO, SERIE, @numeroNew, N,MESA, FECHA, HORAINI, HORAFIN, DIASEMANA, CODCLIENTE, CODVENDEDOR, CAJA, Z, IVAINC, CODTARIFA, ESBARRA, SUBTOTAL, 
                IMPRESIONES, CODTIPOTASA1, AUTOMATICO, ABONADO,CODALMACEN, FECHAANULACION, @SerieFiscal, @SerieFiscal2, @nroCancelado, MESALIBERADA, TIQUETCOMP, 
                AGRUPADO, IDMOTIVODTO, FECHAREAL, HORATOTAL, FECHAINI, FECHAFIN, @NroZeta
                FROM TIQUETSCAB
                WHERE SERIE = @Serie and NUMERO = @Numero and N = @N";

            try
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;
                    _cmd.Transaction = _tran;

                    _cmd.Parameters.AddWithValue("@fo", _fo);
                    _cmd.Parameters.AddWithValue("@serie", _serie);
                    _cmd.Parameters.AddWithValue("@numero", _numero);
                    _cmd.Parameters.AddWithValue("@n", _n);
                    _cmd.Parameters.AddWithValue("@numeroNew", _nroNuevo);
                    _cmd.Parameters.AddWithValue("@serieFiscal", _serieFiscal1);
                    _cmd.Parameters.AddWithValue("@serieFiscal2", _serieFiscal2);
                    _cmd.Parameters.AddWithValue("@nroCancelado", _numeroFiscal);
                    _cmd.Parameters.AddWithValue("@NroZeta", _nroZeta);

                    int _update = _cmd.ExecuteNonQuery();
                    if (_update > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("InsertCancelacion. " + ex.Message);
            }
        }
    }
}
