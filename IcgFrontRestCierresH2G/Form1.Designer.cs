﻿namespace IcgFrontRestCierresH2G
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btCierreX = new System.Windows.Forms.Button();
            this.btCierreZ = new System.Windows.Forms.Button();
            this.btSalir = new System.Windows.Forms.Button();
            this.btReimpresion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btCierreX
            // 
            this.btCierreX.Location = new System.Drawing.Point(17, 32);
            this.btCierreX.Name = "btCierreX";
            this.btCierreX.Size = new System.Drawing.Size(75, 23);
            this.btCierreX.TabIndex = 0;
            this.btCierreX.Text = "Cierre &X";
            this.btCierreX.UseVisualStyleBackColor = true;
            this.btCierreX.Click += new System.EventHandler(this.btCierreX_Click);
            // 
            // btCierreZ
            // 
            this.btCierreZ.Location = new System.Drawing.Point(101, 32);
            this.btCierreZ.Name = "btCierreZ";
            this.btCierreZ.Size = new System.Drawing.Size(75, 23);
            this.btCierreZ.TabIndex = 1;
            this.btCierreZ.Text = "Cierre &Z";
            this.btCierreZ.UseVisualStyleBackColor = true;
            this.btCierreZ.Click += new System.EventHandler(this.btCierreZ_Click);
            // 
            // btSalir
            // 
            this.btSalir.Location = new System.Drawing.Point(288, 32);
            this.btSalir.Name = "btSalir";
            this.btSalir.Size = new System.Drawing.Size(75, 23);
            this.btSalir.TabIndex = 2;
            this.btSalir.Text = "&Salir";
            this.btSalir.UseVisualStyleBackColor = true;
            this.btSalir.Click += new System.EventHandler(this.btSalir_Click);
            // 
            // btReimpresion
            // 
            this.btReimpresion.Location = new System.Drawing.Point(186, 32);
            this.btReimpresion.Name = "btReimpresion";
            this.btReimpresion.Size = new System.Drawing.Size(93, 23);
            this.btReimpresion.TabIndex = 3;
            this.btReimpresion.Text = "&Reimpresión Z";
            this.btReimpresion.UseVisualStyleBackColor = true;
            this.btReimpresion.Click += new System.EventHandler(this.btReimpresion_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 97);
            this.Controls.Add(this.btReimpresion);
            this.Controls.Add(this.btSalir);
            this.Controls.Add(this.btCierreZ);
            this.Controls.Add(this.btCierreX);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICG Argentina Cierres";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btCierreX;
        private System.Windows.Forms.Button btCierreZ;
        private System.Windows.Forms.Button btSalir;
        private System.Windows.Forms.Button btReimpresion;
    }
}

